<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    //
    protected $guarded = ['id'];

    protected $prefix = 'VIS00';


    public function reason()
    {
        return $this->belongsTo(VisitReason::class,'reason_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(Contact::class,'customer_id','id');
    }

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }

    public function getId()
    {
        return $this->prefix.$this->id;
    }
}
