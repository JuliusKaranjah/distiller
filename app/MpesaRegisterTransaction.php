<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MpesaRegisterTransaction extends Model
{
    protected $guarded = ['id'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }
}
