<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleRepInvoice extends Model
{
    //
    protected $guarded = ['id'];

    private $prefix = 'INV00';

    public function saleRep()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','id');
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

}
