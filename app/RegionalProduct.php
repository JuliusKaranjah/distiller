<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionalProduct extends Model
{
    //
    protected $guarded = ['id'];


    public function location()
    {
        return $this->belongsTo(BusinessLocation::class,'location_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
