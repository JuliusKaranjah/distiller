<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MpesaCallBackResponse extends Notification
{
    use Queueable;
    /**
     * @var string
     */
    private $string;

    /**
     * Create a new notification instance.
     *
     * @param string $string
     */
    public function __construct($string = 'nothing sent')
    {
        //
        $this->string = $string;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)

                    ->subject('Mpesa Response')
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line($this->string);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
