<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentProcessing extends Model
{
    protected $guarded = ['id'];

    protected $dates = ['remit_at'];

    public function transactionPayment()
    {
        return $this->belongsTo(TransactionPayment::class,'transaction_payment_id','id');
    }


    public function paymentProcessToArray()
    {
        return

            [
                'id' => $this->id ,
                'created_at'=> $this->created_at,
                'transaction_payment_id'=> $this->transaction_payment_id,
                'method'=> $this->method,
                'status'=> $this->status,
                'made_by' => $this->madeBy->fullName()
            ];
    }


    public function madeBy()
    {
        return $this->belongsTo(User::class,'created_by','id');
    }

}
