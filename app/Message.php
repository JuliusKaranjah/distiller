<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $guarded = ['id'];

    protected $prefix = 'MSG00';

    public function addedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }

    public function getSenderName()
    {
        return @$this->addedBy->fullName()?:'System';
    }

    public function getId()
    {
        return $this->prefix.$this->id;
    }
}
