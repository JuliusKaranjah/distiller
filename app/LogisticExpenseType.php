<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogisticExpenseType extends Model
{
    //
    protected $guarded = ['id'];
}
