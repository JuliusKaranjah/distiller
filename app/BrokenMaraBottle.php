<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokenMaraBottle extends Model
{
    //
    protected $guarded = ['id'];

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }

    public function addedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }
}
