<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampleOrderProduct extends Model
{
    //
    protected $guarded = ['id'];

    public function product()
    {
      return $this->belongsTo(Product::class);
    }

    public function sampleOrder()
    {
        return $this->belongsTo(SampleOrder::class);
    }
}
