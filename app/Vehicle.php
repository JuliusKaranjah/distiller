<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //
    protected $guarded = ['id'];

    public function setNumberPlateAttribute($value)
    {
        $this->attributes['number_plate'] = strtoupper(str_replace(' ','',$value));
    }

    public function getNumberPlateAttribute($numberPlate )
    {

        $vowels = str_limit($numberPlate,3,'');
        $numbers = str_replace($vowels,'',$numberPlate);

        return $vowels.' '.$numbers;
    }

    public function name()
    {
        return ucwords(strtolower($this->name)).' : '.$this->number_plate;
    }
}
