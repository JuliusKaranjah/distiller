<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SamplePromotionProduct extends Model
{
    //
    protected $guarded = ['id'];

    public function samplePromotion()
    {
        return $this->belongsTo(SamplePromotion::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
