<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampleProduct extends Model
{
    //
    protected $guarded = ['id'];

    private $prefix = 'STO00';

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getId()
    {
        return $this->prefix.$this->id;
    }
}
