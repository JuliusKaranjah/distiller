<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitReason extends Model
{
    //
    protected $guarded = ['id'];

    public function visits()
    {
         return $this->hasMany(Visit::class,'reason_id','id');
    }

    public function addedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }
}
