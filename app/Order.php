<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    protected $orderPrefix = 'ORD00';

    protected $dates = ['action_at','loaded_at', 'dispatched_at', 'received_at','sent_loading_bay_at'];

    private $prefix = 'DEL00';

    public function addedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function saleRepInvoice()
    {
        return $this->hasOne(SaleRepInvoice::class,'order_id','id');
    }

    public function locationFrom()
    {
        return $this->belongsTo(BusinessLocation::class,'origin_location_id','id');
    }

    public function locationTo()
    {
        return $this->belongsTo(BusinessLocation::class,'destination_location_id','id');
    }

    public function dispatchExpenses()
    {
        return $this->hasMany(OrderDispatchExpense::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }




    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getId()
    {
     return $this->orderPrefix . $this->id;
    }

    public function getInvoiceId()
    {
        return $this->prefix.$this->id;
    }
}
