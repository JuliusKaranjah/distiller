<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySaleRepInvoice extends Model
{
    //
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function add(User $user)
    {
        return

            $user->companySaleRepInvoice()->create(
            [
                'amount'
            ]
        );
    }
}
