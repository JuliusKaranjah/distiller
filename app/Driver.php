<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //
    protected $guarded = ['id'];

    public function getFullNameAttribute($full_name)
    {
        return ucfirst(strtolower($full_name));
    }
}
