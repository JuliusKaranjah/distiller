<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SampleOrder extends Model
{
    //
    protected $guarded = ['id'];

    private $prefix = 'PROM00';

    public function addedBy()
    {
        return $this->belongsTo(User::class,'added_by','id');
    }

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }

    public function sampleOrderProducts()
    {
        return $this->hasMany(SampleOrderProduct::class,'sample_order_id','id');
    }

    public function getId()
    {
        return $this->prefix.$this->id;
    }

}
