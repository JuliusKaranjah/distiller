<?php

namespace App\Http\Controllers;

use App\BusinessLocation,
    App\Printer,
    App\InvoiceLayout, 
    App\InvoiceScheme;

use App\MessageReciever;
use App\OrderDeliveryNotifiedUser;
use App\ProductionOrderRecipient;
use App\User;
use Illuminate\Http\Request;

class LocationSettingsController extends Controller
{
	 /**
     * All class instance.
     *
     */
    protected $printReceiptOnInvoice;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->printReceiptOnInvoice = ['1' => 'Yes', '0' => 'No'];
        $this->receiptPrinterType = ['browser' => 'Browser Based Printing', 'printer' => 'Use Configured Receipt Printer'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($location_id)
    {
    	//Check for locations access permission
		if (!auth()->user()->can('business_settings.access') || 
			!auth()->user()->can_access_this_location($location_id)
		) {
        	abort(403, 'Unauthorized action.');
    	}

    	$business_id = request()->session()->get('user.business_id');

    	$location = BusinessLocation::where('business_id', $business_id)
    					->findorfail($location_id);

        $printers = Printer::forDropdown($business_id);

    	$printReceiptOnInvoice = $this->printReceiptOnInvoice;
    	$receiptPrinterType = $this->receiptPrinterType;

        $invoice_layouts = InvoiceLayout::where('business_id', $business_id)
                            ->get()
                            ->pluck('name', 'id');
        $invoice_schemes = InvoiceScheme::where('business_id', $business_id)
                            ->get()
                            ->pluck('name', 'id');

    	return view ('location_settings.index')
            ->with(compact('location', 'printReceiptOnInvoice', 'receiptPrinterType', 'printers', 'invoice_layouts', 'invoice_schemes'));
    }

    /**
     * Update the settings
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateSettings($location_id, Request $request)
    {
    	try{

    		//Check for locations access permission
    		if (!auth()->user()->can('business_settings.access') || 
    			!auth()->user()->can_access_this_location($location_id)
    		) {
            	abort(403, 'Unauthorized action.');
        	}
        	
    		$input = $request->only(['print_receipt_on_invoice', 'receipt_printer_type', 'printer_id', 'invoice_layout_id', 'invoice_scheme_id']);

            //Auto set to browser in demo.
            if(config('app.env') == 'demo'){
                $input['receipt_printer_type'] = 'browser';
            }

	    	$business_id = request()->session()->get('user.business_id');

	    	$location = BusinessLocation::where('business_id', $business_id)
	    					->findorfail($location_id);

	    	$location->fill($input);
	    	$location->update();

	    	$output = ['success' => 1, 
	    				'msg' => __("receipt.receipt_settings_updated")
	    			];

    	} catch(\Exception $e){

            $output = ['success' => 0, 
	    				'msg' => __("messages.something_went_wrong")
	    			];
        }

    	return back()->with('status', $output);
    }

    public function addMessageRecipient(Request $request)
    {
        MessageReciever::create(
            [
                'user_id' => $request->user_id
            ]
        );

        return back()->with(['success' => 1,'msg' => 'Successfully added a new message recipient']);
    }

    public function deleteMessageRecipient($id)
    {
        MessageReciever::where(
            [
                'id' => $id
            ]
        )->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted message recipient']);
    }

    public function addProductionRecipient(Request $request)
    {

        ProductionOrderRecipient::create(
            [
                'user_id' => $request->user_id
            ]
        );
        return back()->with(['success' => 1,'msg' => 'Successfully added a new production message recipient']);
    }

    public function deleteProductionRecipient($id)
    {
        ProductionOrderRecipient::where(
            [
                'id' => $id
            ]
        )->delete();

        return back()->with(['success' => 1,'msg'=>'Successfully deleted production message recipient']);
    }

    public function addOrderDeliveryRecipient(Request $request)
    {

        OrderDeliveryNotifiedUser::create(
            [
                'user_id' => $request->user_id
            ]
        );
        return back()->with(['success' => 1,'msg' => 'Successfully added a message recipient']);
    }

    public function deleteOrderDeliveryRecipient($id)
    {

        OrderDeliveryNotifiedUser::where(
            [
                'id' => $id
            ]
        )->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted a message recipient']);
    }
}
