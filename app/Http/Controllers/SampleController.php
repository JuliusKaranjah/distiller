<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Product;
use App\SampleOrder;
use App\SampleOrderProduct;
use App\SampleProduct;
use App\SamplePromotion;
use App\SamplePromotionProduct;
use App\User;
use App\VariationLocationDetails;
use Carbon\Carbon;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SampleController extends Controller
{
    //
    public function index(Request $request)
    {


        if($request->ajax())
        {
            $sampleOrders = SampleOrder::where([]);


            if(isset($request->start_date) && isset($request->end_date))
            {
                $sampleOrders = $sampleOrders
                    ->where('created_at','>=',Carbon::parse($request->start_date)->startOfDay())
                    ->where('created_at','<=',Carbon::parse($request->end_date)->endOfDay());
            }


            $sampleOrders = $sampleOrders->get();



            return DataTables::of($sampleOrders)
                ->addColumn('order_id',function ($order)
                {
                    return $order->getId();
                })
                ->addColumn('added_by',function ($order)
                {
                    return $order->addedBy->fullName();
                })
                ->addColumn('products',function ($order)
                {
                    $html = '';
                    $comma ='';
                    foreach ($order->sampleOrderProducts as $sampleOrderProduct) {
                        $html .=$comma.$sampleOrderProduct->product->name.' - '.$sampleOrderProduct->quantity;
                        $comma =', ';
                    }
                    return $html;
                })
                ->editColumn('status',function ($order)
                {
                    return '<span class="badge badge-primary">'.$order->status.'</span>';
                })
                ->editColumn('accountant_status',function ($order)
                {
                    return '<span class="badge badge-primary">'.$order->accountant_status.'</span>';
                })
                ->editColumn('action',function ($order)
                {
                    $html = '';

                    if(auth()->user()->can('sample.accountant')||
                    auth()->user()->can('sample.sale_rep_approval')){


                    $html =
                        '<div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                data-toggle="dropdown" aria-expanded="false">
                                actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                    if(auth()->user()->can('sample.accountant')) {
                        $html .= "<li><a view-details href='" . action('SampleController@viewDetails', ['id' => $order->id]) . "'>
                                <i class='fa fa-money' aria-hidden='true'></i>Accountant Approval</a></li>";
                    }

                    if(auth()->user()->can('sample.sale_rep_approval')) {
                        $html .= "<li><a view-details href='" . action('SampleController@saleRepresentativeApproval', ['id' => $order->id]) . "'>
                                <i class='fa fa-money' aria-hidden='true'></i>Sale Repre. Approval</a></li>";
                    }
                    $html .="</ul>";

                    }
                    return $html;

                })
                ->rawColumns(['status','accountant_status','products','action'])
                ->make(true);
        }

        return view('sample.list');
    }

    public function createOrder()
    {
        $products = Product::all();

        return view('sample.createOrder')->with(['products' => $products]);
    }

    public function storeOrder(Request $request)
    {
        //make Order form sample products
        $sampleOrder = SampleOrder::create([
            'added_by' => auth()->id(),
            'status' => 'pending',
            'accountant_status' => 'pending',
        ]);

        $index = 0;
        foreach ($request->product as $item) {
            $sampleOrder->sampleOrderProducts()->create([
                'quantity' => $request->quantity[$index],
                'product_id' => $request->product[$index]
            ]);
            $index += 1;
        }

        //send messages to accounts and sale repre.
        $message = 'New Promo order has been created by '.auth()->user()->fullName().'.';
        $message .= ' containing: ';
        $index = 0;
        foreach ($sampleOrder->sampleOrderProducts as $sampleOrderProduct) {
            $comma = '';
            if($index+1 < $sampleOrder->sampleOrderProducts->count())
            {
                $comma = ', ';
            }
            $message .='-'.$sampleOrderProduct->product->sku.' - '.number_format($sampleOrderProduct->quantity) . $comma;
            $index +=1;
        }

        sendSmsToMany(getSaleRepresentativePhonesToArray(),$message);//all sale representatives

        sendSmsToMany(getAccountantPhonesToArray(),$message);//send to accountants

        return back()->with(['success' => 1,'msg' => 'Successfully ordered for promotion products']);
    }

    public function viewDetails(Request $request)
    {
        $sampleOrder = SampleOrder::where(['id' => $request->id])->first();

        return view('sample.__sampleAccountantModal',['sampleOrder' => $sampleOrder])->render();
    }

    public function accountantApprove(Request $request)
    {

        if ($request->action == 'decline')
        {
            SampleOrder::where(['id' => $request->order_id])->update(['accountant_status' => 'declined']);
            SampleOrderProduct::where(['sample_order_id' => $request->order_id])->update(['status' => 'declined']);
        }
        elseif($request->action == 'approve')
        {

            SampleOrder::where(['id' => $request->order_id])->update(['accountant_status' => 'approved']);


            if(!empty($request->product))
            {
                for ($index=0;$index<sizeof($request->product);$index++)
                {
                    if(doubleval($request->quantity[$index])>0)
                    {
                        SampleOrderProduct::where(['id' => $request->product[$index]])
                            ->update(['status' => 'approved','quantity' => $request->quantity[$index]]);
                    }else{
                        SampleOrderProduct::where(['id' => $request->product[$index]])
                            ->delete();
                    }
                }
            }
        }
        elseif ($request->action == 'approveSaleRep')
        {
            SampleOrder::where(['id' => $request->order_id])->update(['status' => 'approved']);

            SampleOrderProduct::where('sample_order_id',$request->order_id)->update(['status' => 'approved']);

            $sampleOrderProducts = SampleOrderProduct::
            where('sample_order_id',$request->order_id)->get();

            foreach ($sampleOrderProducts as $item) {
                $this->transferStockFromLocationToSalesRep
                ($item->product_id,getBusinessOrderOriginLocationId()
                    ,$item->product->variation_id,
                    $item->quantity,auth()->id());
            }


        }
        elseif ($request->action == 'declineSaleRep')
        {
            SampleOrder::where(['id' => $request->order_id])->update(['status' => 'declined']);

            SampleOrderProduct::where('sample_order_id',$request->order_id)->update(['status' => 'declined']);
        }

        return back()->with(['success' => 1,'msg'=>'Success']);
    }

    public function saleRepresentativeApproval(Request $request)
    {
        $sampleOrder = SampleOrder::where(['id' => $request->id])->first();

        return view('sample.__sampleModal',['sampleOrder' => $sampleOrder])->render();

    }

    private function transferStockFromLocationToSalesRep($productId,$locationId,$variationId,$quantity,$salesRep)
    {

        VariationLocationDetails::where('variation_id', $variationId)
            ->where('product_id', $productId)
            ->where('location_id', $locationId)
            ->decrement('qty_available', $quantity);

        //to the sale prom. account.

        $sampleProduct = SampleProduct::where(
            [
                'product_id' => $productId,
                'sale_rep_id' => $salesRep,
                'variation_id' => $variationId,
            ]
        );

        if($sampleProduct->count())
        {

            $sampleProduct->update(['quantity'=>$quantity + $sampleProduct->first()->quantity]);

        }else{
            $sampleProduct = SampleProduct::create(
                [
                    'product_id' => $productId,
                    'sale_rep_id' => $salesRep,
                    'variation_id' => $variationId,
                    'quantity' => $quantity,
                ]
            );
        }

        return true;
    }

    public function stock(Request $request)
    {
        if($request->ajax())
        {
            $stocks = SampleProduct::all();


            return DataTables::of($stocks)
                ->addColumn('stock_id',function ($stock)
                {
                    return $stock->getId();
                })
                ->addColumn('product',function ($stock)
                {
                    return $stock->product->name;
                })
                ->addColumn('action',function ($stock)
                {
                    $html =
                        '<div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                data-toggle="dropdown" aria-expanded="false">
                                actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                    $html .= "<li><a view-details href='" . action('SampleController@viewDetails',['id' => $stock->id]) . "'>
                                <i class='fa fa-money' aria-hidden='true'></i>Accountant Approval</a></li>";


                    $html .= "<li><a view-details href='" . action('SampleController@saleRepresentativeApproval',['id' => $stock->id]) . "'>
                                <i class='fa fa-money' aria-hidden='true'></i>Sale Repre. Approval</a></li>";

                    $html .="</ul>";

                    return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('sample.stock');
    }

    public function assign()
    {
        $stocks = SampleProduct::where(['sale_rep_id'=>auth()->id()])->get();

        $customers = Contact::all();

        return view('sample.assign')->with(['stocks' => $stocks,'customers' => $customers,]);
    }

    public function storePromotion(Request $request)
    {
        //

        if(!isset($request->customer) || ($request->customer=='') || is_null($request->customer))
        {
            return back()->with(['success' => 0,'msg' => 'Select customer']);
        }
        if(!isset($request->product) || empty($request->product))
        {
            return back()->with(['success' => 0,'msg' => 'Trying to assign no products']);
        }


        $samplePromotion = SamplePromotion::create(
            [
                'customer_id' => $request->customer,
                'added_by' => auth()->id(),
                'description' => $request->note
            ]
        );

        //add sample promotion products
        for ($index = 0; sizeof($request->product)>$index; $index++) {


            $stock = SampleProduct::where(['id' => $request->product[$index]])->first();


            $samplePromotion->samplePromotionProducts()->create(
                [
                    'product_id' => $stock->product_id,
                    'quantity' => ($quantity = $request->quantity[$index])
                ]
            );


            $stock->decrement('quantity',$quantity);

        }


        //send message to customer
        $message='You have received a gift from Crywan Enterprises';


            $comma = '';
            foreach ($samplePromotion->samplePromotionProducts as $samplePromotionProduct) {

                $message .= $comma.'- '.$samplePromotionProduct->product->name;
                $comma = ', ';
            }


        $message .= '. We are glad to be of service. For queries / compliments, contact us : 0722 260928 or email at sales@crywandistillers.com';

        sendSms($samplePromotion->customer->mobile,$message);

        return back()->with(['success' => 1,'msg' => 'Successfully assigned promotion']);
    }

    public function assignToSales(Request $request)
    {
        $stocks = SampleProduct::where(['sale_rep_id'=>auth()->id()])->get();

        $sales = User::where('id','!=',auth()->id())->get();

        return view('sample.assignToSales')->with(['stocks' => $stocks,'sales' => $sales,]);
    }

    public function assignSampleToSales(Request $request)
    {

        //validate selected sales and has products

        if(!isset($request->sale_rep) || is_null($request->sale_rep) || $request->sale_rep=='')
        {
            return back()->with(['success' => 0,'msg' => 'Please select sale rep.']);
        }

        if(!isset($request->product) || empty($request->product) || $request->product=='')
        {
            return back()->with(['success' => 0,'msg' => 'Please select products.']);
        }


        for($index=0; $index<sizeof($request->product); $index++)
        {
            $this->moveSampleStock(auth()->id(),$request->sale_rep,$request->product[$index],$request->quantity[$index]);
        }

        return back()->with(['success' => 1,'msg' => 'Successfully issued products to sales rep']);
    }

    private function moveSampleStock($from, $to, $sampleProductId, $quantity)
    {
        try{


            //decrease stock
            $sampleProductOrigin = SampleProduct::where([
                'id' => $sampleProductId,
                'sale_rep_id' => $from
            ])->first();

            $sampleProductOrigin->decrement('quantity',$quantity);

            //increase stock

            $sampleProductDesti = SampleProduct::where([
                'product_id' => $sampleProductOrigin->product_id,
                'sale_rep_id' => $to
            ])->first();

            if(is_null($sampleProductDesti))
            {
                $sampleProductDesti = SampleProduct::create([
                    'product_id' => $sampleProductOrigin->product_id,
                    'sale_rep_id' => $to
                ]);
            }


            $sampleProductDesti->update(['quantity'=>$quantity+$sampleProductDesti->quantity]);

        }catch (\Exception $e)
        {
            //
            dd($e);
        }

        return true;
    }
}
