<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MessageController extends Controller
{
    //
    public function index()
    {
        if(request()->ajax() && (auth()->user()->can('message.view')||auth()->user()->can('message.view_all')))
        {
            if(auth()->user()->can('message.view_all')) {
                $messages = Message::all();
            }else{
                $messages = Message::where(['added_by'=>auth()->id()])->get();
            }

            return DataTables::of($messages)
                ->editColumn('recipient',function ($message){
                    return str_replace('_',' ',$message->recipient);
                })
                ->editColumn('id',function ($message){
                    return $message->getId();
                })
                ->make(true);
        }
        return view('messages.list');
    }
    public function create()
    {
        $users = User::where('phone','!=',null)->get();
        $contacts = Contact::where('mobile','!=',null)->get();

        return view('messages.create',compact('users','contacts'));
    }

    private function recordMessage($message,$recipient)
    {
        return Message::create(
            [
                'added_by' => auth()->id(),
                'body' => $message,
                'recipient' => getNameFromPhone($recipient)
            ]
        );
    }

    public function store(Request $request)
    {

        $message = $request->body;

        $recipients = [];

        if($request->recipient =='other')
        {
            $recipients = array_unique($request->recipientInput);

            foreach ($recipients as $recipient) {

                $this->recordMessage($message,$recipient);
            }

        }else{
            if($request->recipient == 'all_customers')
            {
                $recipients = getAllCustomerNumbersToArray();

            }elseif($request->recipient == 'all_sale_rep')
            {
                $recipients = getAllSaleRepNumbersToArray();
            }

            $this->recordMessage($message,$request->recipient);
        }


        if(!empty($recipients))
        {
            sendSmsToMany($recipients,$message);

        }
        

        return back()->with(['success' => 1,'msg' => 'Successfully sent messages']);
    }
}
