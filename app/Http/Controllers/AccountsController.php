<?php

namespace App\Http\Controllers;

use App\BrokenMaraBottle;
use App\CompanySaleRepInvoice;
use App\Contact;
use App\CreditNote;
use App\DebitNote;
use App\PaymentProcessing;
use App\RemittedCommission;
use App\SaleRepInvoice;
use App\SaleStatement;
use App\Transaction;
use App\User;
use App\Utils\ProductUtil;
use App\Utils\TransactionUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Repo\Invoice;
use Yajra\DataTables\Facades\DataTables;

class AccountsController extends Controller
{
    //
    public function __construct(TransactionUtil $transactionUtil, ProductUtil $productUtil)
    {
        $this->transactionUtil = $transactionUtil;
        $this->productUtil = $productUtil;
    }


    public function index(Request $request)
    {

        if (auth()->user()->can('accounts.view'))
        {

            if (request()->ajax()) {
                $business_id = request()->session()->get('user.business_id');


                $cheques = PaymentProcessing::where(['method' => 'cheque'])->get(['id', 'method', 'status', 'cheque_number', 'amount', 'created_at', 'created_by', 'transaction_payment_id'])->load(['madeBy', 'transactionPayment']);


                if (request()->has('created_by')) {
                    $created_by = request()->get('created_by');
                    if (!empty($created_by)) {
                        $cheques->where('transactions.created_by', $created_by);
                    }
                }

                if ($request->start_date && $request->end_date) {
                    $start = request()->start_date;
                    $end = request()->end_date;
                    $cheques = $cheques->where('created_at', '>=', Carbon::parse($start)->startOfDay())
                        ->where('created_at', '<=', Carbon::parse($end)->endOfDay());


                }


                return DataTables::of($cheques)
                    ->addColumn('action',
                        '<div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                        data-toggle="dropdown" aria-expanded="false">' .
                        __("messages.actions") .
                        '<span class="caret"></span><span class="sr-only">Toggle Dropdown
                        </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    
                        <li><a href="{{action(\'AccountsController@chequeDetails\', [$id])}}" class="view_payment_modal"><i class="fa fa-money"></i>view details</a></li>
                    </ul></div>'
                    )
                    ->removeColumn('id')
                    ->editColumn('amount', 'KES {{@number_format($amount,2)}}')
                    ->editColumn('created_at', '{{@format_date($created_at)}}')
                    ->editColumn('made_by', '{{@($made_by["surname"]." ".$made_by["first_name"]." ".$made_by["last_name"] )}}')
                    ->addColumn('customer', '{{@getCustomerName($transaction_payment)}}')
                    ->editColumn('status',
                        '<span class="label label-{{@statusBadgeClass($status)}}">{{$status}}</span>'
                    )
                    ->setRowAttr([
                        'data-href' => function ($row) {
                            if (auth()->user()->can("sell.view")) {
                                return action('SellController@show', [$row->id]);
                            } else {
                                return '';
                            }
                        }])
                    ->rawColumns(['amount', 'made_by', 'status', 'action', 'transaction_payment', 'customer'])
                    ->make(true);
            }

            return view('accounts.list');
        }
    }

    public function chequeDetails(Request $request)
    {
        if (auth()->user()->can('accounts.view'))
        {
            $chequeId = $request->keys()[0];

            $cheque = PaymentProcessing::where(['id' => $chequeId])->first()->load(['transactionPayment']);

            return view('accounts.chequeModal', ['cheque' => $cheque])->render();
        }


    }

    public function chequeAction(Request $request)
    {

        if (auth()->user()->can('accounts.view'))
        {
            $cheque = PaymentProcessing::where(['id' => $request['chequeId']])->first();



            if (!is_null($cheque)) {

                $action = 'approved';
                if($request->action == 'decline'){
                    $action = 'declined';
                }

                if($request->action == 'accept')
                {
                    Invoice::adjustSaleRepInvoice($cheque->madeBy,$cheque->amount);
                }elseif($cheque->status != 'initiated'){

                    Invoice::adjustSaleRepInvoice($cheque->madeBy,$cheque->amount * -1);
                }

                $cheque->update(['status' => $action]);
            }



            return back()->with(['status' => 1,'msg' => 'Successfully ' . $action . ' cheque']);
        }
        return back(['status' => 0,'msg' => 'Permission denied']);
    }

    public function mpesaList(Request $request)
    {


        if (auth()->user()->can('accounts.view'))
        {
            if (request()->ajax()) {

                $mpesa = PaymentProcessing::where(['method' => 'mpesa'])->get(['id', 'status', 'method', 'amount', 'created_at', 'created_by', 'transaction_payment_id'])->load(['madeBy', 'transactionPayment']);


                if ($request->start_date && $request->end_date) {
                    $start = request()->start_date;
                    $end = request()->end_date;
                    $mpesa = $mpesa->where('created_at', '>=', Carbon::parse($start)->startOfDay())
                        ->where('created_at', '<=', Carbon::parse($end)->endOfDay());


                }


                return DataTables::of($mpesa)
                    ->addColumn('action',
                        '<div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                        data-toggle="dropdown" aria-expanded="false">' .
                        __("messages.actions") .
                        '<span class="caret"></span><span class="sr-only">Toggle Dropdown
                        </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    
                        <li><a href="{{action(\'AccountsController@chequeDetails\', [$id])}}" class="view_payment_modal"><i class="fa fa-money"></i>view details</a></li>
                    </ul></div>'
                    )
                    ->removeColumn('id')
                    ->editColumn('amount', 'KES {{@number_format($amount,2)}}')
                    ->editColumn('created_at', '{{@format_date($created_at)}}')
                    ->editColumn('made_by', '{{@($made_by["surname"]." ".$made_by["first_name"]." ".$made_by["last_name"] )}}')
                    ->addColumn('customer', '{{@getCustomerName($transaction_payment)}}')
                    ->editColumn('status',
                        '<span class="label label-{{@statusBadgeClass($status)}}">{{$status}}</span>'
                    )
                    ->rawColumns(['amount', 'made_by', 'status', 'transaction_payment', 'customer'])
                    ->make(true);
            }

            return view('accounts.mpesaList');
        }
    }

    public function cashList(Request $request)
    {
        if (auth()->user()->can('accounts.view'))
        {
            if (request()->ajax()) {

                $cash = PaymentProcessing::where(['method' => 'cash'])->get(['id', 'status', 'method', 'amount', 'created_at', 'created_by', 'transaction_payment_id'])->load(['madeBy', 'transactionPayment']);


                if ($request->start_date && $request->end_date) {
                    $start = request()->start_date;
                    $end = request()->end_date;
                    $cash = $cash->where('created_at', '>=', Carbon::parse($start)->startOfDay())
                        ->where('created_at', '<=', Carbon::parse($end)->endOfDay());


                }


                return DataTables::of($cash)
                    ->addColumn('action',
                        '<div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                        data-toggle="dropdown" aria-expanded="false">' .
                        __("messages.actions") .
                        '<span class="caret"></span><span class="sr-only">Toggle Dropdown
                        </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    
                        <li><a href="{{action(\'AccountsController@chequeDetails\', [$id])}}" class="view_payment_modal"><i class="fa fa-money"></i>view details</a></li>
                    </ul></div>'
                    )
                    ->removeColumn('id')
                    ->removeColumn('action')
                    ->editColumn('amount', 'KES {{@number_format($amount,2)}}')
                    ->editColumn('created_at', '{{@format_date($created_at)}}')
                    ->editColumn('made_by', '{{@($made_by["surname"]." ".$made_by["first_name"]." ".$made_by["last_name"] )}}')
                    ->addColumn('customer', '{{@getCustomerName($transaction_payment)}}')
                    ->editColumn('status',
                        '<span class="label label-{{@statusBadgeClass($status)}}">{{$status}}</span>'
                    )
                    ->rawColumns(['amount', 'made_by', 'status', 'transaction_payment', 'customer'])
                    ->make(true);
            }

            return view('accounts.cashList');
        }
    }

    public function bankTransferList(Request $request)
    {
        if (auth()->user()->can('accounts.view'))
        {
            if (request()->ajax()) {

                $cash = PaymentProcessing::where(['method' => 'bank_transfer'])->get(['id', 'status', 'method', 'amount', 'created_at', 'created_by', 'transaction_payment_id'])->load(['madeBy', 'transactionPayment']);


                if ($request->start_date && $request->end_date) {
                    $start = request()->start_date;
                    $end = request()->end_date;
                    $cash = $cash->where('created_at', '>=', Carbon::parse($start)->startOfDay())
                        ->where('created_at', '<=', Carbon::parse($end)->endOfDay());


                }


                return DataTables::of($cash)
                    ->addColumn('action',
                        '<div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                        data-toggle="dropdown" aria-expanded="false">' .
                        __("messages.actions") .
                        '<span class="caret"></span><span class="sr-only">Toggle Dropdown
                        </span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    
                        <li><a href="{{action(\'AccountsController@bankTransferModal\', [$id])}}" class="view_payment_modal"><i class="fa fa-money"></i>view details</a></li>
                    </ul></div>'
                    )
                    ->removeColumn('id')
                    ->editColumn('amount', 'KES {{@number_format($amount,2)}}')
                    ->editColumn('created_at', '{{@format_date($created_at)}}')
                    ->editColumn('made_by', '{{@($made_by["surname"]." ".$made_by["first_name"]." ".$made_by["last_name"] )}}')
                    ->addColumn('customer', '{{@getCustomerName($transaction_payment)}}')
                    ->editColumn('status',
                        '<span class="label label-{{@statusBadgeClass($status)}}">{{$status}}</span>'
                    )
                    ->rawColumns(['amount', 'made_by', 'status', 'transaction_payment', 'customer', 'action'])
                    ->make(true);
            }

            return view('accounts.bankTransferList');
        }
    }

    public function receiveCashFromSales($id)
    {
        if (auth()->user()->can('accounts.view'))
        {
            $saleReps = User::all();

            return view('accounts.CashDepositModal', ['saleReps' => $saleReps,'saleRepId' => $id])->render();
        }
    }

    public function postReceiveCashFromSales(Request $request)
    {


        if (auth()->user()->can('accounts.view'))
        {
            $cash = PaymentProcessing::where(['method' => 'cash', 'created_by' => $request->sale_rep_id, 'status' => 'initiated'])->orderBy('created_at', 'desc')->get();

            $cashAmount = $cash->sum('amount');


            if ($request->cash_amount > $cashAmount) {
                return back()->with(['success' => 0 ,'msg' => 'Amount exceeds the cash the sale rep has']);
            }

            $remainingAmount = $request->cash_amount;


            foreach ($cash as $item) {
                //if the amount is same as the cash recorded.
                if($remainingAmount>0)
                {
                    if($item->amount == $remainingAmount)
                    {
                        $item->update(['status' => 'completed', 'remit_at' => now()]);
                        $remainingAmount = 0;
                    }elseif($item->amount > $remainingAmount){
                        $itemData = $item->toArray();

                        $item->update(['amount' => $item->amount - $remainingAmount]);

                        $itemData['amount'] = $remainingAmount;
                        $itemData['remit_at'] = now();
                        $itemData['status'] = 'completed';

                        PaymentProcessing::create($itemData);
                        $remainingAmount = 0;
                    }else{
                        $item->update(['status' => 'completed', 'remit_at' => now()]);
                        $remainingAmount -= $item->amount;
                    }
                }

            }

            Invoice::adjustSaleRepInvoice(User::where(['id' => $request->sale_rep_id])->first(),$request->cash_amount);

            return back()->with(['success' => 'Successfully collected cash']);
        }
        return back()->with(['status'=>0,'msg' => 'Permission denied']);
    }

    public function bankTransferModal($id)
    {
        $trans = PaymentProcessing::where(['id' => $id])->first();

        return view('accounts.bankTransferModal', ['cheque' => $trans])->render();
    }

    public function invoice()
    {
        if(auth()->user()->can('invoice.view'))
        {
            if(\request()->ajax())
            {
                if(auth()->user()->can('invoice.view_all_invoice'))
                {
                    $saleReps = User::has('saleRepInvoices')->get();

                }else{
                    $saleReps = User::has('saleRepInvoices')->where(['id'=>auth()->id()])->get();

                }

                return DataTables::of($saleReps)

                    ->addColumn('name',function ($user){
                        return $user->fullName();
                    })
                    ->addColumn('invoice',function ($user){
                        return "KES ".number_format($user->totalInvoiceAmount(),2);
                    })
                    ->addColumn('commission',function ($user){
                        return "KES ".number_format($user->totalCompanySaleRepInvoice(),2);
                    })
                    ->addColumn('allowed_limit',function ($user){
                        return "KES ".number_format($user->credit_limit - $user->totalInvoiceAmount(),2);
                    })
                    ->editColumn('credit_limit',function ($user){
                        return "KES ".number_format($user->credit_limit,2);
                    })
                    ->editColumn('id',function ($user){
                        return '#'.$user->id;
                    })
                    ->make(true);

            }


            $invoiceTotal = SaleRepInvoice::where([])->sum('amount');
            $commissionTotal = CompanySaleRepInvoice::where([])->sum('amount');

            return view('accounts.invoices')->with(['invoiceTotal'=>$invoiceTotal,'commissionTotal'=>$commissionTotal]);

        }

        abort(422,'Access denied');
    }

    public function reconciliation(Request $request)
    {
        //configure permissions
        if(auth()->user()->can('accounts.view_all'))
        {
            $users = User::has('saleRepInvoices')->orHas('companySaleRepInvoice')->orHas('paymentProcessings')->get();

            $user = isset($request->sr_id)?User::whereId($request->sr_id)->first():$users->first();

        }else{

            $users = User::where(['id' => auth()->id()])->get();
            $user = auth()->user();
        }




        $cash = PaymentProcessing::where(['method' => 'cash', 'created_by'=>isset($request->sr_id)?$request->sr_id:$user->id])->get();
        $mpesa = PaymentProcessing::where(['method' => 'mpesa', 'created_by'=>isset($request->sr_id)?$request->sr_id:$user->id])->get();
        $cheque = PaymentProcessing::where(['method' => 'cheque', 'created_by'=>isset($request->sr_id)?$request->sr_id:$user->id])->get();
        $bankTransfer = PaymentProcessing::where(['method' => 'bank_transfer', 'created_by'=>isset($request->sr_id)?$request->sr_id:$user->id])->get();
        $other = PaymentProcessing::where(['method' => 'other', 'created_by'=>isset($request->sr_id)?$request->sr_id:$user->id])->get();

        $remittedCommissions =  RemittedCommission::where(['sale_rep_id' => isset($request->sr_id)?$request->sr_id:$user->id])->get();


        return view('accounts.reconcile')
            ->with([
                'users' => $users,
                'saleRep' => $user,
                'cash'=>$cash,
                'mpesa'=>$mpesa,
                'cheque'=>$cheque,
                'other'=>$other,
                'bankTransfer'=>$bankTransfer,
                'remittedCommissions'=>$remittedCommissions,
            ]);
    }

    public function remitCommission(Request $request)
    {

        //find the user
        $amount = doubleval($request->remit_amount);
        $description = $request->description;

        $user = User::where(['id' => $request->sale_rep_id])->first();


        if($amount<=0)
        {
            return back()->with(['success' => 0,'msg' => 'Please enter a valid amount']);
        }

        if($amount > $user->totalCompanySaleRepInvoice())
        {
            return back()->with(['success' => 0,'msg' => 'Amount to remit exceed the sales rep. commission']);
        }

        if(!is_null($user))
        {

            $remit = RemittedCommission::create(
                [
                    'added_by' => auth()->id(),
                    'amount' => $amount,
                    'sale_rep_id' => $request->sale_rep_id,
                    'description' => $description
                ]
            );

            (new Invoice)->reduceCompanyCommission($user,$amount);
        }

        return back()->with(['success' => 1,'msg' => 'Successfully remitted sales rep. commission']);
    }

    public function statement(Request $request)
    {
        if($request->ajax())
        {
            $users = User::all();

            return DataTables::of($users)
                ->addColumn('name',function ($data){
                    return $data->fullName();
                })
                ->addColumn('action',function ($data){

                            $html = '<div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                     data-toggle="dropdown" aria-expanded="false">actions
                                    <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                    </span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                            $html .= "<li><a href='".action('AccountsController@saleStatement',['id' => $data->id])."'><i class='fa fa-info' aria-hidden='true'></i>View Statement</a></li>";
                            $html .= "</ul>";
                            return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('accounts.statements');
    }

    public function saleStatement($id, Request $request)
    {

        if(!isset($request->start_date)){
            $request->start_date = Carbon::now()->startOfDay();
        }
        if(!isset($request->end_date)){
            $request->end_date = Carbon::now()->endOfDay();
        }

        $sale = User::where(['id' => $id])->first();

        $saleStatements = SaleStatement::where('sale_rep_id',$id)
            ->whereBetween('created_at',[Carbon::parse(str_replace('-','/',$request->start_date))->startOfDay(),Carbon::parse(str_replace('-','/',$request->end_date))->endOfDay()])
            ->get();


        $sell_details = $this->transactionUtil->getSellTotals(getBusiness()->id, Carbon::parse(str_replace('-','/',$request->start_date))->startOfDay(), Carbon::parse(str_replace('-','/',$request->end_date))->endOfDay(), '', $id);


        return view('accounts.salesStatement')->with([
            'sale' => $sale,
            'saleStatements' => $saleStatements,
            'totalSaleAmount' => $sell_details['total_sell_exc_tax']
        ]);
    }

    public function debitNote(Request $request)
    {
        if($request->ajax())
        {
            if(auth()->user()->can('accounts.debit'))
            {
                $debitNotes = DebitNote::all();
            }else{
                $debitNotes = auth()->user()->debitNotes;
            }

            return DataTables::of($debitNotes)
                ->addColumn('sale',function ($data){
                    return $data->saleRep->fullName();
                })
                ->editColumn('amount',function ($data){
                    return 'KES '.number_format($data->amount,2);
                })
                ->make(true);
        }

        if(auth()->user()->can('accounts.debit'))
        {
            $sale = "All";
            $totalDebitAmount = DebitNote::all()->sum('amount');
        }else{
            $sale = auth()->user()->fullName();
            $totalDebitAmount = $sale->debitNotes->sum('amount');
        }



        return view('accounts.debitNote')->with(
            [
                'sale'=>$sale,
                'totalDebitAmount' => $totalDebitAmount
            ]
        );
    }

    public function creditNote(Request $request)
    {
        if($request->ajax())
        {
            if(auth()->user()->can('accounts.credit'))
            {
                $creditNotes = CreditNote::all();
            }else{
                $creditNotes = auth()->user()->creditNotes;
            }

            return DataTables::of($creditNotes)
                ->addColumn('sale',function ($data){
                    return $data->saleRep->fullName();
                })
                ->editColumn('amount',function ($data){
                    return 'KES '.number_format($data->amount,2);
                })
                ->make(true);
        }

        if(auth()->user()->can('accounts.credit'))
        {
            $sale = "All";
            $totalCreditAmount = CreditNote::all()->sum('amount');
        }else{
            $sale = auth()->user()->fullName();
            $totalCreditAmount = $sale->creditNotes->sum('amount');
        }




        return view('accounts.creditNote')->with(
            [
                'sale'=>$sale,
                'totalCreditAmount' => $totalCreditAmount
            ]
        );
    }

    public function addDebitNote(Request $request)
    {
        $sales = User::all();

        return view('accounts.__addDebitNote',['sales' => $sales])->render();
    }

    public function storeDebitNote(Request $request)
    {
        DebitNote::create(
            [
                'sale_rep_id' => $request->sale_rep_id,
                'added_by' => auth()->id(),
                'amount' => $request->amount,
                'description' => $request->description,
            ]
        );

        (new SaleStatement())->addDebit($request->amount,$request->description,$request->sale_rep_id);

        return back()->with(['success' => 1,'msg' => 'Successfully added debit note']);
    }

    public function storeCreditNote(Request $request)
    {
        CreditNote::create(
            [
                'sale_rep_id' => $request->sale_rep_id,
                'amount' => $request->amount,
                'added_by' => auth()->id(),
                'description' => $request->description
            ]
        );

        (new SaleStatement())->addCredit($request->amount,$request->description,$request->sale_rep_id);

        return back()->with(['success'=>1,'msg' => 'Successfully added credit note']);
    }

    public function addCreditNote()
    {
        $sales = User::all();

        return view('accounts.__addCreditNote',['sales' => $sales])->render();
    }

}
