<?php

namespace App\Http\Controllers;

use App\BusinessLocation;
use App\Product;
use App\RegionalProduct;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RegionalProductController extends Controller
{
    //
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $regionalProducts = RegionalProduct::all();

            return DataTables::of($regionalProducts)
                ->addColumn('product',function ($regionalProduct)
                {
                    return $regionalProduct->product->name;
                })
                ->addColumn('location',function ($regionalProduct)
                {
                    return $regionalProduct->location->name;
                })
                ->addColumn('action',function ($regionalProduct)
                {
                    $html = '<div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                 data-toggle="dropdown" aria-expanded="false">actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                    $html .= "<li><a  href='".action('RegionalProductController@delete',['id'=>$regionalProduct->id])."'><i class='fa fa-trash' aria-hidden='true'></i> Delete</a></li>";

                    $html .= "</ul>";

                    return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('regionalProduct.list');
    }

    public function add()
    {
        $products = Product::all();

        $businessLocations = BusinessLocation::all();

        return view('regionalProduct.__addModal',['products' => $products,'businessLocations' => $businessLocations,])->render();
    }

    public function store(Request $request)
    {
        if(RegionalProduct::where(
            [
                'location_id' => $request->region_id,
                'product_id' => $request->product_id
            ]
        )->count())
        {
            return back()->with(['success' => 0,'msg' => 'Regional product price already exists']);
        }

        RegionalProduct::create(
            [
                'product_id' => $request->product_id,
                'location_id' => $request->region_id,
                'amount' => $request->price,
            ]
        );

        return back()->with(['success' => 1,'msg' => 'Added regional product price successfully']);
    }

    public function delete(Request $request)
    {
        RegionalProduct::where(
            [
                'id' => $request->id
            ]
        )->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully delete regional product price']);
    }

}
