<?php

namespace App\Http\Controllers;

use App\Driver;
use App\ExpenseCategory;
use App\LogisticExpenseType;
use App\Order;
use App\Transaction;
use App\Utils\TransactionUtil;
use App\Vehicle;
use App\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
class LogisticController extends Controller
{
    //
    private $transactionUtil;

    public function __construct(TransactionUtil $transactionUtil)
    {
        $this->transactionUtil = $transactionUtil;
    }


    public function index(Request $request)
    {
        if(\request()->ajax())
        {
            if(auth()->user()->can('logistics.view_all_logistics'))
            {
                $logistics = Order::where(function ($qry){
                    return $qry->where('status','approved')
                        ->orWhere('status','partially approved');
                });
            }else{

                $logistics = Order::where(function ($qry){
                    return $qry->where('status','approved')
                        ->orWhere('status','partially approved');
                })->where('added_by',auth()->id());
            }




            if(isset($request->status) && $request->status == 'loaded')
            {

                $logistics = $logistics->where('logistic_status' , 'loaded');
            }
            elseif(isset($request->status) && $request->status == 'dispatched')
            {

                $logistics = $logistics->where('logistic_status' , 'dispatched');
            }elseif(isset($request->status) && $request->status == 'received')
            {

                $logistics = $logistics->where('logistic_status' , 'received');
            }elseif(isset($request->status) && $request->status == 'sr_loading_bay_tab')
            {

                $logistics = $logistics->where('logistic_status' , 'sent_to_loading_bay');
            }else{
                $logistics = $logistics->where('logistic_status' , 'pending');
            }

            if(isset($request->start_date) && isset($request->end_date))
            {
                $logistics = $logistics->whereBetween('created_at',[Carbon::parse($request->start_date)->startOfDay() ,Carbon::parse($request->end_date)->endOfDay()]);
            }


            return DataTables::of($logistics)
                ->addColumn('order_id',function ($order)
                {
                    return $order->getId();
                })
                ->addColumn('ordered_by',function ($order)
                {
                    return $order->addedby->fullName();
                })
                ->editColumn('logistic_status',function ($order){
                    return str_replace('_',' ',$order->logistic_status);
                })
                ->addColumn('origin',function ($order){
                    return $order->locationFrom->name;
                })
                ->addColumn('destination',function ($order){
                    return $order->locationTo->name;
                })
                ->addColumn('action',function ($q){

                    $html = '<div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                 data-toggle="dropdown" aria-expanded="false">actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                        $html .= "<li><a view-details href='".url('logistics/details/'.$q->id)."'><i class='fa fa-info' aria-hidden='true'></i>View Details</a></li>";
                        if($q->logistic_status == 'pending')
                        {
                            $html .= "<li><a view-details href='".url('logistics/load/'.$q->id)."'><i class='fa fa-plus' aria-hidden='true'></i>Approve loading</a></li>";
                        }elseif($q->logistic_status == 'loaded')
                        {
                            $html .= "<li><a href='".url('logistics/send-to-loading-bay/'.$q->id)."'><i class='fa fa-arrow-right' aria-hidden='true'></i> Send To Loading Bay</a></li>";
                        }elseif($q->logistic_status == 'sent_to_loading_bay')
                        {
                            $html .= "<li><a href='".url('logistics/dispatch/'.$q->id)."'><i class='fa fa-arrow-right' aria-hidden='true'></i> Dispatch</a></li>";
                        }
                        elseif($q->logistic_status == 'dispatched')
                        {
                            if(auth()->id() == $q->added_by){
                                $html .= "<li><a href='".url('logistics/receive/'.$q->id)."'><i class='fa fa-arrow-right' aria-hidden='true'></i> Receive</a></li>";
                            }

                            $html .= "<li><a print-invoice-note  href='".url('order/print-delivery-note/'.$q->id)."'><i class='fa fa-print' aria-hidden='true'></i> Delivery Note</a></li>";
                        }

                        return $html;

            })
                ->make(true);
        }



        if(auth()->user()->can('logistics.view_all_logistics'))
        {
            $pendingOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('logistic_status','pending')->count();
            $loadedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('logistic_status','loaded')->count();
            $dispatchedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('logistic_status','dispatched')->count();
            $receivedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('logistic_status','received')->count();

        }else{

            $pendingOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('added_by',auth()->id())->where('logistic_status','pending')->count();

            $loadedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('added_by',auth()->id())->where('logistic_status','loaded')->count();

            $dispatchedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('added_by',auth()->id())->where('logistic_status','dispatched')->count();

            $receivedOrders = Order::where(function ($qry){
                return $qry->where('status','approved')
                    ->orWhere('status','partially approved');
            })->where('added_by',auth()->id())->where('logistic_status','received')->count();
        }





        return view('logistics.index',compact('pendingOrders',
                    'loadedOrders',
                    'dispatchedOrders',
                    'receivedOrders'));
    }
    
    
    public function orderLoad($id)
    {


        $order = Order::where('id',$id)->first();

        $warehouses = Warehouse::all();
        $vehicles = Vehicle::all();
        $expenseCategories = ExpenseCategory::all();
        $drivers = Driver::all();

        return view('logistics.__load',
            ['order'=>$order,'warehouses' => $warehouses,'vehicles' => $vehicles,
                'expenseCategories' => $expenseCategories,'drivers' => $drivers]
        )->render();
    }

    public function warehouse()
    {
        if(request()->ajax())
        {
            $warehouses = Warehouse::all();


            return DataTables::of($warehouses)

                ->addColumn('action',function ($warehouse)
                {
                    return '<a href="'.action('LogisticController@deleteWarehouse',['id' => $warehouse->id]).'" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                })
                ->addColumn('status',function ($warehouse)
                {
                    return $warehouse->active?'active':'inactive';
                })
                ->make(true);
        }
        return view('logistics.warehouse');
    }

    public function addWarehouse()
    {
        return view('logistics.__addWarehouse')->render();
    }

    public function storeWarehouse(Request $request)
    {
        $data = $request->only(['name','description']);
        $data['added_by'] = auth()->id();
        Warehouse::create(
            $data
        );

        return back()->with(['success'=>1,'msg'=>'Successfully added a warehouse']);
    }

    public function deleteWarehouse(Request $request)
    {
        Warehouse::where(['id' => $request->id])
            ->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted warehouse']);
    }

    public function vehicle()
    {
        if(request()->ajax())
        {
            $vehicles = Vehicle::all();


            return DataTables::of($vehicles)

                ->addColumn('action',function ($vehicle)
                {
                    return '<a  href="'.action('LogisticController@deleteVehicle',['id' => $vehicle->id]).'" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                })

                ->make(true);
        }
        return view('logistics.vehicle');
    }

    public function addVehicle()
    {
        return view('logistics.__addVehicle')->render();
    }

    public function storeVehicle(Request $request)
    {
        $data = ($request->only(['name','number_plate','description']));

        Vehicle::create(
            $data
        );

        return back()->with(['success' => 1,'msg' => 'Successfully added a new vehicle']);
    }

    public function deleteVehicle(Request $request)
    {
        Vehicle::where(['id' => $request->id])->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted vehicle']);
    }

    public function driver()
    {
        if(request()->ajax())
        {
            $drivers = Driver::all();

            return DataTables::of($drivers)
                ->addColumn('action',function ($driver)
                {
                    return '<a class="btn btn-danger" href="'.action('LogisticController@deleteDriver',['id' => $driver->id]).'"><i class="fa fa-trash"></i></a>';
                })
                ->make(true);
        }

        return view('logistics.driver');
    }

    public function addDriver()
    {
        return view('logistics.__addDriver')->render();
    }

    public function storeDriver(Request $request)
    {
        $data = $request->only(['description','full_name','phone']);
        Driver::create($data);

        return back()->with(['success' => 1,'msg' => 'Successfully added a driver']);
    }

    public function deleteDriver(Request $request)
    {
        Driver::where(
            [
                'id' => $request->id,
            ]
        )->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted driver']);
    }

    public function expense()
    {
        if(request()->ajax())
        {
            $expenseTypes = LogisticExpenseType::all();

            return DataTables::of($expenseTypes)
                ->addColumn('action',function ($expense)
                {
                    return '<a class="btn btn-danger" href="'.action('LogisticController@deleteExpense',['id' => $expense->id]).'"><i class="fa fa-trash"></i></a>';
                })
                ->make(true);
        }
        return view('logistics.expense');
    }

    public function addExpense()
    {
        return view('logistics.__addExpense')->render();
    }

    public function deleteExpense(Request $request)
    {

        LogisticExpenseType::where(['id' => $request->id])->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted expense type']);
    }

    public function storeExpense(Request $request)
    {
        $data = $request->only(['description','name']);

        LogisticExpenseType::create($data);

        return back()->with(['success' => 1,'msg' => 'Successfully added expense type']);
    }

    public function PostLogistic(Request $request)
    {


        try{
            $order = Order::where(['id' => $request->orderId])->first();
            if(!is_null($order))
            {
                $order->update([
                    'logistic_status' => 'loaded',
                    'loaded_at' => now(),
                    'warehouse_id' => $request->warehouse_id,
                    'vehicle_id' => $request->vehicle_id,
                    'driver_id' => $request->driver_id,

                ]);

                $business_id = request()->session()->get('user.business_id');

                $index = 0;
                if(!empty($request->expense))
                {
                    foreach ($request->expense as $item) {



                        $transaction_data = $request->only([ 'ref_no', 'transaction_date', 'location_id', 'final_total', 'payment_status', 'expense_for', 'additional_notes', 'expense_category_id']);

                        $business_id = $request->session()->get('user.business_id');
                        $user_id = $request->session()->get('user.id');
                        $transaction_data['business_id'] = $business_id;
                        $transaction_data['created_by'] = $user_id;
                        $transaction_data['type'] = 'expense';
                        $transaction_data['expense_category_id'] = $item;
                        $transaction_data['status'] = 'final';
                        $transaction_data['location_id'] = $order->destination_location_id;
                        $transaction_data['transaction_date'] = now();
                        $transaction_data['final_total'] = $request['cost'][$index];
                        $transaction_data['additional_notes'] =
                            'Cost incurred moving order #'.$order->id.' from '.$order->locationFrom->name.' to '.$order->locationTo->name;

                        //Update reference count
                        $ref_count = $this->transactionUtil->setAndGetReferenceCount('expense');
                        $transaction_data['ref_no'] = $this->transactionUtil->generateReferenceNumber('expense', $ref_count);

                        Transaction::create($transaction_data);

                        $index +=1;
                    }
                }

                $message = 'Successfully loaded';

                if ($request->action == 'load_and_dispatch')
                {
                    return $this->dispatchOrder($request->orderId);
                }
            }

            return back()->with(['success' => 1,'msg' => $message]);

        }catch (\Exception $e)
        {
            dd($e);
        }


    }

    public function sendToLoadingBay($id)
    {
        Order::where([
            'id' => $id
        ])->update([
            'logistic_status' => 'sent_to_loading_bay',
            'sent_loading_bay_at' => now()
        ]);

        return back()->with(['success' => 1,'msg' => 'Successfully sent to loading bay']);
    }

    public function dispatchOrder($id)
    {
        $order = Order::where(['id' => $id])->
            first();

        if(!is_null($order)) {

            $order->update([
                'logistic_status' => 'dispatched',
                'dispatched_at' => now()
            ]);

            //send msg to sales rep.
            try {

                $message = 'Your order#'.$order->id.' containing '.number_format($order->orderItems->count()).
                    ' items has been dispatched by '.auth()->user()->fullName().
                    '. Vehicle: '. $order->vehicle->name().
                    ', Driver: '. $order->driver->full_name;


                sendSms($order->addedBy->phone,$message);

            } catch (\Exception $e) {
                //
            }
        }


        return back()->with(['success' => 1,'msg' => 'Successfully dispatched']);

    }

    public function receiveOrder($id)
    {
        Order::where(['id' => $id])
            ->update([
                'logistic_status' => 'received',
                'received_at' => now()
            ]);

        $order = Order::where(['id' => $id])->first();

        if(!is_null($order))
        {

            $message = 'Order #'.$id.' has been delivered to . '.$order->addedBy->fullName().
                ' at '.$order->locationTo->name.'. Vehicle: '.$order->vehicle->name().
                ', driver: '.$order->driver->full_name;

                sendSmsToMany(getOrderDeliveryMessageReceivers(),$message);
        }


        return back()->with(['success' => 1,'msg' => 'Successfully received']);

    }

    public function details($id)
    {
        $order = Order::where(['id' => $id])->first();

        return view('logistics.__logisticDetails',['order' => $order])->render();
    }
}
