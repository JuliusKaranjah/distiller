<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessLocation;
use App\Order;
use App\OrderItem;
use App\Product;
use App\SaleMaraBottle;
use App\Transaction;
use App\Utils\ProductUtil;
use App\Utils\TransactionUtil;
use App\VariationLocationDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Repo\AfricasTalking;
use Repo\AfricasTalkingHandler;
use Repo\Invoice;
use Repo\MaraBottle;
use Yajra\DataTables\DataTables;

class OrderController extends Controller
{
        //
    protected  $productUtil;
    protected  $transactionUtil;
    public function __construct(ProductUtil $productUtil, TransactionUtil $transactionUtil)
    {
        $this->productUtil = $productUtil;
        $this->transactionUtil = $transactionUtil;
    }



    public function index(Request $request)
    {
        //if(auth()->user()->can('order.view'))
        {

            if(request()->ajax())
            {

                $orders = Order::where([]);

                if(isset($request->start_date) && isset($request->end_date))
                {
                    $orders = $orders
                        ->where('created_at','>=',Carbon::parse($request->start_date)->startOfDay())
                        ->where('created_at','<=',Carbon::parse($request->end_date)->endOfDay());
                }
                if(!auth()->user()->can('order.view_all_orders'))
                {
                    $orders = $orders->where(['added_by'=>auth()->id()]);
                }


                $orders = $orders->get();

                return DataTables::of($orders)
                    ->editColumn('ordered_by',function ($q){
                        return $q->addedBy->fullName();
                    })
                    ->editColumn('action_at',function ($q){

                        return $q->action_at?$q->action_at->toDayDateTimeString():'--';
                    })
                    ->editColumn('id',function ($q){
                        return $q->getId();
                    })

                    ->editColumn('status',function ($q){

                        $badgeClass = 'default';
                        if($q->status == 'approved')
                        {
                            $badgeClass = 'success';
                        }elseif ($q->status == 'partially approved')
                        {
                            $badgeClass = 'primary';
                        }elseif ($q->status == 'declined')
                        {
                            $badgeClass = 'danger';
                        }


                        return "<span class='badge badge-".$badgeClass."'>$q->status</span>";
                    })

                    ->editColumn('accountant_status',function ($q){

                        $badgeClass = 'default';
                        if($q->accountant_status == 'approved')
                        {
                            $badgeClass = 'success';
                        }elseif ($q->accountant_status == 'declined')
                        {
                            $badgeClass = 'danger';
                        }


                        return "<span class='badge badge-".$badgeClass."'>$q->accountant_status</span>";
                    })
                    ->addColumn('ordered_at',function ($q){
                        return $q->created_at;
                    })

                    ->addColumn('action',function ($q){
                        $html =

                            '<div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                data-toggle="dropdown" aria-expanded="false">
                                actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                        </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                        if (auth()->user()->can('order.order_accountant_process'))
                        {
                            $html .="<li><a view-details href='".url('order/accountant-approval/'.$q->id)."'><i class='fa fa-money' aria-hidden='true'></i>Accountant Approval</a></li>";
                        }


                        $html .="<li><a view-details href='".url('order/'.$q->id)."'><i class='fa fa-eye' aria-hidden='true'></i>View Details</a></li>";

                        if($q->status == 'approved' || $q->status == 'partially approved')
                        {
                            $html .="<li><a print-invoice-note href='".url('order/print-invoice-note/'.$q->id)."'><i class='fa fa-print' aria-hidden='true'></i>Print Invoice Note</a></li>";
                        }
                        if($q->status == 'approved' || $q->status == 'partially approved')
                        {
                            $html .="<li><a print-delivery-note href='".url('order/print-delivery-note/'.$q->id)."'><i class='fa fa-print' aria-hidden='true'></i>Print Delivery Note</a></li>";
                        }

                        $html .="</ul></div>";

                    return $html;

                    })


                    ->rawColumns(['id', 'ordered_by', 'status', 'accountant_status', 'action_at','ordered_at', 'action'])
                    ->make(true);
            }




            return view('order.list');
        }

//        abort(403, 'Unauthorized action.');
    }
    public function viewOrder($id)
    {
        $order = Order::where('id',$id)->first();

        return view('order.__details',['order'=>$order])->render();

    }
    public function create()
    {
        $locations = BusinessLocation::all();

        $permittedLocations = auth()->user()->permitted_locations();

        if($permittedLocations == 'all')
        {
            $permittedLocations = $locations;
        }else{
            $permittedLocations = BusinessLocation::whereIn('id',$permittedLocations)->get();
        }



        return view('order.create')->with(['locations' => $locations,'permittedLocations' => $permittedLocations]);
    }

    public function store(Request $request)
    {



        if(!isset($request->purchases) || empty($request->purchases))
        {


            return back()->with(['success' => 0,'msg' => 'Your order is empty.']);
        }


        if(!isset($request->destination_location_id) || $request->destination_location_id==null || !isset($request->origin_location_id) || $request->origin_location_id ==null)
        {
            return back()->with(['success' => 0,'msg' => 'Please provide destination and origin of the order']);
        }


        //check if the user has exceeded their mara bottle limit//
        //required bottle and credit is depleted!
        foreach ($request->purchases as $purchase) {

            if(!is_null($bottSet = productRequireMaraBottle($purchase['product_id'])))
            {
                $unclearedMaraBottles = ($user= auth()->user())->unclearedMaraBottles();

                if(($bottSet->quantity * doubleval($purchase['quantity']) + $unclearedMaraBottles) > ($limit = ($user)->bottleLimit->amount))
                {

                    $message = "Your allowed mara sellable bottle credit limit is ".number_format($limit - $unclearedMaraBottles);

                    if($unclearedMaraBottles > 0)
                    {
                        $message .=". Please clear your uncollected bottles : ".number_format($unclearedMaraBottles).' bottles';
                    }

                    sendSms($user->phone,$message);

                    return back()->with(['success'=>0,'msg' => "Your allowed mara sellable bottle credit limit is ".number_format($limit - $unclearedMaraBottles)]);
                }
            }
        }


        $order = Order::create(
            [
                'added_by' => auth()->id(),
                'destination_location_id' => $request->destination_location_id,
                'origin_location_id' => $request->origin_location_id,
                'status' => 'pending',
            ]
        );

        if($request->purchases)
        {
            foreach ($request->purchases as $purchase) {

                $order->orderItems()->create(
                    [
                        'product_id' => $purchase['product_id'],
                        'quantity' => $purchase['quantity'],
                        'variation_id' => $purchase['variation_id'],
                        'price' => Product::where(['id' => $purchase['product_id']])->first()->getProductSellPrice($request->destination_location_id),
                    ]
                );

                $qty = floatval(@$this->productUtil->getDetailsFromVariation($purchase['product_id'],
                    getBusiness()['id'],getBusinessOrderOriginLocationId(),true)['qty_available']);

                if($qty <=0)
                {
                    $product = Product::whereId($purchase['product_id'])->first();

                        $message = 'Quantity alert. Stock level for \''.$product->sku.'\' is low. Available quantity is '.number_format($qty);
                        sendSmsToMany(getProductionMessageReceivers(),$message);


                }

            }
        }


        $message = 'New order #'.$order->id.' placed by '.auth()->user()->fullName().
            ' from '.@BusinessLocation::where('id',$request->destination_location_id)->first()->name.
            '. The order contains : ';


        $comma = '';
        foreach ($order->orderItems as $item) {
            $message .=$comma.$item->product->sku.' - '.number_format($item->quantity);
            $comma = ', ';
        }

        (sendSmsToMany(getNewOrderMessageReceivers(),
            $message));


        return back()->with(['success' => 1,'msg' => 'Successfully made order']);

    }

    public function declineOrder(Request $request)
    {
        $order = Order::where(['id' => $request->order_id])->first();

        $order->update(['status'=>'declined','note' => $request->reason,'action_at' => now()]);


        foreach ($order->orderItems as $orderItem) {
            $orderItem->update(['status'=>'declined']);
        }

        try{

            $message = 'Your order #'.$order->id.
                ' has been declined. All '.number_format($order->orderItems->count()).
                ' products in the order have been declined. Reason: '.$request->reason;

            sendSms($order->addedBy->phone,
                $message);

        }catch (\Exception $e)
        {
            //
        }

        return back()->with(['success' => 1,'msg' => 'Successfully declined']);
    }

    public function transferProductItems(Request $request,$order,$orderTotalAmount,$products)
    {

        $totalAmount = $orderTotalAmount;

        /*purchase and sell lines..*/

        $input_data = $request->only([ 'location_id', 'ref_no', 'transaction_date', 'additional_notes', 'shipping_charges', 'final_total']);

        $business_id = $request->session()->get('user.business_id');
        $user_id = $request->session()->get('user.id');

//        $input_data['final_total'] = $this->productUtil->num_uf($input_data['final_total']);
//        $input_data['total_before_tax'] = $input_data['final_total'];

        $input_data['type'] = 'sell_transfer';
        $input_data['business_id'] = $business_id;
        $input_data['created_by'] = $user_id;
        $input_data['transaction_date'] = now();
        $input_data['shipping_charges'] = now();
        $input_data['status'] = 'final';
        $input_data['payment_status'] = 'paid';

        //Update reference count
        $ref_count = $this->productUtil->setAndGetReferenceCount('stock_transfer');
        //Generate reference number
        if(empty($input_data['ref_no'])){
            $input_data['ref_no'] = $this->productUtil->generateReferenceNumber('stock_transfer', $ref_count);
        }


        $sell_lines = array();
        $purchase_lines = array();
        if(!empty($products)){
            foreach ($products as $product) {
                $sell_line_arr = array(
                    'product_id' => $product['product_id'],
                    'variation_id' => $product['variation_id'],
                    'quantity' => $this->productUtil->num_uf($product['quantity']),
                    'item_tax' => 0,
                    'tax_id' => null);

                $purchase_line_arr = $sell_line_arr;
                $sell_line_arr['unit_price'] = $this->productUtil->num_uf($product['unit_price']);
                $sell_line_arr['unit_price_inc_tax'] = $sell_line_arr['unit_price'];

                $purchase_line_arr['purchase_price'] = $sell_line_arr['unit_price'];
                $purchase_line_arr['purchase_price_inc_tax'] = $sell_line_arr['unit_price'];

                $sell_lines[] = $sell_line_arr;
                $purchase_lines[] = $purchase_line_arr;
            }
        }





        //Create Sell Transfer transaction
        $input_data =
            [
                'location_id' => $order->origin_location_id,
                'ref_no' => strtoupper(str_random(12)),
                'transaction_date' => now()->format('Y-m-d'),
                'shipping_charges' => 0,
                'final_total' => $totalAmount,
                'total_before_tax' => $totalAmount,
                "type" => "sell_transfer",
                "business_id" => 1,
                "created_by" => auth()->id(),
                "status" => "final",
                "payment_status" => "paid",
            ];


        $sell_transfer = Transaction::create($input_data);

        //Create Purchase Transfer at transfer location
        $input_data['type'] = 'purchase_transfer';
        $input_data['status'] = 'received';
        $input_data['location_id'] = $order->destination_location_id;
        $input_data['transfer_parent_id'] = $sell_transfer->id;

        $purchase_transfer = Transaction::create($input_data);


        //Sell Product from first location
        if(!empty($sell_lines)){
            $this->transactionUtil->createOrUpdateSellLines($sell_transfer, $sell_lines, $input_data['location_id']);
        }

        //Purchase product in second location

        if(!empty($purchase_lines)){
            $purchase_transfer->purchase_lines()->createMany($purchase_lines);
        }


        //Decrease product stock from sell location
        //And increase product stock at purchase location
        foreach ($products as $product) {
            $this->productUtil->decreaseProductQuantity(
                $product['product_id'],
                $product['variation_id'],
                $sell_transfer->location_id,
                $this->productUtil->num_uf($product['quantity'])
            );

            $this->productUtil->updateProductQuantity(
                $purchase_transfer->location_id,
                $product['product_id'],
                $product['variation_id'],
                $this->productUtil->num_uf($product['quantity'])
            );
        }


        $business_id = $request->session()->get('user.business_id');

        //Map sell lines with purchase lines
        $business = ['id' => $business_id,
            'accounting_method' => $request->session()->get('business.accounting_method'),
            'location_id' => $sell_transfer->location_id
        ];
        $this->transactionUtil->mapPurchaseSell($business, $sell_transfer->sell_lines, 'purchase');

        $output = array('success' => 1,
            'msg' => __('lang_v1.stock_transfer_added_successfully')
        );

        //check stock alert for headquarter stock.
        foreach ($products as $product)
        {
            $product = Product::whereId($product['product_id'])->first();

            $qty = floatval(@$this->productUtil->getDetailsFromVariation($product->id,
                getBusiness()['id'],getBusinessOrderOriginLocationId(),true)['qty_available']);

            if($product->alert_quantity >= $qty)
            {
                //send alert level notification..
                $message = 'Quantity alert. Stock level for \''.$product->sku.'\' is low. Available quantity is '.number_format($qty);
                sendSmsToMany(getProductionMessageReceivers(),$message);
            }

        }

        return $output;

    }


    public function approveOrder(Request $request)
    {


        $order = Order::where('id',$request->order_id)->first();

        $saleRepInvoice = Invoice::addSaleRepInvoice($order);

        $totalAmount = 0;
        $products = [];

        if($request->orderItemId)
        {
            $index = 0;
            foreach ($request->orderItemId as $item) {
                $orderItem = OrderItem::where(['id' => $item])->first();

                $product = $orderItem->product;

                $qty = floatval(@$this->productUtil->getDetailsFromVariation($product->id,
                    getBusiness()['id'],getBusinessOrderOriginLocationId(),true)['qty_available']);

                if($qty>=$request->orderItemAmount[$index] && ($request->orderItemAmount[$index]>0) && !is_null($request->orderItemAmount[$index]))
                {
                    //check if the product requires mara ice bottle..
                    if($productMaraBottle = productRequireMaraBottle($orderItem->product_id))
                    {
                        //add to sales
                        MaraBottle::addMoreBottle($order->addedBy,$request->orderItemAmount[$index] * $productMaraBottle->quantity);
                    }

                    $products[] =
                        [
                            'product_id' => $orderItem->product_id,
                            'variation_id' => $orderItem->variation_id,
                            'enable_stock' => $orderItem->product->enable_stock,
                            'quantity' => $request->orderItemAmount[$index],
                            'unit_price' => $price = $request->unit_price[$index]/*$orderItem->product->getProductSellPrice()*/,
                            'price' => $price?$request->orderItemAmount[$index] * $price:0,
                        ];

                    $orderItem->update(
                        [
                            'status' => 'approved',
                            'price' => $price,
                            'quantity' => $request->orderItemAmount[$index]
                        ]);

                    $totalAmount += $price?$request->orderItemAmount[$index] * $price:0;

                }


                $index +=1;
            }
        }



        if(sizeof($products)>0)
        {
            $this->transferProductItems($request,$order,$totalAmount,$products);
        }




        $saleRepInvoice->update(['amount' => $totalAmount]);
        if(sizeof($products)>0){

            $orderStatus = sizeof($products) == $order->orderItems->count()?'approved':'partially approved';

            $order->update(['status' => $orderStatus,'action_at'=>now()]);

            try{

                $message = 'Your order #'.$order->id.' containing: ';

                $comma = '';
                foreach ($products as $product) {
                    $message .=$comma.Product::find($product['product_id'])->sku.' - '
                        .number_format($product['quantity']).', ';
                    $comma = ', ';
                }
                $message .=' has been approved by '.auth()->user()->fullName();

                sendSms($order->addedBy->phone,$message);


            }catch (\Exception $e)
            {
                //
            }

            return back()->with(['success' => 1,'msg' => 'Successfully approved order']);
        }

        return back()->with(['success' => 0,'msg' => "".(sizeof($request->orderItemId)>1)?'products are':'product is'." out of stock"]);


    }

    public function processOrder(Request $request)
    {

        $order = Order::where(['id' => $request->orderId])->first();


        if($request->orderAction == 'deleteOrder')
        {
            if($order->count())
            {
                $order->delete();
            }

            return back()->with(['success' => 0,'msg' => 'Successfully deleted order']);
        }

        if($request->orderAction == 'DeleteDeclinedOrderItem')
        {
            foreach ($order->orderItems->where('status','pending') as $item) {
                $item->delete();
            }
            foreach ($order->orderItems->where('status','declined') as $item) {
                $item->delete();
            }

            $order->update(['status' => 'approved']);

            return back()->with(['success' => 1,'msg' => 'Successfully deleted declined products']);
        }



        $itemsCount =  0;



        if(sizeof($request->orderItemId) && ($request->orderAction == 'approvedDeclinedOrder' || $request->orderAction == 'approvedDeclinedOrderItem'))
        {


            $totalAmount = 0;

            $products = [];

            if($order->orderItems->count())
            {
                $itemsCount = $order->orderItems->where('status','declined')->count();

                for ($index = 0; $index< sizeof($request->orderItemId); $index++) {

                    $orderItem = OrderItem::where(['id' => $request->orderItemId[$index]])->first();

                    $product = $orderItem->product;

                    $qty = floatval(@$this->productUtil->getDetailsFromVariation($product->id,
                        getBusiness()['id'],getBusinessOrderOriginLocationId(),true)['qty_available']);



                    if((floatval($qty)>=floatval($request->orderItemAmount[$index])) && $request->orderItemAmount[$index]>0)
                    {
                        $orderItem->update(['status' => 'approved','quantity' => $request->orderItemAmount[$index]]);

                        $products[] =
                            [
                                'product_id' => $orderItem->product_id,
                                'variation_id' => $orderItem->variation_id,
                                'enable_stock' => $orderItem->product->enable_stock,
                                'quantity' => $request->orderItemAmount[$index],
                                'unit_price' => $price = $request->unit_price[$index],
                                'price' => $subTotal = $price?$price*$request->orderItemAmount[$index]:0,
                            ];

                        $orderItem->update(['status' => 'approved','quantity' => $request->orderItemAmount[$index],'price' => $price]);

                        $totalAmount += $subTotal;
                    }
                }
            }



            if(sizeof($products)>0)
            {
                $this->transferProductItems($request,$order,$totalAmount,$products);
            }


            $saleRepInvoice = Invoice::addSaleRepInvoice($order);


            $saleRepInvoice->update(['amount' => $totalAmount]);


           if(sizeof($products) > 0)
           {
               $orderStatus = !$order->orderItems->where('status','decline')->count() && !$order->orderItems->where('status','pending')->count()?'approved':'partially approved';

               $order->update(['status' => $orderStatus,'action_at'=>now()]);

               try{

                   $message = 'Your order #'.$order->id.' has been approved Containing: ';
                   $comma = '';
                   foreach ($order->orderItems as $orderItem) {
                       $message .=$comma.$orderItem->product->code.' -'.number_format($orderItem->quantity).', ';
                       $comma=', ';
                       }



                   if($request->orderAction == 'approvedDeclinedOrder')
                   {
                       $message = 'Your order #'.$order->id.' has been approved All '.$order->orderItems->count().' products in the order has been approved by '
                           .auth()->user()->fullName();
                   }else{
                       $message = 'Your order #'.$order->id.' has been approved ';

                       $message .= 'Only '.number_format(sizeof($products)).
                           ' products has been approved by '.auth()->user()->fullName();


                   }

                   sendSms($order->addedBy->phone,$message);


               }catch (\Exception $e)
               {
                   //
               }

               return back()->with(['success' => 1,'msg' => 'Successfully approved order']);
           }
            return back()->with(['success' => 0,'msg' => 'Out of stock']);
        }

    }

    public function getPrintInvoiceNoteContent($id)
    {
        $order = Order::whereId($id)->first();

        return view('order.InvoiceNote',['order' => $order])->render();
    }

    public function getPrintDeliveryNoteContent($id)
    {
        $order = Order::whereId($id)->first();

        return view('order.deliveryNote',['order' => $order])->render();
    }

    public function orderAccountantApproval($id)
    {
        $order = Order::where('id',$id)->first();

        return view('order.__accountantDetail',['order'=>$order])->render();
    }

    public function orderAccountantDecline(Request $request)
    {

        $order = Order::where('id',$request->order_id)->first();

        ($order->update(['accountant_status' => 'declined','note' => $request->reason]));


        try{

            $message = 'Your order #'.$order->id.
                ' has been declined. Reason: '.$request->reason;

            sendSms($order->addedBy->phone,
                $message);

        }catch (\Exception $e)
        {
            //
        }

        return back()->with(['success' => 1,'msg' => 'Successfully declined order']);

    }

    public function accountantApproveOrder(Request $request)
    {
        $order = Order::where(['id' => $request->order_id])->first();

        if(!is_null($order))
        {

//            OrderItem::whereNotIn('id',$request['orderItemId'])->delete();

            $index = 0;
            if(sizeof($request['orderItemId'])>0)
            {
                foreach ($request['orderItemId'] as $orderItemId) {

                $orderItem = OrderItem::where(['id' => $orderItemId]);

                if($request['orderItemAmount'][$index] <=0)
                {
                    $orderItem->delete();
                }else{
                    $orderItem->update(['quantity' => $request['orderItemAmount'][$index]]);
                }

                $index += 1;
            }
            $order->update(
                [
                    'accountant_status' => 'approved'
                ]
            );
        }
        }

        return back()->with(['status' => 1,'msg' => 'Successfully approved order','action_at' => now()]);
    }

}
