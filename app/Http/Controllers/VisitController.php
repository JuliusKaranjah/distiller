<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Visit;
use App\VisitReason;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class VisitController extends Controller
{
    //
    public function index()
    {
        if(request()->ajax())
        {
            if(auth()->user()->can('visit.view_all'))
            {
                $visits = Visit::all();
            }else
            {
                $visits = Visit::where(['sale_rep_id' => auth()->id()])->get();
            }

            return DataTables::of($visits)
                ->addColumn('reason',function ($v){
                    return $v->reason->name;
                })
                ->editColumn('id',function ($v)
                {
                    return $v->getId();
                })
                ->addColumn('customer',function ($v){
                    return @$v->customer->name;
                })
                ->addColumn('sale',function ($v){
                    return $v->saleRep->fullName();
                })
                ->make(true);
        }

        return view('visit.list');
    }

    public function create()
    {
        $customers = Contact::whereCreatedBy(auth()->id())->get();

        $reasons = VisitReason::all();

        return view('visit.create',compact('customers','reasons'));
    }

    public function store(Request $request)
    {

        $geolocation = @$_COOKIE["fbdata"];

        Visit::create([
            'customer_id' => $request->customer_id,
            'sale_rep_id' => auth()->id(),
            'reason_id' => $request->reason,
            'geolocation' => $geolocation,
            'findings' => $request->finding,
            'conclusion' => $request->conclusion,
            'description' => $request->description,
        ]);


        return Redirect('visit')->with(['success' => 1,'msg' => 'Successfully added a visit']);
    }

    public function delete($id)
    {
        Visit::where([
            'id' => $id
        ])->delete();

        return back()->with(['success' => 1,'Successfully deleted the visit']);
    }

    public function reason()
    {
        if(\request()->ajax())
        {
            $reasons = VisitReason::all();

            return DataTables::of($reasons)
                ->editColumn('added_by',function ($reason)
                {
                    return $reason->addedBy->fullName();
                })
                ->addColumn('action',function ($reason)
                {
                    return '<a href="'.action('VisitController@deleteReason',['id' => $reason->id]).'" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                })

                ->rawColumns(['action'])
                ->make(true);
        }

        return view('visit.reason');
    }

    public function deleteReason(Request $request)
    {
        VisitReason::where(['id' => $request->id])->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted reason']);
    }

    public function addReason()
    {
        return view('visit.__addVisitForm')->render();
    }

    public function storeReason(Request $request)
    {

         VisitReason::create(
             [
                 'name' => $request->name,
                 'description' => $request->description,
                 'added_by' => auth()->id(),
             ]
         );

         return back()->with(['success' => 1,'msg' => 'Successfully added visit reason']);
    }

    public function addCustomer()
    {
        return view('visit.__addCustomerModal')->render();
    }
}
