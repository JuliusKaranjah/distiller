<?php

namespace App\Http\Controllers;

use App\Product;
use App\RequireMaraBottle;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BottleSettingController extends Controller
{
    //

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $bottledProducts = RequireMaraBottle::all();

            return DataTables::of($bottledProducts)
                ->addColumn('product',function ($data){
                    return $data->product->name;
                })
                ->addColumn('action',function ($data){

                            $html = '<div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                     data-toggle="dropdown" aria-expanded="false">actions
                                    <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                    </span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                            $html .= "<li><a href='".action('BottleSettingController@delete',['id' => $data->id])."'><i class='fa fa-trash' aria-hidden='true'></i>Delete</a></li>";
                            $html .= "</ul>";
                            return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        $products = Product::all();

        return view('business.bottleSettings',compact('products'));
    }

    public function delete(Request $request)
    {
        RequireMaraBottle::where(['id' => $request->id])->delete();

        return back()->with(['success' => 1,'msg' => 'Successfully deleted']);
    }

    public function store(Request $request)
    {
        if(RequireMaraBottle::where(['product_id' => $request->product_id])->count())
        {
            return back()->with(['success'=>0,'msg'=>'Already exists in our records']);
        }

        RequireMaraBottle::create(
            [
                'quantity' => $request->amount,
                'product_id' => $request->product_id,
                'added_by' => auth()->id()
            ]
        );

        return back()->with(['success' => 1,'msg' => 'Successfully added']);
    }
}
