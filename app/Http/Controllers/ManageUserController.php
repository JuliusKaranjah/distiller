<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

use DB;

class ManageUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('user.view') && !auth()->user()->can('user.create') ) {
            abort(403, 'Unauthorized action.');
        }

        if(request()->ajax())
        {
            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');


            $users = User::where('business_id',$business_id)
                ->where('id','!=',$user_id)
                ->where('is_cmmsn_agnt', 0)
                ->get();


            return DataTables::of($users)

                ->addColumn('full_name',function ($user)
                {
                    return $user->fullName();
                })
                ->addColumn('role',function ($user)
                {
                    try{
                        return @explode("#", $user->getRoleNames()[0], 2)[0];
                    }catch (\Exception $e)
                    {
                        //
                        return '';

                    }

                })
                ->addColumn('action',function ($user)
                {
                    $html = '';
                    if(auth()->user()->can('user.update'))
                    {
                        $html .= "<a href=".action('ManageUserController@edit', [$user->id])." class='btn btn-xs btn-primary'><i class='glyphicon glyphicon-edit'></i> edit</a>&nbsp;";
                    }

                    if(auth()->user()->can('user.delete'))
                    {
                        $html .= "<button data-href=".action('ManageUserController@destroy', [$user->id])." class='btn btn-xs btn-danger delete_user_button'><i class='glyphicon glyphicon-trash'></i> delete</button>";
                    }

                    return $html;


                })


                ->make(true);

        }



        /*if (request()->ajax() || false) {
            $business_id = request()->session()->get('user.business_id');
            $user_id = request()->session()->get('user.id');

            $users = User::where('business_id', $business_id)
                        ->where('id', '!=', $user_id)
                        ->where('is_cmmsn_agnt', 0)
                        ->select(['id','phone', 'username',
                            DB::raw("CONCAT(COALESCE(surname, ''), ' ', COALESCE(first_name, ''), ' ', COALESCE(last_name, '')) as full_name"), 'email']);

            return Datatables::of($users)
                ->addColumn('role',

                    '{{@explode("#", App\User::find($id)->getRoleNames()[0], 2)[0]}}'
                )
                ->addColumn('phone',function ($user)
                {
                    return $user->phone;
                })
                ->addColumn('action',
                    '@can("user.update")
                    <a href="{{action(\'ManageUserController@edit\', [$id])}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</a>
                        &nbsp;
                        @endcan
                        @can("user.delete")

                        @endcan'
                )
                ->filterColumn('full_name', function($query, $keyword) {
                    $query->whereRaw("CONCAT(COALESCE(surname, ''), ' ', COALESCE(first_name, ''), ' ', COALESCE(last_name, '')) like ?", ["%{$keyword}%"]);
                })
                ->removeColumn('id')
                ->rawColumns(['action','phone'])
                ->make(true);
        }*/

        //$users = User::where(['business_id'=>auth()->user()->business_id])->get();

        return view ('manage_user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->can('user.create') ) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        $roles_array = Role::where('business_id', $business_id)->get()->pluck('name', 'id');
        $roles = array();
        foreach ($roles_array as $key => $value) {
            $roles[$key] = str_replace('#' . $business_id , '', $value);
        }

        $ask_commision_percent = false;
        if(in_array(request()->session()->get('business.sales_cmsn_agnt'), ['logged_in_user', 'user'])){
            $ask_commision_percent = true;
        }

        return view ('manage_user.create')
                    ->with( compact('roles', 'ask_commision_percent') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if (!auth()->user()->can('user.create') ) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $user_details = $request->only(['surname', 'first_name', 'last_name', 'username', 'email', 'password','phone']);
            $business_id = $request->session()->get('user.business_id');
            $user_details['business_id'] = $business_id;
            $user_details['password'] = bcrypt($user_details['password']);

            //Sales commission percentage
            if($request->has('cmmsn_percent')){
                $user_details['cmmsn_percent'] = $request->get('cmmsn_percent');
            } else {
                $user_details['cmmsn_percent'] = 0;
            }

            $user_details['should_redirect_to_dashboard'] = false;

            if($request->should_redirect_to_dashboard)
            {
                $user_details['should_redirect_to_dashboard'] = $request->should_redirect_to_dashboard;
            }

            $user_details['credit_limit'] = doubleval($request->credit_limit);

            //Create the user
            $user = User::create($user_details);

            $role_id = $request->input('role');
            $role = Role::findOrFail($role_id);
            $user->assignRole($role->name);

            $output = array('success' => 1, 
                        'msg' => __("user.user_added")
                    );

        } catch(\Exception $e){
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = array('success' => 0, 
                            'msg' => __("messages.something_went_wrong")
                        );
        }

        return redirect('users')->with('status', $output);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->can('user.view') ) {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('user.update') ) {
            abort(403, 'Unauthorized action.');
        }

        $user = User::findOrFail($id);

        $business_id = request()->session()->get('user.business_id');
        $roles_array = Role::where('business_id', $business_id)->get()->pluck('name', 'id');
        $roles = array();
        foreach ($roles_array as $key => $value) {
            $roles[$key] = str_replace('#' . $business_id , '', $value);
        }

        $ask_commision_percent = false;
        if(in_array(request()->session()->get('business.sales_cmsn_agnt'), ['logged_in_user', 'user'])){
            $ask_commision_percent = true;
        }
        
        return view ('manage_user.edit')
                    ->with( compact('roles', 'user', 'ask_commision_percent') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('user.update') ) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $user_data = $request->only(['surname', 'first_name', 'last_name', 'email','phone']);

            if(!empty($request->input('password'))){
                $user_data['password'] = bcrypt($request->input('password'));
            }

            //Sales commission percentage
            if($request->has('cmmsn_percent')){
                $user_data['cmmsn_percent'] = $request->get('cmmsn_percent');
            } else {
                $user_data['cmmsn_percent'] = 0;
            }
            //shouldRedirectToDashboard after loggin
            if($request->has('should_redirect_to_dashboard'))
            {
                $user_data['should_redirect_to_dashboard'] = $request['should_redirect_to_dashboard'];
            }

            if(auth()->user()->can('user.edit_credit_limit'))
            {
                $user_data['credit_limit'] = $request->credit_limit;
            }




            $user = User::findOrFail($id);
            $user->update($user_data);

            $role_id = $request->input('role');
            $user_role = $user->roles->first();

            if( $user_role->id != $role_id ){
                $user->removeRole($user_role->name);

                $role = Role::findOrFail($role_id);
                $user->assignRole($role->name);
            }

            $output = array('success' => 1, 
                        'msg' => __("user.user_update_success")
                    );
        } catch(\Exception $e){

            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = array('success' => 0, 
                            'msg' => __("messages.something_went_wrong")
                        );
        }

        return redirect('users')->with('status', $output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('user.delete') ) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {

            try {

                User::where('id', $id)->delete();

                $output = array('success' => true, 
                                'msg' => __("user.user_delete_success")
                                );

            } catch(\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
                $output = array('success' => false, 
                            'msg' => __("messages.something_went_wrong")
                        );
            }

            return $output;
        }
    }
}
