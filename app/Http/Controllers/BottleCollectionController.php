<?php

namespace App\Http\Controllers;

use App\BottleLimit;
use App\BrokenMaraBottle;
use App\MaraBottleCharge;
use App\ReceivedMaraBottle;
use App\SaleMaraBottle;
use App\SaleStatement;
use App\User;
use Illuminate\Http\Request;

use Yajra\DataTables\DataTables;

class BottleCollectionController extends Controller
{
    //

    public function listBottle(Request $request)
    {
        if($request->ajax())
        {
            if(isset($request->sale_rep_id) && $request->sale_rep_id != 'all' && $request->sale_rep_id != null)
            {
                $bottle = SaleMaraBottle::where(['sale_rep_id' => $request->sale_rep_id])->get();
            }else{
                $bottle = SaleMaraBottle::all();
            }

            return DataTables::of($bottle)
                ->addColumn('sale',function ($bottle)
                {
                    return $bottle->saleRep->fullName();
                })
                ->make(true);
        }

        return view('bottle.list');
    }

    public function listLimit(Request $request)
    {

        if($request->ajax())
        {
            $limit = BottleLimit::all();

            return DataTables::of($limit)
                ->editColumn('amount',function ($data){
                    return number_format($data->amount);
                })
                ->addColumn('sale',function ($data){
                    return $data->saleRep->fullName();
                })
                ->addColumn('action',function ($data){
                    $html = '<div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                 data-toggle="dropdown" aria-expanded="false">actions
                                <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                </span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                    $html .= "<li><a view-modal-popup href='".action('BottleCollectionController@edit',['id' => $data->id])."'><i class='fa fa-pencil' aria-hidden='true'></i> Edit</a></li>";

                        $html .= "<li><a  href='".action('BottleCollectionController@delete',['id'=>$data->id])."'><i class='fa fa-trash' aria-hidden='true'></i> Delete</a></li>";
                        $html .="</ul>";

                        return $html;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('bottle.limit');
    }

    public function add()
    {
        $sales = User::all();

        return view('bottle.__addModal',['sales' => $sales])->render();
    }

    public function store(Request $request)
    {

        if(BottleLimit::where(
            [
                'sale_rep_id' => $request->sale_rep_id,
            ]
        )->count())
        {
            return back()->with(['success' => 0,'msg' => 'Bottle limit already exist']);
        }

        BottleLimit::create(
            [
                'sale_rep_id' => $request->sale_rep_id,
                'amount' => $request->amount,
                'added_by' => auth()->id(),
                'description' => $request->description
            ]
        );
        return back()->with(['success' => 1,'msg' => 'Bottle limit added']);
    }

    public function edit(Request $request)
    {
        $limit = BottleLimit::where(['id' => $request->id])->first();

        return view('bottle.__editModal',['limit' =>$limit])->render();
    }

    public function delete(Request $request)
    {
        BottleLimit::where(['id' => $request->id])->delete();

        return back()->with(['success' => 1,'msg' =>'Successfully deleted']);
    }

    public function update(Request $request)
    {


        BottleLimit::where(
            [
                'id' => $request->bottle_limit_id
            ]
        )->update(
            [
                'amount' => $request->amount,
                'description' => $request->description,
            ]
        );

        return back()->with(['success' => 1,'msg'=>'Successfully updated']);
    }

    public function process(Request $request)
    {
        if($request->ajax())
        {
            if(isset($request->table) && $request->table=='charges-broken-bottle')
            {

                if(isset($request->sale_rep_id) && $request->sale_rep_id != 'all' && $request->sale_rep_id!=null)
                {
                    $receivedBottles = MaraBottleCharge::where(
                        [
                            'sale_rep_id' => $request->sale_rep_id
                        ]
                    )->get();
                }else{
                    $receivedBottles = MaraBottleCharge::all();
                }

                return DataTables::of($receivedBottles)
                    ->addColumn('sale',function ($data){
                        return $data->saleRep->fullName();
                    })->addColumn('addedBy',function ($data){
                        return $data->addedBy->fullName();
                    })
                    ->make(true);
            }

            if(isset($request->table) && $request->table=='received-bottle')
            {

                if(isset($request->sale_rep_id) && $request->sale_rep_id != 'all' && $request->sale_rep_id!=null)
                {
                    $receivedBottles = ReceivedMaraBottle::where(
                        [
                            'sale_rep_id' => $request->sale_rep_id
                        ]
                    )->get();
                }else{
                    $receivedBottles = ReceivedMaraBottle::all();
                }

                return DataTables::of($receivedBottles)
                    ->addColumn('sale',function ($data){
                        return $data->saleRep->fullName();
                    })->addColumn('addedBy',function ($data){
                        return $data->addedBy->fullName();
                    })
                    ->make(true);
            }
            if(isset($request->table) && $request->table=='received-broken-bottle')
            {

                if(isset($request->sale_rep_id) && $request->sale_rep_id != 'all' && $request->sale_rep_id!=null)
                {
                    $receivedBottles = BrokenMaraBottle::where(
                        [
                            'sale_rep_id' => $request->sale_rep_id
                        ]
                    )->get();
                }else{
                    $receivedBottles = BrokenMaraBottle::all();
                }

                return DataTables::of($receivedBottles)
                    ->addColumn('sale',function ($data){
                        return $data->saleRep->fullName();
                    })->addColumn('addedBy',function ($data){
                        return $data->addedBy->fullName();
                    })
                    ->addColumn('action',function ($data){

                        return '';
                                /*$html = '<div class="btn-group">
                                      <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                         data-toggle="dropdown" aria-expanded="false">actions
                                        <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                                $html .= "<li><a view-modal-popup href='".action('BottleCollectionController@charge',['id' =>$data->id,])."'><i class='fa fa-info' aria-hidden='true'></i>Charge Bottle</a></li>";
                                $html .= "</ul>";
                                return $html;*/
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }



            if(isset($request->sale_rep_id) && $request->sale_rep_id != 'all' && $request->sale_rep_id!=null)
            {
                $saleBottles = SaleMaraBottle::where(
                    [
                        'sale_rep_id' => $request->sale_rep_id
                    ]
                )->get();
            }else{
                $saleBottles = SaleMaraBottle::all();
            }

            //if(isset($request->actionable) && $request->actionable==true)
            {
                return DataTables::of($saleBottles)
                    ->addColumn('sale',function ($saleBottle)
                    {
                        return $saleBottle->saleRep->fullName();
                    })
                    ->addColumn('action',function ($saleBottle){

                        $html = '<div class="btn-group">
                                      <button type="button" class="btn btn-info dropdown-toggle btn-xs" 
                                         data-toggle="dropdown" aria-expanded="false">actions
                                        <span class="caret"></span><span class="sr-only">Toggle Dropdown
                                        </span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                        $html .= "<li><a view-modal-popup href='".action('BottleCollectionController@receive',['id' => $saleBottle->id])."'><i class='fa fa-handshake-o' aria-hidden='true'></i>Receive Bottles</a></li>";
                        $html .= "<li><a view-modal-popup href='".action('BottleCollectionController@receiveBroken',['id' => $saleBottle->id])."'><i class='fa fa-chain-broken' aria-hidden='true'></i>Record Broken</a></li>";
                        $html .= "</ul>";
                        return $html;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
        }



        if(isset($request->sale_rep_id) && $request->sale_rep_id!= 'all')
        {
            $sale = User::where(['id' => $request->sale_rep_id])->first();

            $bottleLimit = doubleval(@$sale->bottleLimit->amount);
            $bottleBalance = doubleval(@$sale->unclearedMaraBottles());
            $broken = doubleval(@$sale->brokenBottles->sum('amount'));

        }else{

            $bottleBalance = SaleMaraBottle::sum('amount');
            $bottleLimit = BottleLimit::sum('amount');
            $broken = BrokenMaraBottle::sum('amount');
        }

        $users = User::all();

        return view('bottle.process')->with(
            [
                'bottleBalance' => $bottleBalance,
                'bottleLimit' => $bottleLimit,
                'broken' => $broken,
                'users' => $users,
            ]
        );
    }

    public function receive(Request $request)
    {
        $saleRep = @SaleMaraBottle::where(['id' => $request->id])->first()->saleRep;

        return view('bottle.__receiveModal',['mark'=>'received','saleRep' => $saleRep,'saleMaraBottleId'=>$request->id])->render();
    }

    public function receiveBroken(Request $request)
    {
        $saleRep = @SaleMaraBottle::where(['id' => $request->id])->first()->saleRep;
        $saleReps = User::all();

        return view('bottle.__receiveModal',['mark'=>'broken','saleRep' => $saleRep,'users' => $saleReps,'saleMaraBottleId'=>$request->id])->render();
    }

    public function charge(Request $request)
    {
        $saleRep = @($bottleBroken = BrokenMaraBottle::where(['id' => $request->id])->first())->saleRep;

        return view('bottle.__chargeModal',['bottleBroken'=>$bottleBroken,'mark'=>'broken','saleRep' => $saleRep,'saleMaraBottleId'=>$request->id])->render();
    }

    public function postReceive(Request $request)
    {

        //check if the sale rep has this amount of bottle
        $saleMaraBottle =SaleMaraBottle::where(['sale_rep_id' => $request->sale_rep_id])->first();

        if(($amount = $request->amount)> $saleMaraBottle->amount)
        {
            return back()->with(['success' => 0,'msg'=>'Bottle amount exceeds the amount sale rep. has']);
        }

        $saleMaraBottle->decrement('amount',$amount);

        if($request->mark == 'received')
        {

            ReceivedMaraBottle::create(
                [
                    'added_by' => auth()->id(),
                    'amount' => $amount,
                    'sale_rep_id' => $request->sale_rep_id
                ]
            );


            return back()->with(['success' => 1,'msg'=>'Successfully collected bottles']);
        }

            BrokenMaraBottle::create(
                [
                    'added_by' => auth()->id(),
                    'amount' => $amount,
                    'charge' => ($totalAmount = ($amount * 20)),
                    'sale_rep_id' => $request->sale_rep_id
                ]
            );

        ((new SaleStatement())->addDebit($totalAmount,'Charged for broken mara Bottle',$request->userToCharge));


        return back()->with(['success' => 1,'msg'=>'Successfully collected broken bottles']);
    }

    public function postCharge(Request $request)
    {

        try{
            $brokenMaraBottle = BrokenMaraBottle::where(['id' => $request->brokenBottleId])->first();

            if(($amount = $request->amount) > $brokenMaraBottle->charge)
            {
                return back()->with(['success' => 0,'msg' => 'The amount exceeds bottle charges']);
            }


            $brokenMaraBottle->decrement('charge',$request->amount);



            if($brokenMaraBottle->charge == 0)
            {
                $brokenMaraBottle->delete();
            }

            MaraBottleCharge::create(
                [
                    'sale_rep_id' => $request->sale_rep_id,
                    'added_by' => auth()->id(),
                    'amount' => $request->amount,
                ]
            );
        }catch (\Exception $e)
        {
            //
        }

        return back()->with(['success' => 1, 'msg' => 'Successfully charged broken bottle']);
    }
}
