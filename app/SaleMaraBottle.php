<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleMaraBottle extends Model
{
    //
    protected $guarded = ['id'];

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }
}
