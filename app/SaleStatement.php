<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleStatement extends Model
{
    //
    protected $guarded = ['id'];

    public function saleRep()
    {
        return $this->belongsTo(User::class,'sale_rep_id','id');
    }

    public function addCredit($amount,$description, $saleRepId)
    {
        //add invoice amount..
        \Repo\Invoice::adjustSaleRepInvoice(User::findOrFail($saleRepId),$amount*-1);

        return $this->create(
            [
                'description' => $description,
                'credit' => $amount,
                'sale_rep_id' => $saleRepId,
            ]
        );
    }

    public function addDebit($amount,$description, $saleRepId)
    {
        //reduce invoice amount..
        \Repo\Invoice::adjustSaleRepInvoice(User::findOrFail($saleRepId),$amount);

        return $this->create(
            [
                'description' => $description,
                'debit' => $amount,
                'sale_rep_id' => $saleRepId,
            ]
        );
    }
}
