<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SamplePromotion extends Model
{
    //
    protected $guarded = ['id'];

    public function samplePromotionProducts()
    {
        return $this->hasMany(SamplePromotionProduct::class);
    }

    public function customer()
    {
        return $this->belongsTo(Contact::class,'customer_id','id');
    }

}
