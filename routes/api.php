<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::any('callback-new',function (Request $request){



    {
        $data = [];

        Mail::raw(json_encode(request()->all()), function ($to) {
            $to->to("kimsonjulius1@gmail.com")
                ->subject("Stk Push status response ");
        });

        $data['phone_number'] = $request['phone_number'];

        if ($request['Body']['stkCallback']['ResultCode'] === 1032) {
            //cancelled
            $data['status_code'] = 1032;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;

        } elseif ($request['Body']['stkCallback']['ResultCode'] === 1037) {
            //timeout
            $data['status_code'] = 1037;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;
        } elseif ($request['Body']['stkCallback']['ResultCode'] === 1001) {
            //timeout
            $data['status_code'] = 1001;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];
        } elseif ($request['Body']['stkCallback']['ResultCode'] === 2001) {
            //timeout
            $data['status_code'] = 2001;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];
        } else if ($request['Body']['stkCallback']['ResultCode'] === 0) {
            //success
            $data['status_code'] = 0;//success response////kila kitu iko sawa//
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;
            $items = $request['Body']['stkCallback']['CallbackMetadata']['Item'];

            foreach ($items as $item) {
                if ($item['Name'] === 'Amount') {
                    $data['amount'] = $item['Value'];
                }

                if ($item['Name'] === 'MpesaReceiptNumber') {
                    $data['transaction_code'] = $item['Value'];
                }

                if ($item['Name'] === 'PhoneNumber') {
                    $data['phone_number'] = $item['Value'];
                }

            }

            $data['transaction_code'] = str_replace('-','_', $data['transaction_code']);


        }



        Mail::raw(json_encode($data), function ($to) {
            $to->to("kimsonjulius1@gmail.com")
                ->subject("Stk Push status response ");
        });
    }

});




Route::any('callback',function (){



    Mail::raw(json_encode(request()->all()), function ($to) {
        $to->to("kimsonjulius1@gmail.com")
            ->subject("Stk Push status response ");
    });


    try{
        $request = request()->all();

        $data = [];

//        $data['phone_number'] = $request['phone_number'];

        if ($request['Body']['stkCallback']['ResultCode'] === 1032) {
            //cancelled
            $data['status_code'] = 1032;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;

        } elseif ($request['Body']['stkCallback']['ResultCode'] === 1037) {
            //timeout
            $data['status_code'] = 1037;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;
        } elseif ($request['Body']['stkCallback']['ResultCode'] === 1001) {
            //timeout
            $data['status_code'] = 1001;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];
        } elseif ($request['Body']['stkCallback']['ResultCode'] === 2001) {
            //timeout
            $data['status_code'] = 2001;
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];
        } else if ($request['Body']['stkCallback']['ResultCode'] === 0) {
            //success
            $data['status_code'] = 0;//success response////kila kitu iko sawa//
            $data['message'] = $request['Body']['stkCallback']['ResultDesc'];;
            $items = $request['Body']['stkCallback']['CallbackMetadata']['Item'];

            foreach ($items as $item) {
                if ($item['Name'] === 'Amount') {
                    $data['amount'] = $item['Value'];
                }

                if ($item['Name'] === 'MpesaReceiptNumber') {
                    $data['transaction_code'] = $item['Value'];
                }

                if ($item['Name'] === 'PhoneNumber') {
                    $data['phone_number'] = $item['Value'];
                }

            }

            $data['transaction_code'] = str_replace('-','_', $data['transaction_code']);


            //update the database..
            $mpesa = \App\MpesaTransaction::where('trans_code',$request['transCode'])->first();

            $mpesa->update(
                [
                    'phone_number' => $data['phone_number'],
                    'mpesa_code' => $data['transaction_code'],
                    'confirmed' => true,
                ]
            );

        }

        Mail::raw(json_encode($data), function ($to) {
            $to->to("kimsonjulius1@gmail.com")
                ->subject("Stk Push status response ");
        });

    }catch (Exception $e)
    {
        Mail::raw($e->getMessage(), function ($to) {
            $to->to("kimsonjulius1@gmail.com")
                ->subject("Stk Push status response ");
        });
    }




});