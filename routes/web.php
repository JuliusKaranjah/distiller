<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once('install.php');

Route::middleware(['IsInstalled'])->group(function () {

	Route::get('/', function () {
    	return view('welcome');
	});

	Auth::routes();

	Route::get('/business/register', 'BusinessController@getRegister')->name('business.getRegister');
	Route::post('/business/register', 'BusinessController@postRegister')->name('business.postRegister');
	Route::post('/business/register/check-username', 'BusinessController@postCheckUsername')->name('business.postCheckUsername');
});

Route::get('sells/load-month-sales', 'SellPosController@loadSales');


//Routes for authenticated users only
Route::middleware(['IsInstalled', 'auth', 'SetSessionData', 'language', 'timezone'])->group(function () {

	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('/home', 'HomeController@index')->name('home');
	Route::post('/home/get-purchase-details', 'HomeController@getPurchaseDetails');
	Route::any('/home/get-sell-details', 'HomeController@getSellDetails');
	Route::get('/home/product-stock-alert', 'HomeController@getProductStockAlert');
	Route::get('/home/payment-dues', 'HomeController@getPaymentDues');

	Route::get('/business/settings', 'BusinessController@getBusinessSettings')->name('business.getBusinessSettings');
	Route::post('/business/update', 'BusinessController@postBusinessSettings')->name('business.postBusinessSettings');
	Route::get('/user/profile', 'UserController@getProfile')->name('user.getProfile');
	Route::post('/user/update', 'UserController@updateProfile')->name('user.updateProfile');
	Route::post('/user/update-password', 'UserController@updatePassword')->name('user.updatePassword');

	Route::resource('brands', 'BrandController');

	Route::resource('tax-rates', 'TaxRateController');

	Route::resource('units', 'UnitController');

	Route::post('/contacts/check-contact-id', 'ContactController@checkContactId');
	Route::get('/contacts/customers', 'ContactController@getCustomers');
	Route::resource('contacts', 'ContactController');

	Route::resource('categories', 'CategoryController');

	Route::resource('variation-templates', 'VariationTemplateController');

	Route::get('/products/view/{id}', 'ProductController@view');
	Route::get('/products/list', 'ProductController@getProducts');
	Route::get('/products/list-no-variation', 'ProductController@getProductsWithoutVariations');
	
	Route::post('/products/get_sub_categories', 'ProductController@getSubCategories');
	Route::post('/products/product_form_part', 'ProductController@getProductVariationFormPart');
	Route::post('/products/get_product_variation_row', 'ProductController@getProductVariationRow');
	Route::post('/products/get_variation_template', 'ProductController@getVariationTemplate');
	Route::get('/products/get_variation_value_row', 'ProductController@getVariationValueRow');
	Route::post('/products/check_product_sku', 'ProductController@checkProductSku');
	Route::get('/products/quick_add', 'ProductController@quickAdd');
	Route::post('/products/save_quick_product', 'ProductController@saveQuickProduct');
	
	Route::resource('products', 'ProductController');


	Route::get('/purchases/get_products', 'PurchaseController@getProducts');
	Route::get('/purchases/get_suppliers', 'PurchaseController@getSuppliers');
	Route::post('/purchases/get_purchase_entry_row', 'PurchaseController@getPurchaseEntryRow');
	Route::post('/purchases/get_order_entry_row', 'PurchaseController@getOrderEntryRow');
	Route::post('/purchases/check_ref_number', 'PurchaseController@checkRefNumber');
	Route::get('/purchases/print/{id}', 'PurchaseController@printInvoice');
	Route::resource('purchases', 'PurchaseController');



//order module
    Route::group(['prefix' => 'order'],function (){
       Route::get('/','OrderController@index');
        Route::get('create','OrderController@create');
        Route::post('store','OrderController@store');
        Route::post('approve-order','OrderController@approveOrder');
        Route::post('decline-order','OrderController@declineOrder');
        Route::post('process-order','OrderController@processOrder');
        Route::get('print-delivery-note/{id}','OrderController@getPrintDeliveryNoteContent');
        Route::get('print-invoice-note/{id}','OrderController@getPrintInvoiceNoteContent');
        Route::any('accountant-decline','OrderController@orderAccountantDecline');
        Route::get('accountant-approval/{id}','OrderController@orderAccountantApproval');
        Route::any('accountant-approve-order','OrderController@accountantApproveOrder');
        Route::get('/{id}','OrderController@viewOrder');
    });
//end of order module

    Route::group(['prefix' => 'logistics'],function (){
       Route::get('/','LogisticController@index');
        Route::get('load/{id}','LogisticController@orderLoad');
        Route::get('warehouse','LogisticController@warehouse');
        Route::get('warehouse-add','LogisticController@addWarehouse');
        Route::get('warehouse-delete','LogisticController@deleteWarehouse');
        Route::post('warehouse-store','LogisticController@storeWarehouse');

        Route::get('vehicle','LogisticController@vehicle');
        Route::get('vehicle-add','LogisticController@addVehicle');
        Route::get('vehicle-delete','LogisticController@deleteVehicle');
        Route::post('vehicle-store','LogisticController@storeVehicle');

        Route::get('driver','LogisticController@driver');
        Route::get('driver-add','LogisticController@addDriver');
        Route::get('driver-delete','LogisticController@deleteDriver');
        Route::post('driver-store','LogisticController@storeDriver');

        Route::get('expense','LogisticController@expense');
        Route::get('expense-add','LogisticController@addExpense');
        Route::get('expense-delete','LogisticController@deleteExpense');
        Route::post('expense-store','LogisticController@storeExpense');

        Route::post('post-logistic','LogisticController@PostLogistic');
        Route::get('send-to-loading-bay/{id}','LogisticController@sendToLoadingBay');
        Route::get('dispatch/{id}','LogisticController@dispatchOrder');
        Route::get('receive/{id}','LogisticController@receiveOrder');

        //view popup modals
        Route::get('details/{id}','LogisticController@details');
    });


	Route::get('/sells/drafts', 'SellController@getDrafts');
	Route::get('/sells/approve', 'SellController@approve');
	Route::get('/sells/quotations', 'SellController@getQuotations');
	Route::get('/sells/draft-dt', 'SellController@getDraftDatables');
	Route::resource('sells', 'SellController');

	Route::get('/sells/pos/get_product_row/{variation_id}/{location_id}', 'SellPosController@getProductRow');
	Route::post('/sells/pos/get_payment_row', 'SellPosController@getPaymentRow');
	Route::get('/sells/pos/get-recent-transactions', 'SellPosController@getRecentTransactions');
	Route::get('/sells/{transaction_id}/print', 'SellPosController@printInvoice')->name('sell.printInvoice');
	Route::get('/sells/pos/get-product-suggestion', 'SellPosController@getProductSuggestion');
	Route::resource('pos', 'SellPosController');
	Route::post('pos/pay-with-mpesa','SellPosController@mpesa')->name('pos_pay-with-mpesa');
	Route::post('pos/confirm-with-mpesa','SellPosController@confirmMpesa')->name('pos_confirm-with-mpesa');

	Route::get('load-mpesa-details','SellPosController@loadMpesaDetails')->name('load-mpesa-details');
	Route::post('make-stk-push','SellPosController@makeStkPush')->name('make-stk-push');
	Route::get('reports/list-invoice','SellPosController@listInvoice')->name('pos_listInvoice');
	Route::post('pos/store', 'SellPosController@store');
	Route::get('pos/show-receipt/{transId}','SellPosController@showReceipt')->name('pos_show-receipt');

	Route::resource('roles', 'RoleController');

	Route::resource('users', 'ManageUserController');

	Route::resource('group-taxes', 'GroupTaxController');

	Route::get('/barcodes/set_default/{id}', 'BarcodeController@setDefault');
	Route::resource('barcodes', 'BarcodeController');

	//Invoice schemes..
	Route::get('/invoice-schemes/set_default/{id}', 'InvoiceSchemeController@setDefault');
	Route::resource('invoice-schemes', 'InvoiceSchemeController');

	//Print Labels
	Route::get('/labels/show', 'LabelsController@show');
	Route::get('/labels/add-product-row', 'LabelsController@addProductRow');
	Route::post('/labels/preview', 'LabelsController@preview');

	//Reports...
	Route::get('/reports/service-staff-report', 'ReportController@getServiceStaffReport');
	Route::get('/reports/table-report', 'ReportController@getTableReport');
	Route::get('/reports/profit-loss', 'ReportController@getProfitLoss');
	Route::get('/reports/get-opening-stock', 'ReportController@getOpeningStock');
	Route::get('/reports/purchase-sell', 'ReportController@getPurchaseSell');
	Route::get('/reports/customer-supplier', 'ReportController@getCustomerSuppliers');
	Route::get('/reports/stock-report', 'ReportController@getStockReport');
	Route::get('/reports/stock-details', 'ReportController@getStockDetails');
	Route::get('/reports/tax-report', 'ReportController@getTaxReport');
	Route::get('/reports/trending-products', 'ReportController@getTrendingProducts');
	Route::get('/reports/expense-report', 'ReportController@getExpenseReport');
	Route::get('/reports/stock-adjustment-report', 'ReportController@getStockAdjustmentReport');
	Route::get('/reports/register-report', 'ReportController@getRegisterReport');
	Route::get('/reports/sales-representative-report', 'ReportController@getSalesRepresentativeReport');
	Route::get('/reports/sales-representative-total-expense', 'ReportController@getSalesRepresentativeTotalExpense');
	Route::get('/reports/sales-representative-total-sell', 'ReportController@getSalesRepresentativeTotalSell');
	Route::get('/reports/sales-representative-total-commission', 'ReportController@getSalesRepresentativeTotalCommission');
	Route::get('/reports/stock-expiry', 'ReportController@getStockExpiryReport');
	Route::get('/reports/stock-expiry-edit-modal/{purchase_line_id}', 'ReportController@getStockExpiryReportEditModal');
	Route::post('/reports/stock-expiry-update', 'ReportController@updateStockExpiryReport')->name('updateStockExpiryReport');
	Route::get('/reports/customer-group', 'ReportController@getCustomerGroup');
	Route::get('/reports/product-purchase-report', 'ReportController@getproductPurchaseReport');
	Route::get('/reports/product-sell-report', 'ReportController@getproductSellReport');
	Route::get('/reports/lot-report', 'ReportController@getLotReport');
	Route::get('/reports/purchase-payment-report', 'ReportController@purchasePaymentReport');
	Route::get('/reports/sell-payment-report', 'ReportController@sellPaymentReport');
	Route::get('/reports/mpesa-payment', 'ReportController@mpesaPayment');

	Route::get('/reports/logistics', 'ReportController@getLogistics');
	Route::get('/reports/orders', 'ReportController@getOrders');
	Route::get('/reports/visit', 'ReportController@getVisits');
	Route::get('/reports/messages', 'ReportController@getMessages');

	//Business Location Settings...

    Route::post('settings/add-message-recipient','LocationSettingsController@addMessageRecipient');
    Route::post('settings/add-order-delivery-recipient','LocationSettingsController@addOrderDeliveryRecipient');
    Route::get('settings/delete-order-delivery-recipient/{id}','LocationSettingsController@deleteOrderDeliveryRecipient');
    Route::get('settings/delete-message-recipient/{id}','LocationSettingsController@deleteMessageRecipient');
    Route::post('settings/add-production-recipient','LocationSettingsController@addProductionRecipient');
    Route::get('settings/delete-production-recipient/{id}','LocationSettingsController@deleteProductionRecipient');

	Route::prefix('business-location/{location_id}')->name('location.')->group(function () {
		Route::get('settings', 'LocationSettingsController@index')->name('settings');
		Route::post('settings', 'LocationSettingsController@updateSettings')->name('settings_update');
	});

	//Business Locations...
	Route::post('business-location/check-location-id', 'BusinessLocationController@checkLocationId');
	Route::resource('business-location', 'BusinessLocationController');

	//Invoice layouts..
	Route::resource('invoice-layouts', 'InvoiceLayoutController');

	//Expense Categories...
	Route::resource('expense-categories', 'ExpenseCategoryController');

	//Expenses...
	Route::resource('expenses', 'ExpenseController');

	//Transaction payments...
	Route::get('/payments/add_payment/{transaction_id}', 'TransactionPaymentController@addPayment');
	Route::get('/payments/pay-contact-due/{contact_id}', 'TransactionPaymentController@getPayContactDue');
	Route::post('/payments/pay-contact-due', 'TransactionPaymentController@postPayContactDue');
	Route::resource('payments', 'TransactionPaymentController');

	//Printers...
	Route::resource('printers', 'PrinterController');

	Route::get('/stock-adjustments/remove-expired-stock/{purchase_line_id}', 'StockAdjustmentController@removeExpiredStock');
	Route::post('/stock-adjustments/get_product_row', 'StockAdjustmentController@getProductRow');
	Route::resource('stock-adjustments', 'StockAdjustmentController');

	Route::get('/cash-register/register-details', 'CashRegisterController@getRegisterDetails');
	Route::get('/cash-register/close-register', 'CashRegisterController@getCloseRegister');
	Route::post('/cash-register/close-register', 'CashRegisterController@postCloseRegister');
	Route::resource('cash-register', 'CashRegisterController');

	//Import products
	Route::get('/import-products', 'ImportProductsController@index');
	Route::post('/import-products/store', 'ImportProductsController@store');

	//System Settings
	Route::get('/system-settings', 'BusinessController@getSystemSettings');
	Route::post('/system-settings', 'BusinessController@postSystemSettings');

	//Sales Commission Agent
	Route::resource('sales-commission-agents', 'SalesCommissionAgentController');

	//Stock Transfer
	Route::get('stock-transfers/print/{id}', 'StockTransferController@printInvoice');
	Route::resource('stock-transfers', 'StockTransferController');
	
	Route::get('/opening-stock/add/{product_id}', 'OpeningStockController@add');
	Route::post('/opening-stock/save', 'OpeningStockController@save');

	//Customer Groups
	Route::resource('customer-group', 'CustomerGroupController');

	//Import opening stock
	Route::get('/import-opening-stock', 'ImportOpeningStockController@index');
	Route::post('/import-opening-stock/store', 'ImportOpeningStockController@store');

	//Sell return
	Route::resource('sell-return', 'SellReturnController');
	Route::get('sell-return/get-product-row', 'SellReturnController@getProductRow');
	Route::get('/sell-return/print/{id}', 'SellReturnController@printInvoice');
	
	//Backup 
	Route::resource('backup', 'BackUpController', ['only' => [
		'index', 'create', 'store'
	]]);

	//Restaurant module
	Route::group(['prefix' => 'modules'], function(){

		Route::resource('tables', 'Restaurant\TableController');
		Route::resource('modifiers', 'Restaurant\ModifierSetsController');

		//Map modifier to products
		Route::get('/product-modifiers/{id}/edit', 'Restaurant\ProductModifierSetController@edit');
		Route::post('/product-modifiers/{id}/update', 'Restaurant\ProductModifierSetController@update');
		Route::get('/product-modifiers/product-row/{product_id}', 'Restaurant\ProductModifierSetController@product_row');

		Route::get('/add-selected-modifiers', 'Restaurant\ProductModifierSetController@add_selected_modifiers');

		Route::get('/kitchen', 'Restaurant\KitchenController@index');
		Route::get('/kitchen/mark-as-cooked/{id}', 'Restaurant\KitchenController@markAsCooked');
		Route::post('/refresh-orders-list', 'Restaurant\KitchenController@refreshOrdersList');

		Route::get('/orders', 'Restaurant\OrderController@index');
		Route::get('/orders/mark-as-served/{id}', 'Restaurant\OrderController@markAsServed');
		Route::get('/data/get-pos-details', 'Restaurant\DataController@getPosDetails');
	});

	Route::get('bookings/get-todays-bookings', 'Restaurant\BookingController@getTodaysBookings');
	Route::resource('bookings', 'Restaurant\BookingController');

	Route::group(['prefix' => 'message'],function (){
	   Route::get('/','MessageController@index');
	   Route::get('create','MessageController@create');
	   Route::post('store','MessageController@store');
    });

	/*samples*/
    Route::group(['prefix' => 'sample'],function (){
       Route::get('/','SampleController@index');
       Route::get('create-order','SampleController@createOrder');
       Route::post('store-order','SampleController@storeOrder');
       Route::get('details','SampleController@viewDetails');
       Route::post('accountant_approve','SampleController@accountantApprove');
        Route::get('sale-repre-approve','SampleController@saleRepresentativeApproval');
        Route::get('stock','SampleController@stock');
        Route::get('assign','SampleController@assign');
        Route::get('assign-to-sales','SampleController@assignToSales');
        Route::post('store-promotion','SampleController@storePromotion');
        Route::post('assign-sample-to-sales','SampleController@assignSampleToSales');
    });


//accounts module
    Route::group([
        'prefix' => 'accounts'
    ],function (){
        Route::get('reconciliation','AccountsController@reconciliation');
        Route::get('cheque','AccountsController@index');
        Route::get('mpesa-list','AccountsController@mpesaList');
        Route::get('cash-list','AccountsController@cashList');
        Route::get('receive-cash-from-sales/{id}','AccountsController@receiveCashFromSales');
        Route::post('post-receive-cash-from-sales','AccountsController@postReceiveCashFromSales');
        Route::get('bank-transfer-action/{id}','AccountsController@bankTransferModal');
        Route::get('bank-transfer-list','AccountsController@bankTransferList');
        Route::get('cheque-details','AccountsController@chequeDetails');
        Route::post('cheque-action','AccountsController@chequeAction');
        Route::get('invoice','AccountsController@invoice');
        Route::get('statement','AccountsController@statement');
        Route::get('sales-statement/{id}','AccountsController@saleStatement');
        Route::post('remit-commission','AccountsController@remitCommission');

        Route::get('debit-note','AccountsController@debitNote');
        Route::get('debit-note-add','AccountsController@addDebitNote');
        Route::post('store-debit-note','AccountsController@storeDebitNote');

        Route::get('credit-note','AccountsController@creditNote');
        Route::get('credit-note-add','AccountsController@addCreditNote');
        Route::post('store-credit-note','AccountsController@storeCreditNote');
    });

    Route::group(['prefix' => 'visit'],function (){
        Route::get('/','VisitController@index');
        Route::get('create','VisitController@create');
        Route::get('reason','VisitController@reason');
        Route::post('store-visit','VisitController@storeVisit');
        Route::get('delete-reason','VisitController@deleteReason');
        Route::post('store','VisitController@store');
        Route::get('addReason','VisitController@addReason');
        Route::post('store-Reason','VisitController@storeReason');
        Route::get('add-customer','VisitController@addCustomer');
        Route::get('delete/{id}','VisitController@delete');
    });
//end of accounts module


    //bottle collection module
    Route::group(['prefix' => 'bottle'],function (){
       Route::get('limit','BottleCollectionController@listLimit');
       Route::get('list','BottleCollectionController@listBottle');
       Route::get('add','BottleCollectionController@add');
       Route::get('edit','BottleCollectionController@edit');
       Route::get('delete/{id}','BottleCollectionController@delete');
       Route::post('post-add','BottleCollectionController@store');
       Route::post('store','BottleCollectionController@store');
       Route::post('update','BottleCollectionController@update');

       Route::get('process','BottleCollectionController@process');
       Route::get('receive','BottleCollectionController@receive');
       Route::post('post-receive','BottleCollectionController@postReceive');
       Route::get('receive-broken','BottleCollectionController@receiveBroken');
       Route::get('charge','BottleCollectionController@charge');
       Route::post('post-charge','BottleCollectionController@postCharge');

       Route::get('setting','BottleSettingController@index');
       Route::get('delete','BottleSettingController@delete');
       Route::post('store','BottleSettingController@store');

    });
    //end of bottle module

    //Regional Product
    route::group(['prefix' => 'regional-product'],function (){
       Route::get('/list','RegionalProductController@index');
       Route::get('/add','RegionalProductController@add');
       Route::post('/store','RegionalProductController@store');
       Route::get('/delete','RegionalProductController@delete');
    });


    Route::get('region-price-setting','RegionProductSettingController@index');


});

//get payment overview details
Route::get('cash-at-hand',function ()
{
    $duration = 'today';

    if((request('duration')))
    {
        $duration = request('duration');
    }


    return totalCashAtHand(auth()->id(),$duration);
});


Route::get('mpesa-payment',function ()
{
    $duration = 'today';

    if((request('duration')))
    {
        $duration = request('duration');
    }


    return totalMpesa(auth()->id(),$duration);
});


Route::get('cheque-payment-pending',function ()
{
    $duration = 'today';

    if((request('duration')))
    {
        $duration = request('duration');
    }


    return totalCheque(auth()->id(),$duration);
});


Route::get('cheque-payment-approved',function ()
{
    $duration = 'today';

    if((request('duration')))
    {
        $duration = request('duration');
    }


    return totalCheque(auth()->id(),$duration,'approved');
});


Route::get('test',function (){



    if(env('APP_ENV') != 'local')
    {
        dd('Are you a hacker or something');
    }

    dd('closer');



});
