<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_statements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sale_rep_id');
            $table->double('debit')->nullable();
            $table->double('credit')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('sale_rep_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_statements');
    }
}
