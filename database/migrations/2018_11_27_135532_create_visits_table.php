<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sale_rep_id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('reason_id');
            $table->string('geolocation')->nullable();
            $table->text('description')->nullable();
            $table->text('findings')->nullable();
            $table->text('conclusion')->nullable();

            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')->on('contacts')
                ->onDelete('cascade')->onUpdate('cascade');


            $table->foreign('reason_id')
                ->references('id')->on('visit_reasons')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('sale_rep_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
