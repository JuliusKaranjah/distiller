<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokenMaraBottlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broken_mara_bottles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('added_by');
            $table->unsignedInteger('sale_rep_id');
            $table->double('amount')->default(0);
            $table->double('charge')->default(0);

            $table->timestamps();

            $table->foreign('added_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('sale_rep_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broken_mara_bottles');
    }
}
