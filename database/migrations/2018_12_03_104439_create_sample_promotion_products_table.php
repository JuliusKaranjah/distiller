<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSamplePromotionProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample_promotion_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sample_promotion_id');
            $table->unsignedInteger('product_id');
            $table->double('quantity')->default(0);
            $table->timestamps();

            $table->foreign('sample_promotion_id')
                ->references('id')->on('sample_promotions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample_promotion_products');
    }
}
