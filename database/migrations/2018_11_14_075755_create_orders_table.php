<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default('pending');
            $table->string('accountant_status')->default('pending');
            $table->timestamp('action_at')->nullable();
            $table->unsignedInteger('added_by');
            $table->unsignedInteger('destination_location_id');
            $table->unsignedInteger('origin_location_id');
            $table->text('note')->nullable();

            $table->string('logistic_status')->nullable('pending');
            $table->string('loaded_at')->nullable();
            $table->string('sent_loading_bay_at')->nullable();
            $table->string('dispatched_at')->nullable();
            $table->string('received_at')->nullable();

            $table->unsignedInteger('warehouse_id')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->unsignedInteger('driver_id')->nullable();

            $table->timestamps();

            $table->foreign('added_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('destination_location_id')
                ->references('id')->on('business_locations')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('origin_location_id')
                ->references('id')->on('business_locations')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('warehouse_id')
                ->references('id')->on('warehouses')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('vehicle_id')
                ->references('id')->on('vehicles')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('driver_id')
                ->references('id')->on('drivers')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
