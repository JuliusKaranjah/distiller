<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProcessingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_processings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('transaction_payment_id');
            $table->string('method')->default('cash');
            $table->string('status')->default('initiated');
            $table->string('cheque_number')->nullable();
            $table->double('amount')->default(0);
            $table->timestamp('remit_at')->nullable();

            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('transaction_payment_id')
                ->references('id')->on('transaction_payments')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_processings');
    }
}
