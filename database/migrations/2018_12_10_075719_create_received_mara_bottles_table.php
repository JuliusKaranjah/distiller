<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedMaraBottlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('received_mara_bottles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sale_rep_id');
            $table->double('amount')->default(0);
            $table->unsignedInteger('added_by');
            $table->timestamps();


            $table->foreign('sale_rep_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('added_by')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('received_mara_bottles');
    }
}
