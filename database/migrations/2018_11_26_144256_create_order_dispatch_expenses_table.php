<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDispatchExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_dispatch_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('expense_id');
            $table->double('amount',6,2)->default(0.00);
            $table->timestamps();

            $table->foreign('expense_id')
                ->references('id')->on('logistic_expense_types')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_dispatch_expenses');
    }
}
