<div class="box">
    <div class="box-header">
        <h3 class="box-title">Bottle Charges</h3>
    </div>
    <div class="box-body">


        <div class="table-responsive">
            <table class="table table-bordered table-striped ajax_view" id="chargeBottleTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale Rep.</th>
                    <th>Amount Charged</th>
                    <th>Added By</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>