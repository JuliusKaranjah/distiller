@extends('layouts.app')

@section('title','Add Visit')


@section('content')
    <section class="content-header no-print">
        <h1>Bottle Limit
            <small></small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Bottle Limit</h3>
                @can('visit.create')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" view-modal-popup href="{{action('BottleCollectionController@add')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view" id="visitTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sale Rep.</th>
                            <th>Bottle Limit</th>
                            <th>Note</th>
                            <th>Added At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script>
        //Purchase table

        var order_table = $('#visitTable').DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[4, 'desc']],
            ajax: '',
            columnDefs: [ {
                // "targets": [1,2],
                "orderable": false,
                "searchable": false
            } ],
            columns: [
                { data: 'id', name: 'id'  },
                { data: 'sale', name: 'sale'},
                { data: 'amount', name: 'amount'},
                { data: 'description', name: 'description'},
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action'},
            ]
        });
    </script>
    @endsection