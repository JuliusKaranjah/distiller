<div class="box">
    <div class="box-header">
        <h3 class="box-title">Bottle Balance</h3>
    </div>
    <div class="box-body">


        <div class="table-responsive">
            <table class="table table-bordered table-striped ajax_view" id="visitTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale Rep.</th>
                    <th>Bottle Balance</th>
                    <th>Note</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>