<div class="box">
    <div class="box-header">
        <h3 class="box-title">Bottle Received</h3>
    </div>
    <div class="box-body">


        <div class="table-responsive">
            <table class="table table-bordered table-striped ajax_view" id="receivedBrokenBottleTable">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Sale Rep.</th>
                    <th>Broken Bottle</th>
                    <th>Received By</th>
                    <th>Received At</th>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>