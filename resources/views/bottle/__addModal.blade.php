
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal-content"><div class="box box-solid"><!--box start-->
                <form action="{{url('/bottle/post-add')}}" method="post" class="box-body">
                    {!! csrf_field() !!}
                    <div class="col-md-12 col-lg-12" style="font-weight: bold">

                        Add Bottle Limit
                        <hr>
                    </div>


                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Select Product</label>

                                    <select class="form-control select2" name="sale_rep_id">
                                        @foreach($sales as $sale)
                                            <option value="{{$sale->id}}">{{$sale->fullName()}}</option>
                                            @endforeach
                                    </select>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Quantity</label>

                                    <input type="number" step="1" required name="amount" class="form-control">

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Note (optional)</label>

                                    <textarea name="description" class="form-control" id="" cols="30" rows="3"></textarea>

                                </div>
                            </div>

                        </div>

                    </div>



                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <button class="btn btn-primary" value="load">Add Bottle Limit</button>
                        </div>
                    </div>
                </form>
            </div><!--box end--></div>
    </div>
