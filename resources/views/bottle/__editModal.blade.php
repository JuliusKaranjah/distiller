
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal-content"><div class="box box-solid"><!--box start-->
                <form action="{{action('BottleCollectionController@update')}}" method="post" class="box-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="bottle_limit_id" value="{{$limit->id}}">
                    <div class="col-md-12 col-lg-12" style="font-weight: bold">

                        Edit Bottle Limit
                        <hr>
                    </div>


                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Select Product</label>

                                    <select class="form-control select2 disabled" disabled>
                                        <option>{{$limit->saleRep->fullName()}}</option>
                                    </select>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Quantity</label>

                                    <input type="number" step="1" required name="amount" value="{{$limit->amount}}" class="form-control">

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Note (optional)</label>

                                    <textarea name="description" class="form-control" id="" cols="30" rows="3">{{$limit->description}}</textarea>

                                </div>
                            </div>

                        </div>

                    </div>



                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <button class="btn btn-primary" value="load">Update Bottle Limit</button>
                        </div>
                    </div>
                </form>
            </div><!--box end--></div>
    </div>
