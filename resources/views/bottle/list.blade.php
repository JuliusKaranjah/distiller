@extends('layouts.app')

@section('title','Add Visit')


@section('content')
    <section class="content-header no-print">
        <h1>Bottle Accumulated
            <small></small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Bottle Accumulated</h3>

            </div>
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view" id="visitTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sale Rep.</th>
                            <th>Bottle Accumulated</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script>
        //Purchase table

        var order_table = $('#visitTable').DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[0, 'desc']],
            ajax: '',
            columnDefs: [ {
                // "targets": [1,2],
                "orderable": false,
                "searchable": false
            } ],
            columns: [
                { data: 'id', name: 'id'  },
                { data: 'sale', name: 'sale'},
                { data: 'amount', name: 'amount'},

            ]
        });
    </script>
    @endsection