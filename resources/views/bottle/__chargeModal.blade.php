
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content"><div class="box box-solid"><!--box start-->
                <form action="{{action('BottleCollectionController@postCharge')}}" method="post" class="box-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="brokenBottleId" value="{{$bottleBroken->id}}">
                    <input type="hidden" name="sale_rep_id" value="{{$saleRep->id}}">
                    <div class="col-md-12 col-lg-12" style="font-weight: bold">

                        Charge Sales Rep For Broken Bottle
                        <hr>
                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Amount Charged</label>

                                    <input type="number" step="1" required name="amount" value="{{$bottleBroken->charge}}" class="form-control">

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <button type="submit" class="btn btn-primary">Charge</button>
                        </div>
                    </div>
                </form>
            </div><!--box end--></div>
    </div>
