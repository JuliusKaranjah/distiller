@extends('layouts.app')
@section('title', 'process')

@section('content')

    <style>
        .badge-completed{
            background-color: #4ca75b;
        }
        .badge-primary{
            background-color: #3b8dbc;
        }
        .badge-initiated{
            background-color: #01c0ef;
        }
        .badge-declined{
            background-color: #d63925;
        }
    </style>



    <!-- Content Header (Page header) -->
    {{--<section class="content-header">
        <h1>Reconciliation</h1>
    </section>--}}

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-sm-4 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bottle Balance</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($bottleBalance)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-4 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bottle Limit</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($bottleLimit)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-sm-4 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Broken</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($broken)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" id="accordion">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
                                <i class="fa fa-filter" aria-hidden="true"></i> @lang('report.filters')
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFilter" class="panel-collapse active collapse in" aria-expanded="true">
                        <div class="box-body">
                            {!! Form::open(['url' => action('BottleCollectionController@process'), 'method' => 'get', 'id' => 'sales_representative_filter_form' ]) !!}
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('sr_id',  __('report.user') . ':') !!}
                                    <select class="form-control select2" id="userSelected" name="sale_rep_id" style="width: 100%">
                                        <option value="all">All</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->fullName()}}</option>
                                            @endforeach
                                    </select>

                                </div>
                            </div>


                            {{--<div class="col-md-4 hide">
                                <div class="form-group">

                                    {!! Form::label('sr_date_filter', __('report.date_range') . ':') !!}
                                    {!! Form::text('date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'id' => 'sr_date_filter', 'readonly']); !!}
                                </div>
                            </div>--}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <br>
                                    <button class="btn btn-primary" style="margin-top: 5px  "><i class="fa fa-filter"></i>Filter</button>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Summary -->

        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#sr_sales_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Bottle Balance</a>
                        </li>

                        <li>
                            <a href="#sr_mpesa_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Received Bottle</a>
                        </li>

                        <li>
                            <a href="#sr_cheque_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Broken Bottle</a>
                        </li>

                        {{--<li>
                            <a href="#sr_bankTransfer_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Broken Bottle Charges</a>
                        </li>--}}
{{--
                        <li>
                            <a href="#sr_other_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Other</a>
                        </li>

                        <li>
                            <a href="#sr_commission_remit_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Remitted Commission</a>
                        </li>--}}


                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="sr_sales_tab">
                            @include('bottle.balance')
                        </div>
                        <div class="tab-pane" id="sr_mpesa_tab">
                            @include('bottle.__received')
                        </div>

                        <div class="tab-pane" id="sr_cheque_tab">
                            @include('bottle.__receivedBroken')
                        </div>


                        {{--<div class="tab-pane" id="sr_bankTransfer_tab">
                            @include('bottle.charges')
                        </div>--}}


                       {{-- <div class="tab-pane" id="sr_other_tab">
                            closer4
                        </div>


                        <div class="tab-pane" id="sr_commission_remit_tab">
                            closerl5
                        </div>--}}


                    </div>

                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
    <div class="modal fade view_register" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade view_register" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

@endsection

@section('javascript')
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            $("#cashDeposit").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {
                        $("div.view_register").html(data).modal('show');
                    },
                    error:function (err) {
                        //
                    }
                })

            });



            $(".viewChequePayment").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {
                        $("div.view_register").html(data).modal('show');
                    },
                    error:function (err) {
                        //
                    }
                })

            });

            $("#remitCommission").on('click',function (e) {
                e.preventDefault();

                $("div.remitCommission").modal('show');
            });

            $("#remit_amount").on('input',function () {
                var amount = parseFloat($(this).val());
                var maximum = parseFloat($(this).attr('max'));

                if(amount > maximum)
                {
                    $("span.remitAmount").html('Amount input exceeds sale rep. commission. Commissions is '+maximum).show();
                    $("button.postRemitCommission").addClass('disabled');
                }else {
                    $("span.remitAmount").html('').hide();
                    $("button.postRemitCommission").removeClass('disabled');
                }

            });

            $("button.postRemitCommission").on('click',function (e) {
                if($(this).hasClass('disabled'))
                {
                    e.preventDefault();
                    return;
                }
            })
        })
    </script>

@endsection

@section('script')
    <script>
        $(function () {
            var order_table = $('#visitTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: ''+'?actionable=true&sale_rep_id='+"{{request('sale_rep_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'description', name: 'description'},
                    { data: 'action', name: 'action'},
                ]
            });

            var received_bottle_table = $('#receivedBottleTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: ''+'?table=received-bottle&sale_rep_id='+"{{request('sale_rep_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'addedBy', name: 'addedBy'},
                    { data: 'created_at', name: 'created_at'},
                ]
            });

            var received_broken_bottle_table = $('#receivedBrokenBottleTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: ''+'?table=received-broken-bottle&sale_rep_id='+"{{request('sale_rep_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'addedBy', name: 'addedBy'},
                    { data: 'created_at', name: 'created_at'},

                ]
            });

           /* var chargeBottleTable = $('#chargeBottleTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: ''+'?table=charges-broken-bottle&sale_rep_id='+"{{request('sale_rep_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'addedBy', name: 'addedBy'},
                    { data: 'created_at', name: 'created_at'},
                ]
            });*/
        })
    </script>
    @endsection