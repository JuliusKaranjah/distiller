
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content"><div class="box box-solid"><!--box start-->
                <form action="{{action('BottleCollectionController@postReceive')}}" method="post" class="box-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="sale_mara_bottle_id" value="{{$saleMaraBottleId}}">
                    <input type="hidden" name="sale_rep_id" value="{{$saleRep->id}}">
                    <input type="hidden" name="mark" value="{{$mark}}">
                    <div class="col-md-12 col-lg-12" style="font-weight: bold">

                        Receive {{$mark=='broken'?'broken ':''}}Bottle from {{@$saleRep->fullName()}}
                        <hr>
                    </div>

                    @if($mark == 'broken')
                        <div class="col-md-12 col-lg-12">

                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">

                                        <label>Select User to Charge</label>

                                        <select class="form-control select2" name="userToCharge">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}" @if($user->id == $saleRep->id) selected @endif>{{$user->fullName()}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>

                        </div>
                        @endif



                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>{{$mark=='broken'?'Broken bottle amount ':'Amount to receive'}}</label>

                                    <input type="number" step="any" required name="amount" class="form-control">

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <button type="submit" class="btn btn-primary">Receive Bottle</button>
                        </div>
                    </div>
                </form>
            </div><!--box end--></div>
    </div>
