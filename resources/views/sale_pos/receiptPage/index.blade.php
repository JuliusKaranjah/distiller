<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice No. </title>
    <base href="https://sma.tecdiary.com/"/>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <link rel="shortcut icon" href="https://sma.tecdiary.com/themes/default/admin/assets/images/icon.png"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://sma.tecdiary.com/themes/default/admin/assets/styles/theme.css"

          type="text/css"/>
    <style type="text/css" media="all">
        body {
            color: #000;
        }

        #wrapper {
            max-width: 480px;
            margin: 0 auto;
            padding-top: 20px;
        }

        .btn {
            border-radius: 0;
            margin-bottom: 5px;
        }

        .bootbox .modal-footer {
            border-top: 0;
            text-align: center;
        }

        h3 {
            margin: 5px 0;
        }

        .order_barcodes img {
            float: none !important;
            margin-top: 5px;
        }

        @media print {
            .no-print {
                display: none;
            }

            #wrapper {
                max-width: 480px;
                width: 100%;
                min-width: 250px;
                margin: 0 auto;
            }

            .no-border {
                border: none !important;
            }

            .border-bottom {
                border-bottom: 1px solid #ddd !important;
            }

            table tfoot {
                display: table-row-group;
            }
        }

        #receipt-data{
            margin: 30px;
        }
    </style>
</head>

<body>
<div id="wrapper">
    <div id="receiptData">
        <div class="no-print">
        </div>
        <div id="receipt-data">
            <div class="text-center">
                {{--<img src="https://sma.tecdiary.com/assets/uploads/logos/logo1.png" alt="">--}}
                <h3 style="text-transform:uppercase;">{{ucwords(strtolower(($business = $transaction->business)->name))}}</h3>
                <h4 style="text-transform:uppercase;">Invoice Note</h4>
                <p>

                    {{--{{($location = $transaction->location)->name}}, {{$location->city}}<br>--}}

                    P.O BOX 7736 - 00100 Nairobi Kenya <br>
                    Off Mombasa Road <br>
                    Office Number: 0722 260928 <br>
                    PIN: P051197116U<br>
                </p>
            </div>
            <p>Date: {{\Carbon\Carbon::now()->toDayDateTimeString()}}<br>Sale No/Ref: {{$transaction->id}}<br></p>
            <p>Customer: {{@$transaction->contact->name}}<br></p>
            <div style="clear:both;"></div>
            <table class="table table-condensed">
                <tbody>
                @if($transaction->sell_lines->count())

                    <tr>
                        <th>#</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Sub Total(KES)</th>
                    </tr>

                    @endif


                <?php $totalAmount = 0;?>
                @foreach($transaction->sell_lines as $sell_line)

                    <tr>
                        <td>
                            {{number_format($loop->iteration)}}
                        </td>

                        <td>
                            {{$sell_line->product->name}}
                        </td>
                        <td>
                            {{number_format($sell_line->quantity,2)}}
                        </td>
                        <td>
                            KES {{number_format($subTotal = $sell_line->quantity*$sell_line->unit_price,2)}}
                        </td>
                    </tr>
                    <?php $totalAmount += $subTotal;?>
                @endforeach




                </tbody>
                <tfoot>

                @if(($discountAmount = $transaction->discount_amount)>0)
                    <tr>
                        <th></th>
                        <th>Sub Total</th>
                        <th></th>
                        <th>KES {{number_format($totalAmount ,2)}}</th>
                    </tr>


                <tr>
                    <th></th>
                    <th>Discount</th>
                    <th></th>
                    <th>KES {{number_format($discountAmount,2)}}</th>
                </tr>
                @endif

                <tr>
                    <th></th>
                    <th>Total</th>
                    <th></th>
                    <th>KES {{number_format($totalAmount - $discountAmount,2)}}</th>
                </tr>

                <tr>
                    <th></th>
                    <th>Code</th>
                    <th>Vatable Amount</th>
                    <th>Total Vat</th>
                </tr>

                <tr>
                    <td></td>
                    <td>VAT 16%</td>
                    <td>KES {{number_format($expectedTotal = $totalAmount - $discountAmount,2)}}</td>
                    <td>KES {{number_format(.16*$expectedTotal,2)}}</td>
                </tr>

                </tfoot>
            </table>
            <table class="table table-striped table-condensed">
                <tbody>



                @foreach($transaction->payment_lines as $payment_line)
                    <tr>
                        <td>Paid by: {{$payment_line->method}}</td>
                        <td class="text-right">{{number_format($payment_line->amount,2)}}</td>

                    </tr>
                @endforeach

                <?php
                ?>



                @if(($paidAmount = $transaction->payment_lines->sum('amount'))<($expectedTotal))

                    <td>Amount Due: </td>
                    <td class="text-right">{{number_format($expectedTotal-$paidAmount,2)}}</td>

                    @endif



                @if(($paidAmount = $transaction->payment_lines->sum('amount'))>($expectedTotal))

                    <td>Balance: </td>
                    <td colspan="2">{{number_format($paidAmount-$expectedTotal,2)}}</td>
                    @endif



                </tbody>
            </table>

            <p class="text-center"> Sale rep.: {{auth()->user()->UserFullName}}</p>
            <p class="text-center"> Alcohol not for sale to persons under 18 years of age.</p>
            <p class="text-center"> Excessive consumption of alcohol is harmful to your health</p>

        </div>

        {{--<div class="order_barcodes text-center">
            <img src="https://sma.tecdiary.com/admin/misc/barcode/U0FMRS9QT1MyMDE4LzA4LzAwMDI/code128/74/0/1"
                 alt="SALE/POS2018/08/0002" class="bcimg"/>
            <br>
            <img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABKAQMAAAAmHlAyAAAABlBMVEUAAAD///+l2Z/dAAAACXBIWXMAAA7EAAAOxAGVKw4bAAABDklEQVQokV3SwWoGIAgAYKFr0KsIXgVfXfAq9CqB18D5b7DliuALQtMCGHdiyoEaf5RgEhbKRiQS3U7jP/dli/+comHUKUG+1H7i/hIGfQZCY+bhsVFSX4aOaUIOjXf5BHEweDlC7wW2zuPuN4JnI9lANEZvnNevV0eicVjoriPauBfT3AJTX6JhVSp44SWzEZOiNe6oTV1yNcpeCCnujfOI7wp3tXFpZfBTwR7qIVznojRypgfhcX25cMitQ5XiYdZiQdqNcOyujRaNJ7fkJNuNn8c/Q0D0ZeZkhCULXtYnwIXfH+ahhFTnjlRTHyK50cVPsMYqNHcu6MQzWRgaJcJrzq0vYZxxIMenzF9+AfOJPACkpd2TAAAAAElFTkSuQmCC'
                 alt='https://sma.tecdiary.com/admin/sales/view/2' class='qrimg'/></div>
        <div style="clear:both;"></div>--}}
    </div>

    <div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
        {{--<hr>
        <span class="pull-right col-xs-12">
                    <button onclick="window.print();"
                            class="btn btn-block btn-primary">Print</button>                </span>
        <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="#" id="email">Email</a></span>
        <span class="col-xs-12">
                    <a class="btn btn-block btn-warning" href="{{url('pos/create')}}">Back to POS</a>
                </span>--}}
        <div style="clear:both;"></div>
        <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
            {{--<p style="font-weight:bold;">
                Please don't forget to disble the header and footer in browser print settings.
            </p>
            <p style="text-transform: capitalize;">
                <strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--
            </p>
            <p style="text-transform: capitalize;">
                <strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None
            </p>--}}
        </div>
        <div style="clear:both;"></div>
    </div>
</div>

<script type="text/javascript"
        src="https://sma.tecdiary.com/themes/default/admin/assets/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="https://sma.tecdiary.com/themes/default/admin/assets/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="https://sma.tecdiary.com/themes/default/admin/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://sma.tecdiary.com/themes/default/admin/assets/js/custom.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#email').click(function () {
            bootbox.prompt({
                title: "Email Address",
                inputType: 'email',
                value: "customer@tecdiary.com",
                callback: function (email) {
                    if (email != null) {
                        $.ajax({
                            type: "post",
                            url: "https://sma.tecdiary.com/admin/pos/email_receipt",
                            data: {token: "5ad5ef08fab3288c03ba4763ac14dcac", email: email, id: 2},
                            dataType: "json",
                            success: function (data) {
                                bootbox.alert({message: data.msg, size: 'small'});
                            },
                            error: function () {
                                bootbox.alert({message: 'Ajax request failed, pleas try again', size: 'small'});
                                return false;
                            }
                        });
                    }
                }
            });
            return false;
        });
    });

    // $(window).load(function () {
    //     window.print();
    //     return false;
    // });

</script>
</body>
</html>
