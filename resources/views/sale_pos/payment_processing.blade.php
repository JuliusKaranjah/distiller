@extends('layouts.app')
@section('title','Payment Processing')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>Payment Processing
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content no-print">
	<div class="box">
        <div class="box-header">
        	
        	<div class="box-tools">
                <a class="btn btn-block btn-primary" href="{{action('SellPosController@create')}}">
				<i class="fa fa-plus"></i> @lang('messages.add')</a>
            </div>
        </div>
        <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="input-group">
                      <button type="button" class="btn btn-primary" id="daterange-btn">
                        <span>
                          <i class="fa fa-calendar"></i> Filter By Date
                        </span>
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                  </div>
            </div>
        </div>
            <div class="table-responsive">
        	<table class="table table-bordered table-striped ajax_view" id="sell_table">
        		<thead>
        			<tr>
                        <th>Added At</th>
                        <th>Sales Rep.</th>
                        <th>Method</th>
                        <th>Status</th>
                        <th>Cheque No.</th>
                        <th>Amount</th>
                        <th>Remit At</th>


        			</tr>
        		</thead>
        	</table>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@stop
@section('javascript')
<script type="text/javascript">
$(document).ready( function(){

    var sell_table = function()
    {
        $("#sell_table").DataTable({
            processing: true,
            serverSide: true,
            ajax: '/sells/approve?is_quatation=1',
            columns: [
                { data: 'created_at', name: 'added_at'  },
                { data: 'made_by.username', name: 'sales_rep'},
                { data: 'method', name: 'method'},
                { data: 'status', name: 'status'},
                { data: 'cheque_number', name: 'cheque_number'},
                { data: 'amount', name: 'amount'},
                { data: 'remit_at', name: 'remit_at'},
            ],

        })
    };

    sell_table();





    /*sell_table = $('#sell_table').DataTable({
        processing: true,
        serverSide: true,
        aaSorting: [[0, 'desc']],
        ajax: '/sells/approve?is_quotation=1',
        columnDefs: [ {
            "targets": 4,
            "orderable": false,
            "searchable": false
        } ],
        columns: [
            { data: 'created_at', name: 'created_at'  },
            { data: 'created_at', name: 'created_at'},
            { data: 'name', name: 'contacts.name'},
            { data: 'business_location', name: 'bl.name'},
            { data: 'action', name: 'action'}
        ],
        "fnDrawCallback": function (oSettings) {
            __currency_convert_recursively($('#purchase_table'));
        }
    });*/
    //Date range as a button
   /* $('#daterange-btn').daterangepicker(
        dateRangeSettings,
        function (start, end) {
            $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
            sell_table.ajax.url( '/sells/draft-dt?is_quotation=1&start_date=' + start.format('YYYY-MM-DD') +
                '&end_date=' + end.format('YYYY-MM-DD') ).load();
        }
    );
    $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
        sell_table.ajax.url( '/sells/draft-dt?is_quotation=1').load();
        $('#daterange-btn span').html('<i class="fa fa-calendar"></i> Filter By Date');
    });*/
});
</script>
	
@endsection