<div class="payment_details_div @if( $payment_line['method'] !== 'card' ) {{ 'hide' }} @endif" data-type="card" >
	<div class="col-md-4">
		<div class="form-group">
			{!! Form::label("card_number_$row_index", __('lang_v1.card_no')) !!}
			{!! Form::text("payment[$row_index][card_number]", $payment_line['card_number'], ['class' => 'form-control', 'placeholder' => __('lang_v1.card_no'), 'id' => "card_number_$row_index"]); !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			{!! Form::label("card_holder_name_$row_index", __('lang_v1.card_holder_name')) !!}
			{!! Form::text("payment[$row_index][card_holder_name]", $payment_line['card_holder_name'], ['class' => 'form-control', 'placeholder' => __('lang_v1.card_holder_name'), 'id' => "card_holder_name_$row_index"]); !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			{!! Form::label("card_transaction_number_$row_index",__('lang_v1.card_transaction_no')) !!}
			{!! Form::text("payment[$row_index][card_transaction_number]", $payment_line['card_transaction_number'], ['class' => 'form-control', 'placeholder' => __('lang_v1.card_transaction_no'), 'id' => "card_transaction_number_$row_index"]); !!}
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label("card_type_$row_index", __('lang_v1.card_type')) !!}
			{!! Form::select("payment[$row_index][card_type]", ['visa' => 'Visa', 'master' => 'MasterCard'], $payment_line['card_type'],['class' => 'form-control select2', 'id' => "card_type_$row_index" ]); !!}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label("card_month_$row_index", __('lang_v1.month')) !!}
			{!! Form::text("payment[$row_index][card_month]", $payment_line['card_month'], ['class' => 'form-control', 'placeholder' => __('lang_v1.month'),
			'id' => "card_month_$row_index" ]); !!}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label("card_year_$row_index", __('lang_v1.year')) !!}
			{!! Form::text("payment[$row_index][card_year]", $payment_line['card_year'], ['class' => 'form-control', 'placeholder' => __('lang_v1.year'), 'id' => "card_year_$row_index" ]); !!}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label("card_security_$row_index",__('lang_v1.security_code')) !!}
			{!! Form::text("payment[$row_index][card_security]", $payment_line['card_security'], ['class' => 'form-control', 'placeholder' => __('lang_v1.security_code'), 'id' => "card_security_$row_index"]); !!}
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="payment_details_div @if( $payment_line['method'] !== 'cheque' ) {{ 'hide' }} @endif" data-type="cheque" >
	<div class="col-md-12">
		<div class="form-group">
			{!! Form::label("cheque_number_$row_index",__('lang_v1.cheque_no')) !!}
			{!! Form::text("payment[$row_index][cheque_number]", $payment_line['cheque_number'], ['class' => 'form-control', 'placeholder' => __('lang_v1.cheque_no'), 'id' => "cheque_number_$row_index"]); !!}
		</div>
	</div>
<div class="col-md-12">
		<div class="form-group">
			{!! Form::label("drawer_name_$row_index",'Drawer Name') !!}
			{!! Form::text("payment[$row_index][drawer_name]", $payment_line['drawer_name'], ['class' => 'form-control', 'placeholder' => 'Drawer name', 'id' => "drawer_name_$row_index"]); !!}
		</div>
	</div>
<div class="col-md-12">
		<div class="form-group">
			{!! Form::label("bank_name_$row_index",'Bank Name') !!}
			{!! Form::text("payment[$row_index][bank_name]", $payment_line['bank_name'], ['class' => 'form-control', 'placeholder' => 'Bank Name', 'id' => "bank_name_$row_index"]); !!}
		</div>
	</div>
</div>

<div class="payment_details_div @if( $payment_line['method'] !== 'mpesa' ) {{ 'hide' }} @endif" data-type="mpesa" >
	<div class="col-md-12">


		<div class="form-group">
			{!! Form::label("cheque_number_$row_index",'Mpesa Phone Number') !!}
			{!! Form::text("payment[$row_index][mpesa_phone]", $payment_line['mpesa_phone'], ['class' => 'form-control mpesa_number', 'placeholder' => 'Mpesa Phone', 'id' => "mpesa_phone_$row_index"]); !!}
		</div>

		<div class="form-group">
			{!! Form::label("cheque_number_$row_index",__('lang_v1.mpesa_code')) !!}
			{!! Form::text("payment[$row_index][mpesa_code]", $payment_line['mpesa_code'], ['class' => 'form-control mpesa_code', 'placeholder' => __('lang_v1.mpesa_code'), 'id' => "mpesa_$row_index"]); !!}
		</div>

	</div>

	<div class="col-md-12">

		<div class="form-group">
			<button id="payWithMpesaMulti" type="button" class="btn btn-primary">Pay With Mpesa</button>
		</div>

		<div class="form-group">
			<button id="ConfirmMpesaMulti" type="button" class="btn btn-primary">Confirm Mpesa</button>
		</div>
	</div>
</div>

<div class="payment_details_div @if( $payment_line['method'] !== 'bank_transfer' ) {{ 'hide' }} @endif" data-type="bank_transfer" >
	<div class="col-md-12">
		<div class="form-group">
			{!! Form::label("bank_account_number_$row_index",__('lang_v1.bank_account_number')) !!}
			{!! Form::text( "payment[$row_index][bank_account_number]", $payment_line['bank_account_number'], ['class' => 'form-control', 'placeholder' => __('lang_v1.bank_account_number'), 'id' => "bank_account_number_$row_index"]); !!}
		</div>
	</div>
</div>