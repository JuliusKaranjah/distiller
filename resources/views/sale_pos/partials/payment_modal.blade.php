<div class="modal fade" tabindex="-1" role="dialog" id="modal_payment">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('lang_v1.payment')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div id="payment_rows_div">
                                @foreach($payment_lines as $payment_line)

                                    @if($payment_line['is_return'] == 1)
                                        @php
                                            $change_return = $payment_line;
                                        @endphp

                                        @continue
                                    @endif

                                    @include('sale_pos.partials.payment_row', ['removable' => !$loop->first, 'row_index' => $loop->index, 'payment_line' => $payment_line])
                                @endforeach
                            </div>
                            <input type="hidden" id="payment_row_index" value="{{count($payment_lines)}}">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-block"
                                        id="add-payment-row">@lang('sale.add_payment_row')</button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('sale_note', __('sale.sell_note') . ':') !!}
                                    {!! Form::textarea('sale_note', !empty($transaction)? $transaction->additional_notes:null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => __('sale.sell_note')]); !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('staff_note', __('sale.staff_note') . ':') !!}
                                    {!! Form::textarea('staff_note',
                                    !empty($transaction)? $transaction->staff_note:null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => __('sale.staff_note')]); !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box box-solid bg-orange">
                            <div class="box-body">
                                <div class="col-md-12">
                                    <strong>
                                        @lang('lang_v1.total_items'):
                                    </strong>
                                    <br/>
                                    <span class="lead text-bold total_quantity">0</span>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <strong>
                                        @lang('sale.total_payable'):
                                    </strong>
                                    <br/>
                                    <span class="lead text-bold total_payable_span">0</span>
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <strong>
                                        @lang('lang_v1.total_paying'):
                                    </strong>
                                    <br/>
                                    <span class="lead text-bold total_paying">0</span>
                                    <input type="hidden" id="total_paying_input">
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <strong>
                                        @lang('lang_v1.change_return'):
                                    </strong>
                                    <br/>
                                    <span class="lead text-bold change_return_span">0</span>
                                {!! Form::hidden("change_return", $change_return['amount'], ['class' => 'form-control change_return input_number', 'required', 'id' => "change_return", 'placeholder' => __('sale.amount'), 'readonly'=>true,'disabled'=>true]); !!}
                                <!-- <span class="lead text-bold total_quantity">0</span> -->
                                    @if(!empty($change_return['id']))
                                        <input type="hidden" name="change_return_id"
                                               value="{{$change_return['id']}}">
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    <hr>
                                    <strong>
                                        @lang('lang_v1.balance'):
                                    </strong>
                                    <br/>
                                    <span class="lead text-bold balance_due">0</span>
                                    <input type="hidden" id="in_balance_due" value=0>
                                </div>


                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('messages.close')</button>
                <button type="submit" class="btn btn-primary" id="pos-save">@lang('sale.finalize_payment')</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!---Used for checkout with Mpesa..-->
<div class="modal fade" tabindex="-1" role="dialog" id="mpesa_details_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('lang_v1.mpesa_transaction_details')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">


                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("Mpesa No",'Mpesa No') !!}
                                {!! Form::text("mpesa_no", null, ['class' => 'form-control', 'placeholder' => 'Mpesa No.', 'id' => "mpesa_no", 'autofocus']); !!}
                            </div>
                        </div>

                        <input type="hidden" name="trans_code" id="trans_code">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("", ('Amount')) !!}
                                {!! Form::text("mpesa_amount", null, ['class' => 'form-control', 'placeholder' => 'amount', 'id' => "mpesa_amount", 'autofocus']); !!}
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("", ('Trans. Code')) !!}
                                {!! Form::text("mpesa_code", null, ['class' => 'form-control', 'placeholder' => 'mpesa_code', 'id' => "mpesa_code", 'autofocus']); !!}
                            </div>
                        </div>


                        <div class="clearfix"></div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label("Amount Due",'Amount Due') !!}
                                {!! Form::text("Amount Due", null, ['class' => 'form-control',
                                'placeholder' => 'Amount Due', 'id' => "amount_due_text", 'ready-only'
                                ,'disabled']); !!}
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label("Balance", ('Balance')) !!}
                                {!! Form::text("balance", null, ['class' => 'form-control',
                                'placeholder' => 'balance', 'id' => "balance_text", 'autofocus', 'ready-only',
                                'disabled']); !!}
                            </div>
                        </div>


                        <div class="clearfix"></div>


                    </div>
                </div>
            </div>



            <div class="modal-footer">

                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-primary" id="mpesaApi">Pay With Mpesa</button>
                    <button type="button" class="btn btn-primary" id="confirmPayment">Mpesa Confirm</button>
                    <button type="button" class="btn btn-primary"
                            id="pos-save-mpesa">@lang('sale.finalize_payment')</button>
                </div>



            </div>

        </div>
    </div>
</div>
<!--end Mpesa modal--->


<!---cash checkout.-->
<div class="modal fade" tabindex="-1" role="dialog" id="cash_checkout_detail">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('lang_v1.cash_transaction_details')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">


                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label("", ('Amount')) !!}
                                {!! Form::text("cash_amount", null, ['class' => 'form-control',
                                'placeholder' => 'cash amount', 'id' => "cash_amount", 'autofocus']); !!}
                            </div>
                        </div>


                        <div class="clearfix"></div>


                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label("Amount Due",'Amount Due') !!}
                                {!! Form::text("Amount Due", null, ['class' => 'form-control',
                                'placeholder' => 'Amount Due', 'id' => "amount_due_cash", 'ready-only'
                                ,'disabled']); !!}
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label("Balance", ('Balance')) !!}
                                {!! Form::text("balance", null, ['class' => 'form-control',
                                'placeholder' => 'balance', 'id' => "balance_cash", 'autofocus', 'ready-only',
                                'disabled']); !!}
                            </div>
                        </div>


                        <div class="clearfix"></div>


                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="pos-save-cash">@lang('sale.finalize_payment')</button>
            </div>

        </div>
    </div>
</div>
<!--end of cash checkout--->


<!-- Used for express checkout card transaction -->
<div class="modal fade" tabindex="-1" role="dialog" id="card_details_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">@lang('lang_v1.card_transaction_details')</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("card_number", __('lang_v1.card_no')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.card_no'), 'id' => "card_number", 'autofocus']); !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("card_holder_name", __('lang_v1.card_holder_name')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.card_holder_name'), 'id' => "card_holder_name"]); !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label("card_transaction_number",__('lang_v1.card_transaction_no')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.card_transaction_no'), 'id' => "card_transaction_number"]); !!}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label("card_type", __('lang_v1.card_type')) !!}
                                {!! Form::select("", ['visa' => 'Visa', 'master' => 'MasterCard'], 'visa',['class' => 'form-control select2', 'id' => "card_type" ]); !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label("card_month", __('lang_v1.month')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.month'),
                                'id' => "card_month" ]); !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label("card_year", __('lang_v1.year')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.year'), 'id' => "card_year" ]); !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                {!! Form::label("card_security",__('lang_v1.security_code')) !!}
                                {!! Form::text("", null, ['class' => 'form-control', 'placeholder' => __('lang_v1.security_code'), 'id' => "card_security"]); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="pos-save-card">@lang('sale.finalize_payment')</button>
            </div>

        </div>
    </div>
</div>