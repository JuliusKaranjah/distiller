@extends('layouts.app')

@section('title', 'POS')

@section('content')

<!-- Content Header (Page header) -->
<!-- <section class="content-header">
    <h1>Add Purchase</h1> -->
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol> -->
<!-- </section> -->
<input type="hidden" id="__precision" value="{{config('constants.currency_precision')}}">

<!-- Main content -->
<section class="content no-print">
	<div class="row">
		<div class="col-md-7 col-sm-12">
			<div class="box box-success">

				<div class="box-header with-border">
					<div class="col-sm-6">
						<h3 class="box-title">POS Terminal <i class="fa fa-keyboard-o hover-q text-muted" aria-hidden="true" data-container="body" data-toggle="popover" data-placement="bottom" data-content="@include('sale_pos.partials.keyboard_shortcuts_details')" data-html="true" data-trigger="hover" data-original-title="" title=""></i></h3>
					</div>
					<input type="hidden" id="item_addition_method" value="{{$business_details->item_addition_method}}">
					@if(is_null($default_location))
						<div class="col-sm-6">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-map-marker"></i>
									</span>
								{!! Form::select('select_location_id', $business_locations, null, ['class' => 'form-control input-sm mousetrap select2',
								'placeholder' => __('lang_v1.select_location'),
								'id' => 'select_location_id',
								'required', 'autofocus'], $bl_attributes); !!}
								<span class="input-group-addon">
										@show_tooltip(__('tooltip.sale_location'))
									</span>
								</div>
							</div>
						</div>
					@endif
				</div>

				<form method="post" action="{{url('pos/store')}}">
				{{--{!! Form::open(['url' => action('SellPosController@store'), 'method' => 'post', 'id' => 'add_pos_sell_form' ]) !!}--}}
				{!! csrf_field() !!}

					<input type="hidden" name="status" value="quotation">

				{!! Form::hidden('location_id', $default_location, ['id' => 'location_id', 'data-receipt_printer_type' => isset($bl_attributes[$default_location]['data-receipt_printer_type']) ? $bl_attributes[$default_location]['data-receipt_printer_type'] : 'browser']); !!}

				<!-- /.box-header -->
				<div class="box-body">

					<div class="@if(!empty($commission_agent)) col-sm-4 @else col-sm-6 @endif">
						<div class="form-group" style="width: 100% !important">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-user"></i>
								</span>
								<input type="hidden" id="default_customer_id"
								value="{{ $walk_in_customer['id']}}" >
								<input type="hidden" id="default_customer_name"
								value="{{ $walk_in_customer['name']}}" >
								{!! Form::select('contact_id',
									[], null, ['class' => 'form-control mousetrap', 'id' => 'customer_id', 'placeholder' => 'Enter Customer name / phone', 'required']); !!}
								<span class="input-group-btn">
									<button type="button" class="btn btn-default bg-white btn-flat add_new_customer" data-name=""><i class="fa fa-plus-circle text-primary fa-lg"></i></button>
								</span>
							</div>
						</div>
					</div>

					@if(!empty($commission_agent))
						<div class="col-sm-4">
							<div class="form-group">
							{!! Form::select('commission_agent',
										$commission_agent, null, ['class' => 'form-control select2', 'placeholder' => __('lang_v1.commission_agent')]); !!}
							</div>
						</div>
					@endif

					<div class="@if(!empty($commission_agent)) col-sm-4 @else col-sm-6 @endif">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-barcode"></i>
								</span>
								{!! Form::text('search_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_product', 'placeholder' => __('lang_v1.search_product_placeholder'),
								'disabled' => is_null($default_location)? true : false,
								'autofocus' => is_null($default_location)? false : true,
								]); !!}
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<!-- Call restaurant module if defined -->
			        @if(in_array('tables' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules))
			        	<span id="restaurant_module_span">
			          		<div class="col-md-3"></div>
			        	</span>
			        @endif

					<div class="row col-sm-12 pos_product_div">

						<?php
                        $sellingPriceTax = 0;
                        if( session()->get('business.enable_inline_tax') != 0)
						{
							$sellingPriceTax = $business_details->sell_price_tax;
						}
						?>

						<input type="hidden" name="sell_price_tax" id="sell_price_tax" value="{{$sellingPriceTax}}">

						<!-- Keeps count of product rows -->
						<input type="hidden" id="product_row_count"
							value="0">
						@php
							$hide_tax = '';
							if( session()->get('business.enable_inline_tax') == 0){
								$hide_tax = 'hide';
							}
						@endphp
						<table class="table table-condensed table-bordered table-striped table-responsive" id="pos_table">
							<thead>
								<tr>
									<th class="text-center">
										@lang('sale.product')
									</th>
									<th class="text-center">
										@lang('sale.qty')
									</th>
									<th class="text-center">
										@lang('sale.unit_price')
									</th>
									<th class="text-center {{$hide_tax}}">
										@lang('sale.tax')
									</th>
									<th class="text-center {{$hide_tax}}">
										@lang('sale.price_inc_tax')
									</th>
									<th class="text-center">
										@lang('sale.subtotal')
									</th>
									<th class="text-center"><i class="fa fa-trash" aria-hidden="true"></i></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					@include('sale_pos.partials.pos_details')

					@include('sale_pos.partials.payment_modal')
				</div>
				<!-- /.box-body -->
				{!! Form::close() !!}
			</div>
			<!-- /.box -->
		</div>

		<div class="col-md-5 col-sm-12">
			@include('sale_pos.partials.right_div')
		</div>
	</div>
</section>

<!-- This will be printed -->
<section class="invoice print_section" id="receipt_section">
</section>
<div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	@include('contact.create', ['quick_add' => true])
</div>
<!-- /.content -->
<div class="modal fade register_details_modal" tabindex="-1" role="dialog"
	aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog"
	aria-labelledby="gridSystemModalLabel">
</div>




@stop

@section('javascript')

	<script src="{{ asset('js/pos.js') }}"></script>
	<script src="{{ asset('js/printer.js?v=' . $asset_v) }}"></script>
	@include('sale_pos.partials.keyboard_shortcuts')

	<!-- Call restaurant module if defined -->
    @if(in_array('tables' ,$enabled_modules) || in_array('modifiers' ,$enabled_modules) || in_array('service_staff' ,$enabled_modules))
    	<script src="{{ asset('js/restaurant.js?v=' . $asset_v) }}"></script>
    @endif


	<script>
        $(function () {

            var urlLink = "{{url('/pos/pay-with-mpesa')}}";
            var urlMpesaConfirm = "{{url('/pos/confirm-with-mpesa')}}";

            $("button#payWithMpesaMulti").on('click',function () {


                var amount = $('input.mpesa_amount').val();
                var phonenumber = $('input.mpesa_number').val();

                if(phonenumber.match(/^[0-9]+$/) != null)
                {

                }else {
                    alert('Enter a valid phone number');
                    return;
                }

                $.ajax({
					url:urlLink,
					method:'POST',
					data:{
					    phonenumber:phonenumber,
						amount:amount,
						_token:"{{csrf_token()}}"
					}
				}).success(function (data) {


                    $("#trans_code").val(data['trans_code']);

                }).error(function (err) {
                    console.error(err);
                })
            });

			$("button#mpesaApi").on('click',function () {

                var phonenumber = $("#mpesa_no").val().replace('+','');
                var amount = $("#mpesa_amount").val();

			    //validate phone number..
                if(phonenumber.match(/^[0-9]+$/) != null)
				{

				}else {
                    alert('Enter a valid phone number');
                    return;
				}


                $.ajax({
					url:urlLink,
					method:'POST',
					data:{
					    phonenumber:phonenumber,
						amount:amount,
						_token:"{{csrf_token()}}"
					}
				}).success(function (data) {

                    $("#trans_code").val(data['trans_code']);
                }).error(function (err) {
                    console.error(err);
                })
            });

            $("button#ConfirmMpesaMulti").on('click',function () {
                var transCode = $("#trans_code").val();

                $.ajax({
                    url:urlMpesaConfirm,
                    method:'POST',
                    data:{
                        transcode:transCode,
                        _token:"{{csrf_token()}}"
                    }
                }).success(function (data) {

                    if(data['confirmed'])
                    {
                        alert('confirmed');
                    }
                    else {
                        alert('not confirmed');
                    }

                    $("input.mpesa_code").val(data['mpesa_code']);

                }).error(function (err) {
                    console.error(err);
                })
            });

			$("button#confirmPayment").on('click',function () {
                var transCode = $("#trans_code").val();

                $.ajax({
                    url:urlMpesaConfirm,
                    method:'POST',
                    data:{
                        transcode:transCode,
                        _token:"{{csrf_token()}}"
                    }
                }).success(function (data) {

					if(data['confirmed'])
					{
					    alert('confirmed');
					}
					else {
                        alert('not confirmed');
					}

                    $("#mpesa_code").val(data['mpesa_code']);

                }).error(function (err) {
                    console.error(err);
                })
            });
        })
	</script>

@endsection