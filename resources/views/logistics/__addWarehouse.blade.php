<div class="box box-solid"><!--box start-->
    <form action="{{action('LogisticController@storeWarehouse')}}" method="post" class="box-body">
        {!! csrf_field() !!}
        <div class="col-md-12 col-lg-12" style="font-weight: bold">

            Add Warehouse
            <hr>
        </div>


        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Warehouse</label>

                            <input type="text" name="name" class="form-control">

                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Note (Optional)</label>

                        <input type="text" name="description" class="form-control">

                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-12 col-lg-12">
            <div class="row pull-left">
                <button id="submitOrder" class="btn btn-primary" value="load">Add Warehouse</button>
            </div>
        </div>

    </div>
</div><!--box end-->