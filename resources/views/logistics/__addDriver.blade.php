<div class="box box-solid"><!--box start-->
    <form action="{{action('LogisticController@storeDriver')}}" method="post" class="box-body">
        {!! csrf_field() !!}
        <div class="col-md-12 col-lg-12" style="font-weight: bold">

            Add Driver
            <hr>
        </div>


        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Driver full name</label>

                            <input type="text" name="full_name" class="form-control" required>

                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Phone</label>

                        <input type="text" name="phone" class="form-control" required>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Note (Optional)</label>

                        <input type="text" name="description" class="form-control">

                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-12 col-lg-12">
            <div class="row pull-left">
                <button id="submitOrder" class="btn btn-primary" value="load">Add Vehicle</button>
            </div>
        </div>

</form>

</div><!--box end-->