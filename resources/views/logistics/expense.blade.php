@extends('layouts.app')

@section('title','Expenses')

@section('content')

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-content">
            </div>
        </div>
    </div>

    <section class="content-header no-print">
        <h1>Expenses
            <small></small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content no-print">


        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Expenses</h3>
                @can('logistics.expense.view')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" id="addExpense" href="{{action('LogisticController@addExpense')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">
                @can('logistics.expense.view')

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped ajax_view" id="driver_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Expense</th>
                                <th>note</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                @endcan
            </div>
        </div>

        <div class="modal fade product_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

    </section>


    <!-- /.content -->
@stop

@section('script')
    <script>

        $(function () {

             $('#driver_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[0, 'desc']],
                ajax: '',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'action', name: 'action'},
                ]
            });


        });

        $("#addExpense").on('click',function (e) {
            e.preventDefault();

            $.ajax({
                url:$(this).attr('href'),
                method:'get'
            }).success(function (data) {
                $('div#modal-content').html(data);
                $("div#modal").modal('show');
            })
        })

    </script>
    @stop