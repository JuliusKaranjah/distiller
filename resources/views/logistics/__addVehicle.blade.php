<div class="box box-solid"><!--box start-->
    <form action="{{action('LogisticController@storeVehicle')}}" method="post" class="box-body">
        {!! csrf_field() !!}
        <div class="col-md-12 col-lg-12" style="font-weight: bold">

            Add Vehicle
            <hr>
        </div>


        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Nickname</label>

                            <input type="text" name="name" class="form-control" required>

                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Number Plate</label>

                        <input type="text" placeholder="eg KCB 293Q" maxlength="8" name="number_plate" class="form-control" required>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Note (Optional)</label>

                        <input type="text" name="description" class="form-control">

                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-12 col-lg-12">
            <div class="row pull-left">
                <button id="submitOrder" class="btn btn-primary" value="load">Add Vehicle</button>
            </div>
        </div>
    </form>
    </div><!--box end-->