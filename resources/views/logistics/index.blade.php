@extends('layouts.app')

@section('title','Logistics')

@section('content')

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-content">
            </div>
        </div>
    </div>


    <!-- Main content -->
    <section class="content no-print">

        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-social-dropbox"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pending Orders</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($pendingOrders)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-info"><i class="ion ion-cube"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Loaded Orders</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($loadedOrders)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"><i class="ion ion-android-bus"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Orders On Transit</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($dispatchedOrders)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="ion ion-ios-list"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Dispatched</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($dispatchedOrders + $receivedOrders)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-handshake-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Received Orders</span>
                        <span class="info-box-number total_cash_at_hand">{{number_format($receivedOrders)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

        </div>



        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#sr_pending_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes" aria-hidden="true"></i> Pending Orders</a>
                </li>

                <li>
                    <a href="#sr_loaded_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes" aria-hidden="true"></i> Loaded Orders</a>
                </li>

                <li>
                    <a href="#sr_loading_bay_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes" aria-hidden="true"></i> Loading Bay</a>
                </li>

                <li>
                    <a href="#sr_dispatched_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes" aria-hidden="true"></i> Dispatched Orders</a>
                </li>


                <li>
                    <a href="#sr_receive_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cubes" aria-hidden="true"></i> Received Orders</a>
                </li>


            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="sr_pending_tab">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Pending Orders</h3>
                        </div>
                        <div class="box-body">
                            @can('logistics.view')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped ajax_view" id="order_table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Approved At</th>
                                            <th>Ordered By</th>
                                            <th>Status</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="sr_loaded_tab">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Loaded Orders</h3>
                        </div>
                        <div class="box-body">
                            @can('logistics.view')

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive" style="width: 100%">
                                            <table class="table table-bordered table-striped ajax_view" id="order_loaded_table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Loaded At</th>
                                                    <th>Ordered By</th>
                                                    <th>Status</th>
                                                    <th>Origin</th>
                                                    <th>Destination</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="sr_dispatched_tab">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Dispatched Orders</h3>
                        </div>
                        <div class="box-body">
                            @can('logistics.view')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped ajax_view" id="order_dispatched_table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Dispatched At</th>
                                            <th>Ordered By</th>
                                            <th>Status</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="sr_loading_bay_tab">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Loading bay</h3>
                        </div>
                        <div class="box-body">
                            @can('logistics.view')

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped ajax_view" id="loading_bay_table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Dispatched At</th>
                                            <th>Ordered By</th>
                                            <th>Status</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="sr_receive_tab">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">All Received Orders</h3>
                        </div>
                        <div class="box-body">
                            @can('logistics.view')
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                                    <i class="fa fa-caret-down"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped ajax_view" id="order_received_table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Received At</th>
                                            <th>Ordered By</th>
                                            <th>Status</th>
                                            <th>Origin</th>
                                            <th>Destination</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>



            </div>

        </div>


        <div class="modal fade product_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

    </section>


    <!-- /.content -->
@stop

@section('script')
    <script>

        $(function () {

            $('#daterange-btn').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    order_table.ajax.url('?start_date=' + start.format('YYYY-MM-DD') +
                        '&end_date=' + end.format('YYYY-MM-DD')).load();
                }
            );
            $('#daterange-btn').on('cancel.daterangepicker', function (ev, picker) {
                order_table.ajax.url('').load();
                $('#daterange-btn span').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
            });


            var order_table = $('#order_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                ajax: '',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'action_at', name: 'action_at'},
                    {data: 'ordered_by', name: 'ordered_by'},
                    {data: 'status', name: 'status'},
                    {data: 'origin', name: 'origin'},
                    {data: 'destination', name: 'destination'},
                    {data: 'action', name: 'action'},
                ]
            });

            var order_loaded_table = $('#order_loaded_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                ajax: '?status=loaded',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'loaded_at', name: 'loaded_at'},
                    {data: 'ordered_by', name: 'ordered_by'},
                    {data: 'logistic_status', name: 'logistic_status'},
                    {data: 'origin', name: 'origin'},
                    {data: 'destination', name: 'destination'},
                    {data: 'action', name: 'action'},
                ]
            });

            var order_dispatched_table = $('#order_dispatched_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                ajax: '?status=dispatched',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'dispatched_at', name: 'dispatched_at'},
                    {data: 'ordered_by', name: 'ordered_by'},
                    {data: 'logistic_status', name: 'logistic_status'},
                    {data: 'origin', name: 'origin'},
                    {data: 'destination', name: 'destination'},
                    {data: 'action', name: 'action'},
                ]
            });

            var order_received_table = $('#order_received_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                ajax: '?status=received',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'received_at', name: 'received_at'},
                    {data: 'ordered_by', name: 'ordered_by'},
                    {data: 'logistic_status', name: 'logistic_status'},
                    {data: 'origin', name: 'origin'},
                    {data: 'destination', name: 'destination'},
                    {data: 'action', name: 'action'},
                ]
            });


            var order_received_table = $('#loading_bay_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                ajax: '?status=sr_loading_bay_tab',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'sent_loading_bay_at', name: 'sent_loading_bay_at'},
                    {data: 'ordered_by', name: 'ordered_by'},
                    {data: 'logistic_status', name: 'logistic_status'},
                    {data: 'origin', name: 'origin'},
                    {data: 'destination', name: 'destination'},
                    {data: 'action', name: 'action'},
                ]
            });

            $("table.table").on('click','a[view-details]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET'
                }).success(function (data) {
                    $('div#modal-content').html(data);
                    $("div#modal").modal('show');
                })
            });




            $("table.table").on('click','a[print-invoice-note]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET'
                }).success(function (data) {

                    $("section#receipt_section").html(data).printThis();

                })
            });

        });

    </script>
    @stop