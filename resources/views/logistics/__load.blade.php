<div class="box box-solid"><!--box start-->
    <form class="box-body" method="post" action="{{action('LogisticController@PostLogistic')}}" id="logisticForm">
        {!! csrf_field() !!}

        <input type="hidden" name="orderId" value="{{$order->id}}">
        <input type="hidden" name="action" id="logisticFormAction">
        <div class="col-md-12 col-lg-12" style="font-weight: bold">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <p>Order #{{$order->id}}</p>
                        <p>Date: {{$order->created_at->toDayDateTimeString()}}</p>
                        <p>Ordered By: {{$order->addedBy->fullName()}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <p>Origin: {{$order->locationFrom->name}}</p>
                        <p>Destination: {{$order->locationTo->name}}</p>

                    </div>
                </div>
            </div>
            <hr>
        </div>


        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Select Warehouse</label>
                        <select class="form-control" name="warehouse_id" required>
                            <option value="">Select a warehouse</option>
                            @foreach($warehouses as $warehouse)
                                <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Select vehicle</label>
                        <select class="form-control" name="vehicle_id" required>
                            <option value="">Select a vehicle</option>
                            @foreach($vehicles as $vehicle)
                                <option value="{{$vehicle->id}}">{{$vehicle->name()}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">


                        <label>Select driver</label>
                        <select class="form-control" name="driver_id" required>
                            <option value="">Select a driver</option>
                            @foreach($drivers as $driver)
                                <option value="{{$driver->id}}">{{$driver->full_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">



                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <label>Select Expenses</label><br>
                    <div class="col-md-4">
                    <select name="expense_type" id="expenseType" class="form-control select2">
                        <option value="">Select Expense</option>
                        @foreach($expenseCategories as $expenseCategory)
                            <option value="{{$expenseCategory->id}}">{{$expenseCategory->name}}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-md-4">
                        <input type="number" placeholder="Cost" step="0.01" name="cost" id="cost" class="form-control">
                    </div>
                    <div class="col-md-4">
                         <span class="input-group-btn">
                                <button type="button" id="addExpenseType" class="btn btn-default">Add Expense</button>
                                </span>
                    </div>
                </div>


            </div>
            <hr>
            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <th>Expense</th>
                        <th>Cost</th>
                        <th>Action</th>
                        </thead>
                        <tbody id="LogisticExpense">

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">
            <div class="row pull-left">
                <button id="submitOrder" class="btn btn-primary loadDispatch" type="button" value="load">Approve Loading</button>
                {{--<button id="submitOrder" class="btn btn-primary loadDispatch" type="button" value="load_and_dispatch">Load and Dispatch</button>--}}
            </div>
        </div>

    </form>
</div><!--box end-->

<script>
    $(function () {
        $("button#addExpenseType").on('click',function () {

            var cost = parseFloat($("#cost").val());

            var expenseId = $("#expenseType").val();
            var expenseText = $("#expenseType option:selected").text();

            if(expenseText == 'Select Expense')
            {
                alert('Select expense.');
                return;
            }

            var hiddenInputs = '<input type="hidden" name="cost[]" value="'+cost+'">';
                hiddenInputs += '<input type="hidden" name="expense[]" value="'+expenseId+'">';


            var html = '<tr>'+hiddenInputs+'<td>'+expenseText+'</td><td>'+cost+'</td>';

            html += '<td><button class="btn btn-danger deleteExpense"><i class="fa fa-trash"></i></button></td></tr>';

            $("tbody#LogisticExpense").append(html);

            $("#cost").val(0);
            $("#expenseType").prop('selectedIndex',0)
        });

        $(document).on('click','button.deleteExpense',function () {
            $(this).closest('tr').remove();
        });

        $("button.loadDispatch").on('click',function () {
            $("#logisticFormAction").val($(this).val());

            if($("select[name=warehouse_id]").val() == '')
            {
                alert('select warehouse');
                return;
            }
            if($("select[name=vehicle_id]").val() == '')
            {
                alert('select vehicle');
                return;
            }
            if($("select[name=driver_id]").val() == '')
            {
                alert('select driver');
                return;
            }


            $("form#logisticForm").submit();
        })


    })
</script>