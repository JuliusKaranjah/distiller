{{--input_data = $request->only([ 'location_id', 'ref_no', 'transaction_date', 'additional_notes', 'shipping_charges', 'final_total']);--}}
<?php
$badgeClass = 'default';
if($order->status == 'approved')
{
    $badgeClass = 'success';
}elseif ($order->status == 'partially approved')
{
    $badgeClass = 'primary';
}elseif ($order->status == 'declined')
{
    $badgeClass = 'danger';
}
$accBadgeClass = 'default';
if($order->accountant_status == 'approved')
{
    $accBadgeClass = 'success';
}elseif ($order->accountant_status == 'partially approved')
{
    $accBadgeClass = 'primary';
}elseif ($order->accountant_status == 'declined')
{
    $accBadgeClass = 'danger';
}
?>
<div class="modal-header">
    <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modalTitle"> Order Details (<b>Order Ref. No:</b> #{{ $order->getId() }})
    </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">

            <p class="pull-right"><b>Date:</b> {{ @($order->created_at->toDayDateTimeString()) }}</p>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
           Sales Rep :
            <address>
                <strong>{{ ($saleRep = $order->addedBy)->fullName() }}</strong>
                <br>
                Email Address: {{$saleRep->email}}<br>

            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            Business:
            <address>
                <strong>Crywan Distillers</strong>


                    <br>Athi Business Park


                    <br>Mlolongo,Nairobi,Kenya


                    <br>VAT: P051197116U


            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            <b>Order Ref:</b> #{{ $order->getId()}}<br/>
            <b>Date:</b> {{ $order->created_at->toDayDateTimeString() }}<br/>
            <b>Order Status:</b>
            <span class="badge badge-{{$badgeClass}}">{{ucfirst($order->status)}}</span>
            <br>
            @if($order->status=='declined')
                <b>Reason:</b>
                <br>
                {{$order->note}}
                @endif
            <br><b>Order Accountant Approval:</b>
            <span class="badge badge-{{$accBadgeClass}}">{{ucfirst($order->accountant_status)}}</span>
            <br>
            @if($order->accountant_status=='declined')
                <b>Reason:</b>
                <br>
                {{$order->note}}
                @endif
            <br>
        </div>
    </div>

    <br>
    <div class="row">

        <form action="{{url('order/approve-order')}}" method="POST" id="orderApprovalForm">
            {!! csrf_field() !!}
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <input type="hidden" id="action" value="approve">
            <div class="col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table bg-gray" id="orderItemsTable">
                        <thead>
                        <tr class="bg-green">
                            <th>#</th>
                            <th>Product</th>
                            <th>Ordered</th>
                            <th>Available</th>

                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total=0?>
                        @foreach($order->orderItems as $orderItem)
                            <?php
                            $badgeClass = 'default';
                            if($orderItem->status == 'approved')
                            {
                                $badgeClass = 'success';
                            }elseif ($orderItem->status == 'partially approved')
                            {
                                $badgeClass = 'primary';
                            }elseif ($orderItem->status == 'declined')
                            {
                                $badgeClass = 'danger';
                            }
                            ?>
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ ($product = $orderItem->product)->name }}
                                    @php

                                    $prodDetails = (new \App\Utils\ProductUtil())->getDetailsFromVariation($product->id,getBusiness()['id'],getBusinessOrderOriginLocationId(),true);

                                    $qty = floatval(@$prodDetails->qty_available);


                                    @endphp
                                    @if( $product->type == 'variable')
                                        - {{ $product->variations->product_variation->name}}
                                        - {{ $product->variations->name}}
                                    @endif
                                </td>
                                <td>

                                    @if($showAvailabe = false)

                                        @else
                                        {{$orderItem->quantity}}
                                        @endif
                                </td>
                                <td>
                                    @if($showAvailabe)
                                    {{$qty}}
                                        @else
                                        -
                                        @endif
                                </td>

                                <td><span class="badge badge-{{$badgeClass}}">{{ucfirst($orderItem->status)}}</span></td>


                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <br>




</div>