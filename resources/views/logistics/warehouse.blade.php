@extends('layouts.app')

@section('title','Pending Orders')

@section('content')

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-content">
            </div>
        </div>
    </div>

    <section class="content-header no-print">
        <h1>Warehouses
            <small></small>
        </h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content no-print">


        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Warehouses</h3>
                @can('logistics.warehouse.view')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" id="addWarehouse" href="{{action('LogisticController@addWarehouse')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">
                @can('logistics.warehouse.view')

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped ajax_view" id="warehouse_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Warehouse</th>
                                <th>Status</th>
                                <th>note</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                @endcan
            </div>
        </div>

        <div class="modal fade product_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

    </section>


    <!-- /.content -->
@stop

@section('script')
    <script>

        $(function () {

            var warehouse_table = $('#warehouse_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[0, 'desc']],
                ajax: '',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'status', name: 'status'},
                    {data: 'description', name: 'description'},
                    {data: 'action', name: 'action'},
                ]
            });


        });

        $("#addWarehouse").on('click',function (e) {
            e.preventDefault();

            $.ajax({
                url:$(this).attr('href'),
                method:'get'
            }).success(function (data) {
                $('div#modal-content').html(data);
                $("div#modal").modal('show');
            })
        })

    </script>
    @stop