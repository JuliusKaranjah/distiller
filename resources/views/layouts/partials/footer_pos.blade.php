<!-- Main Footer -->
  <footer class="no-print text-center text-info">
    <!-- To the right -->
    <!-- <div class="pull-right hidden-xs">
      Anything you want
    </div> -->
    <!-- Default to the left -->
    <small>
    	<b>{{ config('app.name', 'Marcus POS') }}  | Copyright &copy; {{ date('Y') }} All rights reserved.</b>
    </small>
</footer>