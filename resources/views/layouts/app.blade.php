@inject('request', 'Illuminate\Http\Request')

@if($request->segment(1) == 'pos' && ($request->segment(2) == 'create' || $request->segment(3) == 'edit'))
    @php
        $pos_layout = true;
    @endphp
@else
    @php
        $pos_layout = false;
    @endphp
@endif

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{in_array(session()->get('user.language', config('app.locale')), config('constants.langs_rtl')) ? 'rtl' : 'ltr'}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - {{ Session::get('business.name') }}</title>

        <script src="{{ asset('AdminLTE/plugins/pace/pace.min.js?v=' . $asset_v) }}"></script>
        <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/pace/pace.css?v='.$asset_v) }}">

        @include('layouts.partials.css')

        @yield('css')
    </head>

    <body class="@if($pos_layout) hold-transition lockscreen @else hold-transition skin-blue sidebar-mini @endif">

        <div class="wrapper">
            <script type="text/javascript">
                if(localStorage.getItem("upos_sidebar_collapse") == 'true'){
                    var body = document.getElementsByTagName("body")[0];
                    body.className += " sidebar-collapse";
                }
            </script>
            @if(!$pos_layout)
                @include('layouts.partials.header')
                @include('layouts.partials.sidebar')
            @else
                @include('layouts.partials.header-pos')
            @endif


            <!-- Content Wrapper. Contains page content -->
            <div class="@if(!$pos_layout) content-wrapper @endif">


                <!-- Add currency related field-->
                <input type="hidden" id="__code" value="{{session('currency')['code']}}">
                <input type="hidden" id="__symbol" value="{{session('currency')['symbol']}}">
                <input type="hidden" id="__thousand" value="{{session('currency')['thousand_separator']}}">
                <input type="hidden" id="__decimal" value="{{session('currency')['decimal_separator']}}">
                <input type="hidden" id="__symbol_placement" value="{{session('business.currency_symbol_placement')}}">
                <!-- End of currency related field-->

                @if (session('status'))
                    <input type="hidden" id="status_span" data-status="{{ session('status.success') }}" data-msg="{{ session('status.msg') }}">
                @endif
                @yield('content')
                @if(config('constants.iraqi_selling_price_adjustment'))
                    <input type="hidden" id="iraqi_selling_price_adjustment">
                @endif

                <!-- This will be printed -->
                <section class="invoice print_section" id="receipt_section">
                </section>

            </div>
            @include('home.todays_profit_modal')
            <!-- /.content-wrapper -->

            @if(!$pos_layout)
                @include('layouts.partials.footer')
            @else
                @include('layouts.partials.footer_pos')
            @endif

            {{--<audio id="success-audio">
              <source src="/audio/success.ogg" type="audio/ogg">
              <source src="/audio/success.mp3" type="audio/mpeg">
            </audio>
            <audio id="error-audio">
              <source src="/audio/error.ogg" type="audio/ogg">
              <source src="/audio/error.mp3" type="audio/mpeg">
            </audio>
            <audio id="warning-audio">
              <source src="/audio/warning.ogg" type="audio/ogg">
              <source src="/audio/warning.mp3" type="audio/mpeg">
            </audio>--}}

        </div>

        @include('layouts.partials.javascripts')
        <div class="modal fade view_modal" tabindex="-1" role="dialog"
        aria-labelledby="gridSystemModalLabel"></div>

        <script>
            $(function () {
                $("select.select2").select2();
                $("select.select2-input").select2({
                    tags:true
                });

                var moneyFormat = function (currency,amount,hideCurrency=false) {

                    if(hideCurrency)
                    {
                        currency = '';
                    }

                    return (amount<0 ? "-"+currency+" " : ''+currency+' ') + parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
                };
            })



        </script>
        @if(session()->has('success'))
            <script>
                $(function () {
                    @if(session('success'))
                    toastr.success("{{session()->has('msg')?session()->get('msg'):(session()->has('success')?session()->get('success'):'Success')}}", 'Success!');
                    @else
                    toastr.error("{{session()->has('msg')?session()->get('msg'):(session()->has('success')?session()->get('success'):'Success')}}", 'Error!');
                    @endif
                })
            </script>
        @endif
    </body>

    <div class="modal fade mapViewHere" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" id="back" >
                <div class="modal-header">
                    <h4>Location</h4>
                </div>
                <div class="modal-body">
                    <div id="map"></div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>
@yield('script')

    <script>
        $(function () {


            $(document).on('click','.showMap',function () {


                var lat= ($(this).html().split('&amp;')[0]);
                var lon = ($(this).html().split('&amp;')[1]);

                var image = 'https://image.maps.api.here.com/mia/1.6/mapview?'+lat+'&'+lon+'&app_id=b6O6r4vfs5WPjUt4o0qD&app_code=QIrlWLvkXtNT97j2lUA0MQ';
                // var image = 'https://image.maps.api.here.com/mia/1.6/mapview?poi=52.5%2C13.4%2C41.9%2C12.5%2C51.5%2C-0.1%2C48.9%2C2.3%2C40.4%2C-3.7&poitxs=16&poitxc=black&poifc=yellow&app_id=b6O6r4vfs5WPjUt4o0qD&app_code=QIrlWLvkXtNT97j2lUA0MQ';

                $("div#map").html('<img src="'+image+'">');

                $("div.mapViewHere").modal('show');

            });


            $(document).on('submit','form',function () {

                $("div.wrapper").css('opacity','0.4');

                $(this).find('button[type=submit]').addClass('disabled').html('submitting...');

            });

            $(document).on('click','a[view-modal-popup]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET',
                    success:function (data) {
                        $("div.view_modal").html(data).modal('show');
                    }
                })
            })
        })
    </script>
</html>