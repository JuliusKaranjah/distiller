<div class="modal-dialog" role="document">
  <div class="modal-content">

    {!! Form::open(['url' => action('TransactionPaymentController@store'), 'method' => 'post', 'id' => 'transaction_payment_add_form' ]) !!}
    {!! Form::hidden('transaction_id', $transaction->id); !!}

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">@lang( 'purchase.add_payment' )</h4>
    </div>

    <div class="modal-body">
      <div class="row">
        <div class="col-md-4">
          <div class="well">
            <strong>
            @if($transaction->type == 'purchase')
              @lang('purchase.supplier') 
            @elseif($transaction->type == 'sell')
              @lang('contact.customer') 
            @endif
            </strong>:{{ $transaction->contact->name }}<br>
            @if($transaction->type == 'purchase')
            <strong>@lang('business.business'): </strong>{{ $transaction->contact->supplier_business_name }}
            @endif
          </div>
        </div>
        <div class="col-md-4">
          <div class="well">
           @if($transaction->type == 'purchase')
            <strong>@lang('purchase.ref_no'): </strong>{{ $transaction->ref_no }}
          @elseif($transaction->type == 'sell')
             <strong>@lang('sale.invoice_no'): </strong>{{ $transaction->invoice_no }}
          @endif
            <br>
            <strong>@lang('purchase.location'): </strong>{{ $transaction->location->name }}
          </div>
        </div>
        <div class="col-md-4">
          <div class="well">
            <strong>@lang('sale.total_amount'): </strong><span class="display_currency" data-currency_symbol="true">{{ $transaction->final_total }}</span><br>
            <strong>@lang('purchase.payment_note'): </strong>
            @if(!empty($transaction->additional_notes))
            {{ $transaction->additional_notes }}
            @else
              --
            @endif
          </div>
        </div>
      </div>
      <div class="row payment_row">
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label("amount" ,'Amount:*') !!}
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-money"></i>
              </span>
              {!! Form::text("amount", @num_format($payment_line->amount), ['class' => 'form-control input_number', 'required', 'placeholder' => 'Amount']); !!}
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label("paid_on" ,'Paid on:*') !!}
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </span>
              {!! Form::text('paid_on', date('m/d/Y', strtotime($payment_line->paid_on) ), ['class' => 'form-control', 'readonly', 'required']); !!}
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            {!! Form::label("method" ,'Pay Via:*') !!}
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-money"></i>
              </span>
              {!! Form::select("method", $payment_types, $payment_line->method, ['class' => 'form-control select2 payment_types_dropdown', 'required', 'style' => 'width:100%;']); !!}
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 hidden mpesaPayment">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Mpesa Phone</label>
              <input type="text" class="form-control" name="mpesa_number" id="mpesa_phone">
            </div>

          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label">Mpesa Code</label>
              <input type="text" class="form-control" name="mpesa_code" id="mpesa_code">
              <input type="hidden" class="form-control" name="trans_code" id="trans_code">
            </div>

          </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 mpesaPayment hidden">
          <div class="btn-group btn-group-sm">
            <button type="button" class="btn btn-primary" id="mpesaApi">Pay With Mpesa</button>
            <button type="button" class="btn btn-primary" id="confirmPayment">Mpesa Confirm</button>
          </div>
        </div>
        <div class="clearfix"></div>




          @include('transaction_payment.payment_type_details')
        <div class="col-md-12">
          <div class="form-group">
            {!! Form::label("note",'Payment Note:') !!}
            {!! Form::textarea("note", $payment_line->note, ['class' => 'form-control', 'rows' => 3]); !!}
          </div>
        </div>
      </div>
    </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">@lang( 'messages.save' )</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">@lang( 'messages.close' )</button>
    </div>

    {!! Form::close() !!}

  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<script>
  $(function () {

      var urlLink = "{{url('/pos/pay-with-mpesa')}}";
      var urlMpesaConfirm = "{{url('/pos/confirm-with-mpesa')}}";


      $("#method").on('change',function () {
          if($(this).val() == 'mpesa')
          {
              $("div.mpesaPayment").removeClass('hidden');
          }else {
              $("div.mpesaPayment").addClass('hidden');
          }
      });

      $("button#mpesaApi").on('click',function () {

          var phonenumber = $("#mpesa_phone").val().replace('+','');
          var amount = $("#amount").val();

          //validate phone number..
          if(phonenumber.match(/^[0-9]+$/) != null)
          {

          }else {
              alert('Enter a valid phone number');
              return;
          }


          $.ajax({
              url:urlLink,
              method:'POST',
              data:{
                  phonenumber:phonenumber,
                  amount:amount,
                  _token:"{{csrf_token()}}"
              }
          }).success(function (data) {

              $("#trans_code").val(data['trans_code']);
          }).error(function (err) {
              console.error(err);
          })
      });


      $("button#confirmPayment").on('click',function () {
          var transCode = $("#trans_code").val();

          $.ajax({
              url:urlMpesaConfirm,
              method:'POST',
              data:{
                  transcode:transCode,
                  _token:"{{csrf_token()}}"
              }
          }).success(function (data) {

              if(data['confirmed'])
              {
                  alert('confirmed');
              }
              else {
                  alert('not confirmed');
              }

              $("#mpesa_code").val(data['mpesa_code']);

          }).error(function (err) {
              console.error(err);
          })
      });
  })
</script>