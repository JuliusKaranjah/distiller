@extends('layouts.app')

@section('title','Assign Promotion Products')


@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Assign Promotion Products</h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => action('SampleController@storePromotion'), 'method' => 'post', 'id' => 'add_promotion' ]) !!}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Select Customer</label>
                            <select class="form-control select2" name="customer">
                                <option value="">Select Customer</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Select Products</label>
                            <select class="form-control select2" id="productSelect">
                                <option value="">Select Products</option>
                                @foreach($stocks as $stock)
                                    <option value="{{$stock->id}}" data-stock="{{$stock->quantity}}">{{$stock->product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Quantity</label>
                            <p class="input-group">
                                <input type="number" class="form-control" id="quantity">
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="addOrderProduct"><i class="fa fa-plus"></i></button>
                                </span>
                            </p>
                            <span class="error errorReport"></span>

                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Action</th>
                            </thead>
                            <tbody id="orderSampleProductsTable">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary">Assign Promotion</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

    <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    </div>

@endsection
@section('script')
    <script>
        $(function () {

            // $("table.table").DataTable();

            $("button#addOrderProduct").on('click',function () {

                var stock = validateSampleProductStock();
                if(stock.status == 'ERROR')
                {
                    alert(stock.message);
                    return;
                }

                var productId = $("#productSelect").val();
                var productName = $("#productSelect option:selected").text();
                var productQuantity = $("#quantity").val();

                var html = '<tr>' +
                    '<td> <input type="hidden" name="product[]" value="'+productId+'">'+productName+'</td>' +
                    '<td> <input type="hidden" name="quantity[]" value="'+productQuantity+'"> '+productQuantity+'</tdtext>' +
                    '<td><button class="btn btn-danger"><i class="fa fa-trash"></i></button></td>' +
                    '</tr>';

                $("tbody#orderSampleProductsTable").append(html);


            });

            $("#quantity").on('input',function () {
               var stock = validateSampleProductStock();
               if(stock.status == 'ERROR'){
                    $("span.errorReport").show().html(stock.message)
               }else {
                   $("span.errorReport").hide().html('')
               }
            });

            var validateSampleProductStock = function(){
                var available = $("#productSelect option:selected").attr('data-stock');
                var quantity = parseFloat($("#quantity").val());

                if(quantity>available)
                {
                    return {'available':available,'message' : 'Out of stock. Only '+available+' available','status':'ERROR'}
                }else {
                    return {'available':available,'message' : ''+available+' available','status':'OK'}
                }
            };

            $("tbody#orderSampleProductsTable").on('click','button.btn-danger',function () {
                $(this).closest('tr').remove();
            });

        })
    </script>
    @endsection