<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" id="modal-content"><div class="modal-header">
            <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="modalTitle">Promotion Order Details (<b>Ref. No:</b> #{{$sampleOrder->getId()}})
            </h4>

            <span class="pull-left">Sales Rep <strong>{{$sampleOrder->addedBy->fullName()}}</strong></span>
            <span class="pull-right">Date <strong>{{$sampleOrder->created_at->toDayDateTimeString()}}</strong></span>
        </div>
        <div class="modal-body">


            <br>
            <div class="row">

                <form action="{{action('SampleController@accountantApprove')}}" method="POST" id="orderAccountApprovalForm">
                    {!! csrf_field() !!}
                    <input type="hidden" name="action" id="action">
                    <input type="hidden" name="order_id" value="{{$sampleOrder->id}}">
                    <div class="col-sm-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table bg-gray" id="orderItemsTable">
                                <thead>
                                <tr class="bg-green">
                                    <th>#</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody>


                                    @foreach($sampleOrder->sampleOrderProducts as $orderProduct)
                                        <tr>
                                            <td>
                                                {{$loop->iteration}}
                                            </td>
                                            <td>
                                                {{$orderProduct->product->name}}
                                            </td>
                                            <td>
                                                @if(($status = $orderProduct->status) != 'approved')
                                                    <span class="quantity hidden" id="span{{$loop->index}}">{{$orderProduct->quantity}}</span>
                                                    <input type="number" class="form-control quantity" span="{{$loop->index}}" name="quantity[]">
                                                    <input type="hidden" value="{{$orderProduct->id}}" class="form-control" name="product[]">
                                                    @else
                                                    {{number_format($orderProduct->quantity)}}
                                                    @endif

                                            </td>
                                        </tr>
                                        @endforeach


                                <tr><td colspan="1"></td>
                                    <td>Total</td>
                                    <td><strong><span id="totalCount">{{number_format($sampleOrder->sampleOrderProducts->sum('quantity'))}} Items</span></strong></td>


                                </tr></tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <br>

            @if($sampleOrder->accountant_status != 'approved')

            <div id="actions">

                <button class="btn btn-primary Approve" type="button">Approve</button>
                @if($sampleOrder->accountant_status != 'declined')
                <button class="btn btn-danger Decline" type="button">Decline</button>

                    @endif
            </div>
                @endif

        </div>
    </div>
</div>

<script>
    $(function () {
      $("input.quantity").each(function (index) {
         $(this).val($(this).closest('tr').find('span').html());
      });

      var getTotal = function()
      {
          var total = 0;

          $("input.quantity").each(function (index) {
              if(($(this).val())!='')
              {
                  total += parseFloat(($(this).val()));
              }
          });

          $("#totalCount").html(total);
      };


        $("input.quantity").on('input',function () {
        getTotal();
      });

        $("button.Approve").on('click',function () {
            $("#action").val('approve');
            $("form#orderAccountApprovalForm").submit();
        });

        $("button.Decline").on('click',function () {
            $("#action").val('decline');
            $("form#orderAccountApprovalForm").submit();
        })
    })
</script>