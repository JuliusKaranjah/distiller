@extends('layouts.app')

@section('title','Order For Promotion Products')


@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Order For Promotion Products</h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => action('SampleController@storeOrder'), 'method' => 'post', 'id' => 'visit_add_form' ]) !!}
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Select Products</label>
                            <select class="form-control select2" id="productSelect">
                                <option value="">Select Products</option>
                                @foreach($products as $product)
                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Quantity</label>
                            <p class="input-group">
                                <input type="number" class="form-control" id="quantity">
                                <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="addOrderProduct"><i class="fa fa-plus"></i></button>
                                </span>
                            </p>

                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Action</th>
                            </thead>
                            <tbody id="orderSampleProductsTable">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary">Make Order</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

    <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    </div>

@endsection
@section('script')
    <script>
        $(function () {

            // $("table.table").DataTable();

            $("button#addOrderProduct").on('click',function () {

                var productId = $("#productSelect").val();
                var productName = $("#productSelect option:selected").text();
                var productQuantity = $("#quantity").val();

                var html = '<tr>' +
                    '<td> <input type="hidden" name="product[]" value="'+productId+'">'+productName+'</td>' +
                    '<td> <input type="hidden" name="quantity[]" value="'+productQuantity+'"> '+productQuantity+'</tdtext>' +
                    '<td><button class="btn btn-danger"><i class="fa fa-trash"></i></button></td>' +
                    '</tr>';

                $("tbody#orderSampleProductsTable").append(html);


            });


            $("tbody#orderSampleProductsTable").on('click','button.btn-danger',function () {
                $(this).closest('tr').remove();
            })
        })
    </script>
    @endsection