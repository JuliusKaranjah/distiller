@extends('layouts.app')

@section('title','Promotion Products')


@section('content')
    <section class="content-header no-print">
        <h1>Promotion Products
            <small></small>
        </h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="modal fade" id="modal" tabindex="-1" role="dialog" style="font-size: calc(100%); display: none; padding-right: 7px;">

        </div>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Promotion Products</h3>

                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" href="{{action('SampleController@createOrder')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>

            </div>
            <div class="box-body">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                    <i class="fa fa-caret-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view" id="visitTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Added By</th>
                            <th>Accountant Status</th>
                            <th>Status</th>
                            <th>Products</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>



@endsection

@section('script')

    <script>
        $(function () {
            //Purchase table

            var order_table = $('#visitTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[0, 'desc']],
                ajax: '',
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'order_id', name: 'order_id'  },
                    { data: 'created_at', name: 'created_at'  },
                    { data: 'added_by', name: 'added_by'  },
                    { data: 'accountant_status', name: 'accountant_status'  },
                    { data: 'status', name: 'status'  },
                    { data: 'products', name: 'products'  },
                    { data: 'action', name: 'action'  },
                ]
            });


            $(document).on('click','a[view-details]',function (e) {
                e.preventDefault();
                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET',
                    success:(function (data) {
                        $("#modal").html(data).modal('show');
                    })
                })
            });

            $('#daterange-btn').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    order_table.ajax.url( '?start_date=' + start.format('YYYY-MM-DD') +
                        '&end_date=' + end.format('YYYY-MM-DD') ).load();
                }
            );
            $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
                order_table.ajax.url( '').load();
                $('#daterange-btn span').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
            });
        });

    </script>
@endsection
