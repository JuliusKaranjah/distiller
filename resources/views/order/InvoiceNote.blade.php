
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">

        <address>
            <h2>
                {{($business = getBusiness())->name}}
            </h2>
            <strong>
                PIN:{{$business->tax_number_1}}
            </strong><br>
            <strong>
                Tel: 0722 260928
            </strong>
            <br>
            <strong>
                To: {{($saleRep = $order->addedBy)->fullName()}}
            </strong>
            <br>
            <strong>
                Phone: {{$saleRep->phone}}
            </strong>
            <br>

        </address>
    </div>

    <div class="col-sm-4 invoice-col" style="float: right">
        <h2>Invoice</h2>
        <p>
            Invoice No: {{($invoice = $order->saleRepInvoice)->getPrefix().$invoice->id}}
        </p>
        <p>
            Date: {{$invoice->created_at->toDayDateTimeString()}}
        </p>

    </div>
</div>



<br>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table bg-gray">
                <tr class="bg-green">
                    <th>Code</th>
                    <th>@lang('sale.product')</th>
                    <th>@lang('sale.qty')</th>
                    <th>@lang('sale.unit_price')</th>
                    <th>@lang('sale.subtotal')</th>
                </tr>
                @php
                    $total = 0.00;
                @endphp
                @foreach($order->orderItems->where('status','approved') as $item)
                    <tr>
                        <td>{{ ($product = $item->product)->sku}}</td>
                        <td>
                            {{ $product->name }}
                            @if( $product->type == 'variable')
                                - {{ $product->variations->product_variation->name}}
                                - {{ $product->variations->name}}
                            @endif
                        </td>
                        <td>{{ number_format($item->quantity )}}</td>
                        <td>
                            <span class="display_currency" data-currency_symbol="true">KES {{ number_format($item->price?:$item->product->getProductSellPrice(),2)}}</span>
                        </td>
                        <td>
                            <span class="display_currency" data-currency_symbol="true">KES {{ number_format(($item->price?:$item->product->getProductSellPrice()) * $item->quantity,2) }}</span>
                        </td>
                    </tr>

                @endforeach
                <tfooter>
                    <td colspan="3"></td>
                    <th>Total</th>
                    <th>KES {{number_format($totalAmount = $order->orderItems->where('status','approved')->sum(function ($orderItem){

                            $price = $orderItem->price?:$orderItem->product->getProductSellPrice();

                        return $orderItem->quantity * ($price>0?$price:1);
                    }),2) }}</th>
                </tfooter>
            </table>
        </div>
    </div>
</div>
<br>




<br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>
                        Received By:
                    </th>
                    <th>
                        Date:
                    </th>
                    <th>
                        Signature:
                    </th>
                </tr>
                <tr>
                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>
                </tr>
            </table>
        </div>
    </div>
</div>