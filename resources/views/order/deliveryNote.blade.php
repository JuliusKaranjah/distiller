<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">

        <address>

            <h2>
                {{($business = getBusiness())->name}}
            </h2>
            <strong>
                PIN:{{$business->tax_number_1}}
            </strong><br>
            <strong>
                Tel: 0722 260928
            </strong>
            <br>
            <strong>
                To: {{($saleRep = $order->addedBy)->fullName()}}
            </strong>
            <br>
            <strong>
                {{$order->locationTo->name}}
            </strong>
            <br>
            <strong>
                Phone: {{$saleRep->phone}}
            </strong>
            <br>

        </address>
    </div>

    <div class="col-sm-4 invoice-col">
        <address style="margin-top: 75px">
            <p>
                Vehicle: <br>
                <strong>{{@$order->vehicle->name()}}</strong>
            </p>
            <p>
                Driver:
                <strong>{{@$order->driver->full_name}}</strong> <br>
                Phone: {{@$order->driver->phone}}

            </p><br>


        </address>
    </div>


    <div class="col-sm-4 invoice-col" style="float: right">
        <h2>Delivery Note</h2>
        <p>
            Delivery #: {{($order)->getPrefix().$order->id}}
        </p>
        <p>
            Invoice #: {{($invoice = $order->saleRepInvoice)->getPrefix(). $invoice->id}}
        </p>
        <p>
            Date: {{$invoice->created_at->toDayDateTimeString()}}
        </p>

    </div>
</div>



<br>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive">
            <table class="table bg-gray">
                <tr class="bg-green">
                    <th>Code</th>
                    <th>@lang('sale.product')</th>
                    <th>@lang('sale.qty')</th>
                </tr>
                @php
                    $total = 0.00;
                @endphp
                @foreach($order->orderItems->where('status','approved') as $item)
                    <tr>
                        <td>{{ ($product = $item->product)->sku}}</td>
                        <td>
                            {{ $product->name }}
                            @if( $product->type == 'variable')
                                - {{ $product->variations->product_variation->name}}
                                - {{ $product->variations->name}}
                            @endif
                        </td>
                        <td>{{ number_format($item->quantity )}}</td>

                    </tr>

                @endforeach

                <tfooter>
                    <tr>
                        <td colspan="2"></td>
                        <td>{{($order->orderItems->where('status','approved')->sum('quantity'))}} items</td>
                    </tr>
                </tfooter>
            </table>
        </div>
    </div>
</div>
<br>




<br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>

                    </th>
                    <th>
                        Name:
                    </th>
                    <th>
                        Signature:
                    </th>
                    <th>
                        Date:
                    </th>
                </tr>
                <tr>
                    <th>
                        Authorised By
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>
                </tr>
                <tr>
                    <th>
                        Issued By
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>
                </tr>
                <tr>
                    <th>
                        Confirmed By:
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>
                </tr>
                <tr>
                    <th>
                        Received By:
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>

                    <th>
                        _____________________________
                    </th>
                </tr>

            </table>
        </div>
    </div>
</div>