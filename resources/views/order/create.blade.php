@extends('layouts.app')
@section('title', __('purchase.add_purchase'))

@section('content')

    <!-- Content Header (Page header) -->

    <input type="hidden" id="p_code" value="KES">
    <input type="hidden" id="p_symbol" value="KSh">
    <input type="hidden" id="p_thousand" value=",">
    <input type="hidden" id="p_decimal" value=".">

    <!-- Main content -->
    <section class="content">



        @include('layouts.partials.error')

        {!! Form::open(['url' => action('OrderController@store'), 'method' => 'post', 'id' => 'add_purchase_form', 'files' => true ]) !!}

        <div class="box box-solid {{$permittedLocations->count()>1?'':'hide'}}">
            <div class="box-body">
                <div class="row">


                    <div class="col-sm-8 col-sm-offset-2">
                        <input type="hidden" name="origin_location_id" value="{{getBusinessOrderOriginLocationId()}}" id="origin_location_id">
                        <div class="form-group">
                            <label for="">Destination Location</label>
                            <div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-search"></i>
							</span>

                                    @if($permittedLocations->count()>1)
                                        <select name="destination_location_id" id="" class="form-control select2">
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                    @endforeach
                                        </select>
                                        @elseif($permittedLocations->count()==1)
                                    <select name="destination_location_id" class="form-control select2" id="" readonly="">
                                        <option value="{{$permittedLocations->first()->id}}">{{$permittedLocations->first()->name}}</option>
                                    </select>
                                @endif

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="box box-solid"><!--box start-->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-group">
                            <div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-search"></i>
							</span>
                                {!! Form::text('search_product', null, ['class' => 'form-control mousetrap', 'id' => 'search_product_from_order', 'placeholder' => __('lang_v1.search_product_placeholder'), 'autofocus']); !!}
                            </div>
                        </div>
                    </div>

                </div>
                @php
                    $hide_tax = '';
                    if( session()->get('business.enable_inline_tax') == 0){
                        $hide_tax = 'hide';
                    }
                @endphp
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-th-green text-center table-striped" id="purchase_entry_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang( 'product.product_name' )</th>
                                    <th>@lang( 'purchase.purchase_quantity' )</th>
                                    <th>UOM</th>
                                    <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <hr/>
                        <div class="pull-right col-md-5">
                            <table class="pull-right col-md-12">
                                <tr class="hide">
                                    <th class="col-md-7 text-right">@lang( 'purchase.total_before_tax' ):</th>
                                    <td class="col-md-5 text-left">
                                        <span id="total_st_before_tax" class="display_currency"></span>
                                        <input type="hidden" id="st_before_tax_input" value=0>
                                    </td>
                                </tr>
                                <tr class="hide">
                                    <th class="col-md-7 text-right">@lang( 'purchase.net_total_amount' ):</th>
                                    <td class="col-md-5 text-left">
                                        <span id="total_subtotal" class="display_currency"></span>
                                        <!-- This is total before purchase tax-->
                                        <input type="hidden" id="total_subtotal_input" value=0  name="total_before_tax">
                                    </td>
                                </tr>
                            </table>
                            <div class="row pull-left"><button id="submitOrder" class="btn btn-primary">Submit Order</button></div>
                        </div>

                        <input type="hidden" id="row_count" value="0">

                    </div>
                </div>
            </div>
        </div><!--box end-->
        <div class="box box-solid"><!--box start-->
        </div>

        {!! Form::close() !!}
    </section>
    <!-- quick product modal -->
    <div class="modal fade quick_add_product_modal" tabindex="-1" role="dialog" aria-labelledby="modalTitle"></div>
    <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    </div>
    <!-- /.content -->
@endsection

@section('javascript')
    <script src="{{ asset('js/purchase.js') }}"></script>
    <script src="{{ asset('js/product.js?v=' . $asset_v) }}"></script>
    @include('purchase.partials.keyboard_shortcuts')
    <script>
        $(function () {


            //disable the submit button on click to prevent submitting multiple times
            $("button#submitOrder").on('click',function (e) {
                if($(this).hasClass('disabled'))
                {
                    e.preventDefault();
                }

                $(this).addClass('disabled').html('submitting...');
            })

            /*$("table#purchase_entry_table").on('input','input.purchase_quantity',function (e) {

                var closest = $(this).closest('tr');

                var limitInput = closest.find('input.purchase_limit');

                var limit = parseFloat(limitInput.val());

                var newValue = $(this).val();

                if(newValue*1 > limit*1)
                {

                    $(this).css('color','red').css('border-color','red');
                    $("button#submitOrder").addClass('disabled');
                    closest.find('span.error').show().html(('Only '+limit+' available'))

                }else
                    {
                        $(this).css('color','black').css('border-color','black');
                        closest.find('span.error').hide().html('');
                        $("button#submitOrder").removeClass('disabled');
                }
            });

            $("form#add_purchase_form").on('submit',function (e) {
                if($("button#submitOrder").hasClass('disabled'))
                {
                    e.preventDefault();
                }
            })*/

        })
    </script>
@endsection
