@extends('layouts.app')

@section('title','List Orders from sales rep')

@section('content')

    <div class="modal" id="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-content">
            </div>
        </div>
    </div>


    <style>
        .badge-warning
        {
            background-color: #f39c12;
        }
        .badge-default
        {
            background-color: #a1a1a1;
        }
        .badge-success
        {
            background-color: #00a65a;
        }
        .badge-info
        {
            background-color: #01c0ef;
        }
        .badge-danger
        {
            background-color: #d73925;
        }
        .badge-primary{
            background-color: #3c8dbc;
        }
    </style>

    <section class="content-header no-print">
        <h1>Orders
            <small></small>
        </h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Orders</h3>
                @can('order.create')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" href="{{action('OrderController@create')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">
                @can('order.view')
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped ajax_view" id="order_table">
                            <thead>
                            <tr>
                               <th>#</th>
                                <th>Ordered By</th>
                                <th>Accountant Status</th>
                                <th>Status</th>
                                <th>Ordered At</th>
                                <th>Action At</th>
                               <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                @endcan
            </div>
        </div>

        <div class="modal fade product_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

        <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

    </section>


    <!-- /.content -->
@stop
@section('javascript')
    <script src="{{ asset('js/purchase.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
    <script>
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            dateRangeSettings,
            function (start, end) {
                $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                order_table.ajax.url( '/order?start_date=' + start.format('YYYY-MM-DD') +
                    '&end_date=' + end.format('YYYY-MM-DD') ).load();
            }
        );
        $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
            order_table.ajax.url( '/order').load();
            $('#daterange-btn span').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
        });
    </script>


    <script>
        //Purchase table

        var order_table = $('#order_table').DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[4, 'desc']],
            ajax: '/order',
            columnDefs: [ {
                // "targets": [1,2],
                "orderable": false,
                "searchable": false
            } ],
            columns: [
                { data: 'id', name: 'id'  },
                { data: 'ordered_by', name: 'ordered_by'},
                { data: 'accountant_status', name: 'accountant_status'},
                { data: 'status', name: 'status'},
                { data: 'ordered_at', name: 'ordered_at'},
                { data: 'action_at', name: 'action_at'},
                { data: 'action', name: 'action'},
            ]
        });

        $(function () {
            $("table#order_table").on('click','a[view-details]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET'
                }).success(function (data) {
                    $('div#modal-content').html(data);
                    $("div#modal").modal('show');
                })
            });

            $("table#order_table").on('click','a[print-delivery-note]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET'
                }).success(function (data) {

                    $("section#receipt_section").html(data).printThis();

                })
            });

            $("table#order_table").on('click','a[print-invoice-note]',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET'
                }).success(function (data) {

                    $("section#receipt_section").html(data).printThis();

                })
            });
        })
    </script>

@endsection