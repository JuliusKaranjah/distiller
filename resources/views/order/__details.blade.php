{{--input_data = $request->only([ 'location_id', 'ref_no', 'transaction_date', 'additional_notes', 'shipping_charges', 'final_total']);--}}
<?php
$badgeClass = 'default';
if($order->status == 'approved')
{
    $badgeClass = 'success';
}elseif ($order->status == 'partially approved')
{
    $badgeClass = 'primary';
}elseif ($order->status == 'declined')
{
    $badgeClass = 'danger';
}
$accBadgeClass = 'default';
if($order->accountant_status == 'approved')
{
    $accBadgeClass = 'success';
}elseif ($order->accountant_status == 'partially approved')
{
    $accBadgeClass = 'primary';
}elseif ($order->accountant_status == 'declined')
{
    $accBadgeClass = 'danger';
}
?>
<div class="modal-header">
    <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="modalTitle"> Order Details (<b>Order Ref. No:</b> #{{ $order->getId() }})
    </h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-12">

            <p class="pull-right"><b>Date:</b> {{ @($order->created_at->toDayDateTimeString()) }}</p>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
           Sales Rep :
            <address>
                <strong>{{ ($saleRep = $order->addedBy)->fullName() }}</strong>
                Email Address: {{$saleRep->email}}<br>
                <p>
                    <strong>Credit Limit:
                        KES {{number_format($creditLimit = $saleRep->credit_limit,2)}}</strong>
                </p>

                <p>
                    <strong>Allowed Limit:
                        KES {{number_format($allowed = $creditLimit-($totalInvoice = $saleRep->totalInvoiceAmount()),2)}}</strong>
                </p>
                <input type="hidden" id="alloweedAmount" value="{{$allowed}}">

                <p>
                    <strong>Uncleared Amount: <br>
                        KES {{number_format($totalInvoice,2)}}</strong>
                </p>

            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            Business:
            <address>
                <strong>{{($business = getBusiness())->name}}</strong>


                    <br>Athi Business Park


                    <br>Mlolongo,Nairobi,Kenya


                    <br>VAT: P051197116U


            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            <b>Order Ref:</b> #{{ $order->getId()}}<br/>
            <b>Date:</b> {{ $order->created_at->toDayDateTimeString() }}<br/>
            <b>Order Status:</b>
            <span class="badge badge-{{$badgeClass}}">{{ucfirst($order->status)}}</span>
            <br>
            @if($order->status=='declined')
                <b>Reason:</b>
                <br>
                {{$order->note}}
                @endif
            <br><b>Order Accountant Approval:</b>
            <span class="badge badge-{{$accBadgeClass}}">{{ucfirst($order->accountant_status)}}</span>
            <br>
            @if($order->accountant_status=='declined')
                <b>Reason:</b>
                <br>
                {{$order->note}}
                @endif
            <br>
        </div>
    </div>

    <br>
    <div class="row">
        <form action="{{url('order/process-order')}}" id="orderActionForm" method="post" class="hide">
            {!! csrf_field() !!}
            <input type="hidden" name="orderId" value="{{$order->id}}">
            <input type="hidden" name="orderAction" id="orderAction">
        </form>

        <form action="{{url('order/decline-order')}}" method="POST" id="orderDeclineForm" class="hide">
            {!! csrf_field() !!}
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <input type="hidden" name="reason" id="reasonInForm">
        </form>
        <form action="{{url('order/approve-order')}}" method="POST" id="orderApprovalForm">
            {!! csrf_field() !!}
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <input type="hidden" id="action" value="approve">
            <div class="col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <table class="table bg-gray" id="orderItemsTable">
                        <thead>
                        <tr class="bg-green">
                            <th>#</th>
                            <th>Product</th>
                            <th>Ordered</th>
                            <th>Available</th>

                            <th>Status</th>
                            <th>Unit Price</th>
                            <th>Sub-total</th>
                            <th class="{{$order->status !='pending'?'hide':''}}">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total=0?>
                        @foreach($order->orderItems as $orderItem)
                            <?php
                            $badgeClass = 'default';
                            if($orderItem->status == 'approved')
                            {
                                $badgeClass = 'success';
                            }elseif ($orderItem->status == 'partially approved')
                            {
                                $badgeClass = 'primary';
                            }elseif ($orderItem->status == 'declined')
                            {
                                $badgeClass = 'danger';
                            }
                            ?>
                            <tr>

                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ ($product = $orderItem->product)->name }}
                                    @php

                                    $prodDetails = (new \App\Utils\ProductUtil())->getDetailsFromVariation($product->id,getBusiness()['id'],getBusinessOrderOriginLocationId(),true);

                                    $qty = floatval(@$prodDetails->qty_available);


                                    @endphp
                                    @if( $product->type == 'variable')
                                        - {{ $product->variations->product_variation->name}}
                                        - {{ $product->variations->name}}
                                    @endif
                                </td>
                                <td>

                                    @if($showAvailabe = ($orderItem->status !='approved') && auth()->user()->can('order.order_process'))
                                        <input type="hidden" class="available" value="{{$qty}}">
                                        <input type="hidden" name="orderItemId[]" value="{{$orderItem->id}}">
                                        <input type="hidden" class="orderItemDefaultAmount" value="{{$orderItem->quantity}}">
                                        <input type="number" name="orderItemAmount[]" class="form-control order_item_amount"  min="0" step="any">
                                        <span class="error hide available">{{'Out of stock'}}</span>
                                        @else
                                        {{$orderItem->quantity}}
                                        @endif
                                </td>
                                <td>
                                    @if($showAvailabe)
                                    {{$qty}}
                                        @else
                                        -
                                        @endif
                                </td>

                                <td><span class="badge badge-{{$badgeClass}}">{{ucfirst($orderItem->status)}}</span></td>
                                <td><span>{{number_format($price = $orderItem->price ?: (is_null($orderItem->product)?0:$orderItem->product->getProductSellPrice()),2)}}</span>
                                    @if(($orderItem->status =='pending' || $orderItem->status == 'declined'))
                                        <input class="unit_price" name="unit_price[]" type="number" step="any">
                                        <input class="unit_price_origin" type="hidden" step="any" value="{{$price}}">
                                        @endif
                                </td>
                                <td class="subTotal">{{number_format($subTotal = ($price==0?0:$price*$orderItem->quantity),2)}}</td>
                                <?php $total+=$subTotal;?>
                                <td class="{{$order->status !='pending'?'hide':''}}">
                                    @if($orderItem->status =='pending' || $orderItem->status == 'declined')
                                    <input type="hidden" class="sub_total" value="{{$subTotal}}">
                                    @endif
                                    @can('order.order_process')
                                    <button class="btn btn-xs btn-danger btn_remove_item"><i class="fa fa-trash"></i></button>
                                        @endcan
                                </td>
                            </tr>

                        @endforeach
                        <tfooter>
                            <td colspan="5"></td>
                            <td>Total</td>
                            <td><strong><span id="totalAmount">{{number_format($total,2)}}</span></strong></td>
                            <td></td>
                        </tfooter>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <br>

    <div id="actions">

        @if($order->accountant_status != 'approved')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <div class="text-center">
                            <div class="form-group">
                                <strong>{{$order->accountant_status=='declined'?'Order has been declined by accountant':'Order pending accountant approval'}}</>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif


        @if(auth()->user()->can('order.order_process') && $order->accountant_status == 'approved')


            @if($order->status == 'pending')
                <div class="row">

                    <div class="row text-center hide" id="reasonDiv">
                        <div class="col-md-6 col-lg-offset-3">
                            <div class="form-group">

                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Reason for declining" id="reason">
                                    <span class="input-group-btn">
                            <button class="btn btn-danger" type="button" id="decline">Submit</button>
                    </span>
                                </div>


                            </div>
                        </div>
                    </div>


                    <div class="text-center">
                        <div class="form-group">
                            <button class="btn btn-primary acceptOrder">Accept</button>
                            <button class="btn btn-danger declineOrder">Decline</button>


                        </div>
                    </div>

                </div>
            @elseif($order->status == 'declined')
                <div class="row">


                    <div class="text-center">
                        <div class="form-group">
                            <button class="btn btn-primary approvedDeclinedOrder">Approve Declined Order</button>
                            <button class="btn btn-danger DeleteOrder">Delete Order</button>


                        </div>
                    </div>

                </div>

            @elseif($order->status == 'partially approved')
                <div class="row">

                    <div class="text-center">
                        <div class="form-group">
                            <button class="btn btn-primary approvedDeclinedOrderItem">Approve Declined Order Items</button>
                            <button class="btn btn-danger DeleteDeclinedOrderItem">Delete Declined Order Items</button>


                        </div>
                    </div>

                </div>
            @endif

        @endif
    </div>



</div>
<script>
    $(function () {

        var orderItemTable = $("table#orderItemsTable");

        $(".acceptOrder").on('click',function () {

            $("form#orderApprovalForm").append(''/*orderItemTable.html()*/).submit();
        });

        $("input.order_item_amount").each(function (index) {

           var theVal = $(this).closest('tr').find('input.orderItemDefaultAmount').val();

           $(this).val(theVal);
        });

        $("input.unit_price").each(function (index) {

           var theVal = $(this).closest('td').find('input.unit_price_origin').val();

           $(this).val(theVal);
        });

        var moneyFormat = function (currency,amount,hideCurrency=false) {

            if(hideCurrency)
            {
                currency = '';
            }

            return (amount<0 ? "-"+currency+" " : ''+currency+' ') + parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
        };

        var updateOrderTotalAmount = function()
        {
            var amount = 0;
            var hideActions = false;
            @if(isset($showAvailabe)?$showAvailabe:false)
            $("input.sub_total").each(function (index) {
                thisAmount = $(this).closest('tr').find('input.order_item_amount').val();

                amount += parseFloat($(this).val());
                $(this).closest('tr').find('span.available').removeClass('hide');
                if(thisAmount > (available = parseFloat($(this).closest('tr').find('input.available').val())))
                {
                    $(this).closest('tr').find('span.available').removeClass('hide').html('Out of stock '+available+' available');

                    hideActions = true;

                }else {
                    $(this).closest('tr').find('span.available').addClass('hide');
                }
            });
            @else
            $("td.subTotal").each(function (index) {

                amount += parseFloat($(this).html().replace(',',''));
            });
            @endif

            $("#totalAmount").html( moneyFormat('KES',amount));



            if(/*parseFloat(amount) > parseFloat($("#alloweedAmount").val())*/ hideActions)
            {
                $("div#actions").slideUp('slow');
            }else {
                $("div#actions").slideDown('slow');
            }
        };

        updateOrderTotalAmount();

        $("button.btn_remove_item").on('click',function () {
            $(this).closest('tr').remove();

            updateOrderTotalAmount();

        });

        $("input.unit_price").on('input',function () {
            var unitPrice = $(this).val();
            var subTotal = 0;
            if(parseFloat(unitPrice)>0)
            {
                subTotal = unitPrice * parseFloat($(this).closest('tr').find('input.order_item_amount').val());
            }

            $(this).closest('tr').find('td.subTotal').html(moneyFormat('KES',subTotal,true));
            $(this).closest('tr').find('input.sub_total').val(subTotal);

            updateOrderTotalAmount();
        });


        $("input.order_item_amount").on('input',function () {


            var quantity = $(this).val();
            var subTotal = 0;
            if(parseFloat(quantity)>0)
            {
                subTotal = quantity * parseFloat($(this).closest('tr').find('input.unit_price').val());
            }

            $(this).closest('tr').find('td.subTotal').html(moneyFormat('KES',subTotal,true));
            $(this).closest('tr').find('input.sub_total').val(subTotal);

            updateOrderTotalAmount();
        });

        $(".declineOrder").on('click',function () {

            $("#reasonDiv").removeClass('hide');
            $("input#reason").focus();
        });
        $("button#decline").on('click',function () {
            $("input#reasonInForm").val($('input#reason').val());

            $('form#orderDeclineForm').submit();
        });

        $("button.approvedDeclinedOrder").on('click',function () {

            $("input#orderAction").val('approvedDeclinedOrder');

            // $('form#orderActionForm').hide('fast');

            $("table#orderItemsTable").appendTo("form#orderActionForm");

            $('form#orderActionForm').submit();
        });

        $("button.DeleteOrder").on('click',function () {

            $("input#orderAction").val('deleteOrder');

            $('form#orderActionForm').submit();
        });
        $("button.approvedDeclinedOrderItem").on('click',function () {

            $("input#orderAction").val('approvedDeclinedOrderItem');

            $('form#orderActionForm').hide('fast');

            $("table#orderItemsTable").appendTo("form#orderActionForm");

            $('form#orderActionForm').submit();
        });

        $("button.DeleteDeclinedOrderItem").on('click',function () {

            $("input#orderAction").val('DeleteDeclinedOrderItem');

            $('form#orderActionForm').submit();
        });
    })
</script>