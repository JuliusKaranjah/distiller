@extends('layouts.app')
@section('title', 'Bottle Setting')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Bottle Settings</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

        <form action="{{action('BottleSettingController@store')}}" id="bottleSettingStoreProduct" method="POST">
            {!! csrf_field() !!}

        </form>

        {!! Form::open(['url' => action('BottleSettingController@index'), 'method' => 'post', 'id' => 'bussiness_edit_form',
                   'files' => true ]) !!}

        <div class="row">

            <div class="col-sm-12">
                <div class="box box-solid"> <!--Address info box start-->
                    <div class="box-header">
                        <h3 class="box-title">Bottle Setting</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="clearfix"></div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Mara Bottle Charge</label>
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-info"></i>
                                    </span>
                                        <input type="number" class="form-control" step="1" name="bottle_charge" value="{{isset($bottleCharge)?$bottleCharge:'20'}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="col-md-6">
                                    <div class="form-group" style="margin-top: 25px">
                                        <button class="btn btn-primary pull-right" type="submit">Update Price</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="box box-solid"> <!--Address info box start-->
                    <div class="box-header">
                        <h3 class="box-title">Items Requiring Mara Bottle</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="clearfix"></div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Select Product</label>
                                    <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-info"></i>
                                    </span>
                                        <select class="form-control select2" id="product_id">
                                            @foreach($products as $product)
                                                <option value="{{$product->id}}">{{$product->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. of bottle</label>
                                        <input type="number"  class="form-control" step="0.01" id="amount">
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="col-md-6">
                                    <div class="form-group" style="margin-top: 25px">
                                        <button class="btn btn-primary pull-right" type="button" id="addProduct">Add Product</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped ajax_view" id="productRequiringBottle">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Product</th>
                                            <th>No. of Bottle</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>



        {!! Form::close() !!}
    </section>
    <!-- /.content -->

@endsection

@section('script')
    <script>
        $(function () {
             var table = $('#productRequiringBottle').DataTable({
                         processing: true,
                         serverSide: true,
                         aaSorting: [[0, 'desc']],
                         ajax: '',
                         columnDefs: [ {
                             // "targets": [1,2],
                             "orderable": false,
                             "searchable": false
                         } ],
                         columns: [
                             { data: 'id', name: 'id'  },
                             { data: 'product', name: 'product'},
                             { data: 'quantity', name: 'quantity'},
                             { data: 'action', name: 'action'},

                         ]});

             $("button#addProduct").on('click',function () {
                 $('form#bottleSettingStoreProduct').append('<input name="product_id" value="'+$("#product_id").val()+'">')
                     .append('<input name="product_id" value="'+$("#product_id").val()+'">')
                     .append('<input name="amount" value="'+$("#amount").val()+'">').submit();
             })
        })
    </script>
    @stop