@extends('layouts.app')
@section('title','Message Report')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Message Report</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" id="accordion">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
                                <i class="fa fa-filter" aria-hidden="true"></i> @lang('report.filters')
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFilter" class="panel-collapse active collapse in" aria-expanded="true">
                        <div class="box-body">
                            {!! Form::open(['url' => action('ReportController@getStockReport'), 'method' => 'get', 'id' => 'stock_report_filter_form' ]) !!}
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Sales Rep.' . ':') !!}<br>
                                        <select id="saleRepId" name="sale_rep_id" class="select2 form-control">
                                            <option value="all">All Sale Rep</option>
                                            @foreach($sales as $sale)
                                                <option value="{{$sale->id}}">{{$sale->fullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="logistics_report">
                                <thead>
                                <tr>
                                    <th>Message ID</th>
                                    <th>Date</th>
                                    <th>Sender</th>
                                    <th>Recipient</th>
                                    <th>Content</th>

                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('javascript')
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            var logistics_report = $('table#logistics_report').DataTable({
                processing: true,
                serverSide: true,
                // aaSorting: [[3, 'desc']],

                "ajax": {
                    "url": "",
                    "data": function (d) {
                        d.saleRepId = $('#saleRepId').val();
                        d.customer_id = $('#customer_id').val();
                        d.reason = $('#reason').val();
                    }
                },
                columns: [
                    {data: 'message_id', name: 'message_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'sender', name: 'sender'},
                    {data: 'recipient', name: 'recipient'},
                    {data: 'body', name: 'body'},
                    // {data: 'customer', name: 'customer'},
                    // {data: 'reason', name: 'reason'},
                    // {data: 'conclusion', name: 'conclusion'},
                    // {data: 'description', name: 'description'},

                ],

                "fnDrawCallback": function (oSettings) {
                    __currency_convert_recursively($('#lot_report'));
                    __show_date_diff_for_human($('#lot_report'));
                }
            });


            if ($('table#logistics_report').length == 1) {
                $('#saleRepId, #customer_id, #reason').change(function () {
                    logistics_report.ajax.reload();
                });
            }

        })
    </script>
@endsection