@extends('layouts.app')
@section('title', __('lang_v1.mpesa_payment_report'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ __('lang_v1.mpesa_payment_report')}}</h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped"
                                   id="mpesa_payment_report_table">
                                <thead>
                                <tr>
                                    <th>@lang('purchase.ref_no')</th>
                                    <th>@lang('purchase.trans_code')</th>
                                    <th>@lang('purchase.mpesa_number')</th>
                                    <th>@lang('purchase.phone_number')</th>
                                    <th>@lang('contact.customer')</th>
                                    <th>@lang('lang_v1.paid_on')</th>
                                    <th>@lang('sale.amount')</th>
                                    <th>@lang('lang_v1.payment_method')</th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($mpesaTrans  as $mpesaTran)

                                    <tr>
                                        <td>{{$mpesaTran->id}}</td>
                                        <td>{{$mpesaTran->code}}</td>
                                        <td>{{($mpesaTran->mpesa_number)?:$mpesaTran->transaction->contact->mobile}}</td>
                                        <td>{{($mpesaTran->transaction->contact->mobile)}}</td>
                                        <td>{{$mpesaTran->transaction->contact->name}}</td>
                                        <td>{{$mpesaTran->created_at}}</td>
                                        <td>{{number_format($mpesaTran->amount,2)}}</td>
                                        <td>Mpesa</td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="modal fade view_register" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

@endsection

@section('javascript')
    <script>
        $(function () {
            $("table#mpesa_payment_report_table").dataTable({
                "order": [[ 0, "desc" ]]
            });
        })
    </script>
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
@endsection