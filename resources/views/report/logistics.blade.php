@extends('layouts.app')
@section('title','Logistics Report')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Logistics Report</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" id="accordion">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
                                <i class="fa fa-filter" aria-hidden="true"></i> @lang('report.filters')
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFilter" class="panel-collapse active collapse in" aria-expanded="true">
                        <div class="box-body">
                            {!! Form::open(['url' => action('ReportController@getStockReport'), 'method' => 'get', 'id' => 'stock_report_filter_form' ]) !!}
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Sales Rep.' . ':') !!}<br>
                                        <select id="saleRepId" name="sale_rep_id" class="select2 form-control">
                                            <option value="all_sale_rep">All Sale Rep</option>
                                            @foreach($sales as $sale)
                                                <option value="{{$sale->id}}">{{$sale->fullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Account status' . ':') !!}<br>
                                        <select id="account_status" name="account_status" class="select2 form-control">
                                            <option value="all">All</option>
                                            @foreach(['pending','approved','partially approved','declined'] as $status)
                                                <option value="{{$status}}">{{ucwords(strtolower($status))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Service Rep. Status' . ':') !!}<br>
                                        <select id="service_status" name="service_status" class="select2 form-control">
                                            <option value="all">All</option>
                                            @foreach(['pending','approved','partially approved','declined'] as $status)
                                                <option value="{{$status}}">{{ucwords(strtolower($status))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Logistics Status' . ':') !!}<br>
                                        <select id="logistic_status" name="logistic_status" class="select2 form-control">
                                            <option value="all">All</option>
                                            @foreach(['pending','loaded','dispatched','received'] as $status)
                                                <option value="{{$status}}">{{ucwords(strtolower($status))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('location_id',  'Sale Rep. Status' . ':') !!}<br>
                                        <select id="sale_status" name="sale_status" class="select2 form-control">
                                            <option value="all">All Sale Rep</option>
                                            <option value="pending">Pending</option>
                                            <option value="received">Received</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="logistics_report">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Date</th>
                                    <th>Sale Rep.</th>
                                    <th>Products</th>
                                    <th>Account Status</th>
                                    <th>Service Rep. Status</th>
                                    <th>Logistic Status</th>
                                    <th>Sale Rep. Status</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

@endsection

@section('javascript')
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            var logistics_report = $('table#logistics_report').DataTable({
                processing: true,
                serverSide: true,
                // aaSorting: [[3, 'desc']],

                "ajax": {
                    "url": "",
                    "data": function (d) {
                        d.sale_rep_id = $('#saleRepId').val();
                        d.account_status = $('#account_status').val();
                        d.service_status = $('#service_status').val();
                        d.logistic_status = $('#logistic_status').val();
                        d.sale_status = $('#sale_status').val();
                    }
                },
                columns: [
                    {data: 'order_id', name: 'order_id'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'sale_rep', name: 'sale_rep'},
                    {data: 'products', name: 'products'},
                    {data: 'accountant_status', name: 'accountant_status'},
                    {data: 'service_rep_status', name: 'service_rep_status'},
                    {data: 'logistic_status', name: 'logistic_status'},
                    {data: 'sale_status', name: 'sale_status'},
                ],

                "fnDrawCallback": function (oSettings) {
                    __currency_convert_recursively($('#lot_report'));
                    __show_date_diff_for_human($('#lot_report'));
                }
            });


            if ($('table#logistics_report').length == 1) {
                $('#saleRepId, #account_status, #service_status, #logistic_status, #sale_status').change(function () {
                    logistics_report.ajax.reload();
                });
            }

        })
    </script>
@endsection