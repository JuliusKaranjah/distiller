@extends('layouts.app')
@section('title', __('report.expense_report'))

@section('css')
    {{--{!! Charts::styles(['highcharts']) !!}--}}
@endsection

@section('content')
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>{{ __('report.expense_report')}}</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary" id="accordion">
              <div class="box-header with-border">
                <h3 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
                    <i class="fa fa-filter" aria-hidden="true"></i> @lang('report.filters')
                  </a>
                </h3>
              </div>
              <div id="collapseFilter" class="panel-collapse active collapse in" aria-expanded="true">
                <div class="box-body">
                  {!! Form::open(['url' => action('ReportController@getExpenseReport'), 'method' => 'get' ]) !!}
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('location_id',  __('purchase.business_location') . ':') !!}
                            {!! Form::select('location_id', $business_locations, null, ['class' => 'form-control select2', 'style' => 'width:100%']); !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('category_id','Category:') !!}
                            {!! Form::select('category', $categories, null, ['placeholder' =>
                            __('report.all'), 'class' => 'form-control select2', 'style' => 'width:100%', 'id' => 'category_id']); !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('trending_product_date_range', __('report.date_range') . ':') !!}
                            {!! Form::text('date_range', @format_date('first day of this month') . ' ~ ' . @format_date('last day of this month') , ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'id' => 'trending_product_date_range', 'readonly']); !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-primary pull-right">@lang('report.apply_filters')</button>
                    </div> 
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    {!! $chart->render() !!}
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection

@section('javascript')

    <script src="{{asset('AdminLTE/plugins/DataTables/datatables.js')}}"></script>
    <script src="{{asset('plugins/chart/highchart/highcharts.js')}}"></script>
    <script src="{{asset('plugins/chart/highchart/map.js')}}"></script>
    <script src="{{asset('plugins/chart/highchart/offline-exporting.js')}}"></script>
    <script src="{{asset('plugins/chart/highchart/world.js')}}"></script>
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
    {{--{!! Charts::assets(['highcharts']) !!}--}}
    <script src="{{asset('js/chart.bundle.js')}}"></script>
@endsection