
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="modal-content"><div class="box box-solid"><!--box start-->
                <form action="{{action('RegionalProductController@store')}}" method="post" class="box-body">
                    {!! csrf_field() !!}
                    <div class="col-md-12 col-lg-12" style="font-weight: bold">

                        Add Regional Product
                        <hr>
                    </div>


                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Select Product</label>

                                    <select class="form-control select2" name="product_id">
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                            @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Select Region</label>

                                    <select class="form-control select2" name="region_id">
                                        @foreach($businessLocations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12">

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="form-group">

                                    <label>Price</label>

                                    <input type="number" step="any" required name="price" class="form-control">

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <button class="btn btn-primary" value="load">Add Regional Product</button>
                        </div>
                    </div>
                </form>
            </div><!--box end--></div>
    </div>
