@extends('layouts.app')
@section('title', __('home.home'))

{{--@section('css')
    {!! Charts::assets(['global','highcharts']) !!}
@endsection--}}



@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>{{ __('home.welcome_message', ['name' => Session::get('user.first_name')]) }}
    </h1>
</section>
@if(auth()->user()->can('dashboard.data'))
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="btn-group pull-right" data-toggle="buttons">
				<label class="btn btn-info active" label="today">
    				<input type="radio" name="date-filter"
    				data-start="{{ date('Y-m-d') }}" 
    				data-end="{{ date('Y-m-d') }}"
    				checked> {{ __('home.today') }}
  				</label>
  				<label class="btn btn-info"  label="week">
    				<input type="radio" name="date-filter"
    				data-start="{{ $date_filters['this_week']['start']}}" 
    				data-end="{{ $date_filters['this_week']['end']}}"
    				> {{ __('home.this_week') }}
  				</label>
  				<label class="btn btn-info"  label="month">
    				<input type="radio" name="date-filter"
    				data-start="{{ $date_filters['this_month']['start']}}" 
    				data-end="{{ $date_filters['this_month']['end']}}"
    				> {{ __('home.this_month') }}
  				</label>
  				<label class="btn btn-info" label="year">
    				<input type="radio" name="date-filter" 
    				data-start="{{ $date_filters['this_fy']['start']}}" 
    				data-end="{{ $date_filters['this_fy']['end']}}" 
    				> {{ __('home.this_fy') }}
  				</label>
            </div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-3 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Total Sales</span>
					<span class="info-box-number total_sell"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>

    	<div class="col-sm-3 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">Cash At Hand</span>
	          <span class="info-box-number total_cash_at_hand"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>

    	<div class="col-sm-3 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">Invoice Due</span>
	          <span class="info-box-number invoice_due"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
	    <div class="col-sm-3 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

	        <div class="info-box-content">
	          <span class="info-box-text">Total Mpesa</span>
	          <span class="info-box-number total_mpesa"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
	    <div class="col-sm-3 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-yellow">
	        	<i class="fa fa-dollar"></i>
				<i class="fa fa-exclamation"></i>
	        </span>

	        <div class="info-box-content">
	          <span class="info-box-text">Pending Cheque</span>
	          <span class="info-box-number pending_cheque"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->

	    <!-- fix for small devices only -->
	    <!-- <div class="clearfix visible-sm-block"></div> -->
	    <div class="col-sm-3 col-xs-12">
	      <div class="info-box">
	        <span class="info-box-icon bg-yellow">
	        	<i class="ion ion-ios-paper-outline"></i>
	        	<i class="fa fa-exclamation"></i>
	        </span>

	        <div class="info-box-content">
	          <span class="info-box-text">Approved Cheque</span>
	          <span class="info-box-number approved_cheque"><i class="fa fa-refresh fa-spin fa-fw margin-bottom"></i></span>
	        </div>
	        <!-- /.info-box-content -->
	      </div>
	      <!-- /.info-box -->
	    </div>
	    <!-- /.col -->
  	</div>
  	<br>
  	<!-- sales chart start -->
  	<div class="row">


		<div class="col-sm-12">
  			<div class="box box-primary">
  				<div class="box-header">
         			<h3 class="box-title">{{ __('home.sells_last_30_days') }}</h3>
         		</div>
	            <div class="box-body">
					<canvas id="monthSales"></canvas>
	          {{--    {!! $sells_chart_1->render() !!}--}}
	            </div>
	            <!-- /.box-body -->
          	</div>
  		</div>
  	</div>

  	<div class="row">
  		<div class="col-sm-12">
  			<div class="box box-primary">
  				<div class="box-header">
         			<h3 class="box-title">{{ __('home.sells_current_fy') }}</h3>
         		</div>
	            <div class="box-body">
					<canvas id="yearSales"></canvas>
	              {{--{!! $sells_chart_2->render() !!}--}}
	            </div>
	            <!-- /.box-body -->
          	</div>
  		</div>
  	</div>
  	<!-- sales chart end -->

  	<!-- products less than alert quntity -->
  	<div class="row">
  		<div class="col-sm-6">
  			<div class="box box-warning">
  				<div class="box-header">
  					<i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
         			<h3 class="box-title">{{ __('home.product_stock_alert') }} @show_tooltip(__('tooltip.product_stock_alert')) </h3>
         		</div>
	            <div class="box-body">
	              <table class="table table-bordered table-striped" id="stock_alert_table">
            		<thead>
            			<tr>
            				<th>@lang( 'sale.product' )</th>
            				<th>@lang( 'business.location' )</th>
                            <th>@lang( 'report.current_stock' )</th>
            			</tr>
            		</thead>
            	</table>
	            </div>
	            <!-- /.box-body -->
          	</div>
  		</div>
  		<div class="col-sm-6">
  			<div class="box box-warning">
  				<div class="box-header">
  					<i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
         			<h3 class="box-title">{{ __('home.payment_dues') }} @show_tooltip(__('tooltip.payment_dues'))</h3>
         		</div>
	            <div class="box-body">
	              <table class="table table-bordered table-striped" id="payment_dues_table">
            		<thead>
            			<tr>
            				<th>@lang( 'purchase.supplier' )</th>
            				<th>@lang( 'purchase.ref_no' )</th>
                            <th>@lang( 'home.due_amount' )</th>
            			</tr>
            		</thead>
            	</table>
	            </div>
	            <!-- /.box-body -->
          	</div>
  		</div>
      <div class="clearfix"></div>
      @if(session('business.enable_product_expiry') == 1)
        <div class="col-sm-12">
          <div class="box box-warning">
            <div class="box-header">
              <i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
                <h3 class="box-title">{{ __('home.stock_expiry_alert') }} @show_tooltip( __('tooltip.stock_expiry_alert', [ 'days' =>session('business.stock_expiry_alert_days', 30) ]) )</h3>
            </div>
                <div class="box-body">
                  <input type="hidden" id="stock_expiry_alert_days" value="{{ \Carbon::now()->addDays(session('business.stock_expiry_alert_days', 30))->format('Y-m-d') }}">
                  <table class="table table-bordered table-striped" id="stock_expiry_alert_table">
                  <thead>
                    <tr>
                      <th>@lang('business.product')</th>
                      <th>@lang('business.location')</th>
                      <th>@lang('report.stock_left')</th>
                      <th>@lang('product.expires_in')</th>
                    </tr>
                  </thead>
                </table>
                </div>
                <!-- /.box-body -->
          </div>
        </div>
      @endif
  	</div>

</section>
<!-- /.content -->
@endif
@stop

@section('javascript')

	<script src="{{asset('js/chart.bundle.js')}}"></script>
	<script>
		var ctx = $("#monthSales");
		var ctx2 = $("#yearSales");

		var urlLink = "{{url('/sells/load-month-sales')}}"+'?days=30';
		var urlLinkYearly = "{{url('/sells/load-month-sales')}}"+'?days=yearly';

		var loadSales = function (link,ctx) {
			$.ajax({
				url:link,
				success:function (data) {

                    makeGraph(data,ctx);
                },
				error:function (err) {
					console.error(err);
					return 'error';
                }
			})
        };



		var makeGraph = function (salesData,graph)
        {



            new Chart(graph, {
                type: 'bar',
                data: {
                    labels: salesData['salesLabel'],
                    datasets: [{
                        label: 'Sales Total Amount',
                        data: /*[12, 19, 3, 5, 2, 3]*/salesData['sales'],

						backgroundColor: ['#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef','#f29d39','#4ca75b','#01c0ef',]

                        /*backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1*/
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });

        };

		loadSales(urlLink,ctx);
		loadSales(urlLinkYearly,ctx2);

		var showSummarySales = function (frequency) {
			$("span[data-level]").hide();
			$("span[data-level="+frequency+"]").show();
        };

        showSummarySales('today');

        $("[data-toggle=buttons]>label").on('click',function () {

            showSummarySales(($(this).attr('label')));
        });

		$(function () {
			$("label[label=year]").trigger('click');
        })

	</script>
	<script src="{{ asset('js/home.js?v=' . $asset_v) }}"></script>
	{{--{!! Charts::assets(['highcharts']) !!}--}}
	@endsection


@section('javascript')
	<script src="{{ asset('js/home.js?v=' . $asset_v) }}"></script>
	{{--{!! Charts::assets(['highcharts']) !!}--}}
	{{--{!! $sells_chart_1->script() !!}--}}
	{{--{!! $sells_chart_2->script() !!}--}}
@endsection

