@extends('layouts.app')

@section('title','Add Visit')


@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Add Visit</h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => action('VisitController@store'), 'method' => 'post', 'id' => 'visit_add_form' ]) !!}
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Reason For Visit</label>
                            <select class="form-control select2" name="reason">
                                <option>Select Reason</option>
                                @foreach($reasons as $reason)
                                    <option value="{{$reason->id}}">{{$reason->name}}</option>
                                    @endforeach

                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">

                                <label>Select Customer</label>
                                <p class="input-group">
                                    <select class="form-control select2" name="customer_id">
                                        <option>Select Customer</option>
                                        @foreach($customers as $customer)
                                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach

                                    </select>
                                    <span class="input-group-btn">
                                <a href="{{action('VisitController@addCustomer')}}" type="button" class="btn btn-primary" id="addCustomer"><i class="fa fa-plus"></i></a>
                                </span>
                                </p>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Findings</label>
                            <textarea name="finding" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Conclusion</label>
                            <textarea name="conclusion" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Note (optional)</label>
                            <textarea name="description" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary">Add Visit</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

    <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    </div>

    @endsection

@section('script')

    <script>
        $(function () {
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(showLocation);
            }else{
                $('#location').html('Geolocation is not supported by this browser.');
            }


            function showLocation(position) {

                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;

                $.ajax({
                    type: 'POST',
                    url: 'https://ridesafeweb.x20labs.com/admin/location_json/',
                    data: 'latitude=' + latitude + '&longitude=' + longitude,
                    success: function (msg) {
                        if (msg) {

                            if (msg.indexOf('Unnamed') != -1) {
                                $("#location").html('latitude=' + latitude + '&longitude=' + longitude);

                                document.cookie = "fbdata = " + 'lat=' + latitude + '&lon=' + longitude;
                            }
                            else {
                                $("#location").html(msg);
                                document.cookie = "fbdata = " + msg;
                            }


                        } else {
                            $("#location").html('Not Available');
                        }

                    },
                    error:function (err) {
                        console.error(err);
                    }

                });

            };

            $("a#addCustomer").on('click',function (e) {

                e.preventDefault();

               $.ajax({
                   url:$(this).attr('href'),
                   method:'GET',
                   success:function (data) {
                       $("div.contact_modal").html(data).modal('show');
                   }
               })
            });

        })
    </script>

    @endsection