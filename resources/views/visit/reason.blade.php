@extends('layouts.app')

@section('title','Reasons For Visit')


@section('content')
    <section class="content-header no-print">
        <h1>Reasons For Visit
            <small></small>
        </h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Reasons For Visit</h3>
                @can('visit.reason.create')
                    <div class="box-tools">
                        <a id="addVisitReason" class="btn btn-block btn-primary" href="{{action('VisitController@addReason')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">


                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view" id="visitTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Reason</th>
                            <th>Note</th>
                            <th>Added At</th>
                            <th>Added By</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal addVisitModalForm" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="modal-content">
            </div>
        </div>
    </div>

@endsection

@section('script')


    <script>
        $(function () {

            //Purchase table

            var order_table = $('#visitTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[0, 'desc']],
                ajax: '',
                columnDefs: [{
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                }],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'added_by', name: 'added_by'},
                    {data: 'action', name: 'action'},
                ]
            });

            $(document).on('click','a#addVisitReason',function (e) {
                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET',
                    success:function (data) {

                        $("#modal-content").html(data);

                        $("div.addVisitModalForm").modal('show');
                    }
                })

            });

        });
    </script>
@endsection