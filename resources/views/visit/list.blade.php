@extends('layouts.app')

@section('title','Add Visit')


@section('content')
    <section class="content-header no-print">
        <h1>Visits
            <small></small>
        </h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content no-print">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">All Visits</h3>
                @can('visit.create')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" href="{{action('VisitController@create')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan
            </div>
            <div class="box-body">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="button" class="btn btn-primary" id="daterange-btn">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                    <i class="fa fa-caret-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped ajax_view" id="visitTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sale Rep.</th>
                            <th>Customer</th>
                            <th>Reason</th>
                            <th>Finding</th>
                            <th>Conclusion</th>
                            <th>Note</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            dateRangeSettings,
            function (start, end) {
                $('#daterange-btn span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                order_table.ajax.url( '?start_date=' + start.format('YYYY-MM-DD') +
                    '&end_date=' + end.format('YYYY-MM-DD') ).load();
            }
        );
        $('#daterange-btn').on('cancel.daterangepicker', function(ev, picker) {
            order_table.ajax.url( '').load();
            $('#daterange-btn span').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
        });
    </script>


    <script>
        //Purchase table

        var order_table = $('#visitTable').DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[0, 'desc']],
            ajax: '',
            columnDefs: [ {
                // "targets": [1,2],
                "orderable": false,
                "searchable": false
            } ],
            columns: [
                { data: 'id', name: 'id'  },
                { data: 'sale', name: 'sale'},
                { data: 'customer', name: 'customer'},
                { data: 'reason', name: 'reason'},
                { data: 'findings', name: 'findings'},
                { data: 'conclusion', name: 'conclusion'},
                { data: 'description', name: 'description'},
            ]
        });
    </script>
    @endsection