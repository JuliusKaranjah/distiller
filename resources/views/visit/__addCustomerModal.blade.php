    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{url('contacts')}}" accept-charset="UTF-8" id="quick_add_contact"><input name="_token" type="hidden" value="{{csrf_token()}}">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add a new contact</h4>
                </div>

                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12 contact_type_div">
                            <div class="form-group">
                                <label for="type">Contact type:*</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                                    <select class="form-control" id="contact_type" required name="type"><option selected="selected" value="">Please Select</option><option value="supplier">Suppliers</option><option value="customer">Customers</option><option value="both">Both (Supplier &amp; Customer)</option></select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 supplier_fields">
                            <div class="form-group">
                                <label for="supplier_business_name">Business Name:*</label>
                                <div class="input-group">
                  <span class="input-group-addon">
                      <i class="fa fa-briefcase"></i>
                  </span>
                                    <input class="form-control" required placeholder="Business Name" name="supplier_business_name" type="text" id="supplier_business_name">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Name:*</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                                    <input class="form-control" placeholder="Name" required name="name" type="text" id="name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="contact_id">Contact ID:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-id-badge"></i>
                </span>
                                    <input class="form-control" placeholder="Contact ID" name="contact_id" type="text" id="contact_id">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-envelope"></i>
                </span>
                                    <input class="form-control" placeholder="Email" name="email" type="email" id="email">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tax_number">Tax number:</label>
                                <div class="input-group">
                  <span class="input-group-addon">
                      <i class="fa fa-info"></i>
                  </span>
                                    <input class="form-control" placeholder="Tax number" name="tax_number" type="text" id="tax_number">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 supplier_fields">
                            <div class="form-group">
                                <label for="pay_term_number">Pay term*:</label>               <div class="input-group">
                  <span class="input-group-addon">
                      <i class="fa fa-handshake-o"></i>
                  </span>
                                    <input class="form-control" required placeholder="Pay term" name="pay_term_number" type="number" id="pay_term_number">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 supplier_fields">
                            <div class="form-group">
                                <label for="pay_term_type">Pay term period*:</label>
                                <div class="input-group">
                  <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                  </span>
                                    <select class="form-control" required id="pay_term_type" name="pay_term_type"><option selected="selected" value="">Please Select</option><option value="months">Months</option><option value="days">Days</option></select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 customer_fields">
                            <div class="form-group">
                                <label for="customer_group_id">Customer Group:</label>
                                <div class="input-group">
                  <span class="input-group-addon">
                      <i class="fa fa-users"></i>
                  </span>
                                    <select class="form-control" id="customer_group_id" name="customer_group_id"><option value="" selected="selected">None</option></select>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <hr/>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="mobile">Mobile:*</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-mobile"></i>
                </span>
                                    <input class="form-control" required placeholder="Mobile" name="mobile" type="text" id="mobile">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="landline">Landline:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-phone"></i>
                </span>
                                    <input class="form-control" placeholder="Landline" name="landline" type="text" id="landline">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="alternate_number">Alternate contact number:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-phone"></i>
                </span>
                                    <input class="form-control" placeholder="Alternate contact number" name="alternate_number" type="text" id="alternate_number">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="city">City:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-map-marker"></i>
                </span>
                                    <input class="form-control" placeholder="City" name="city" type="text" id="city">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="state">State:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-map-marker"></i>
                </span>
                                    <input class="form-control" placeholder="State" name="state" type="text" id="state">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="country">Country:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-globe"></i>
                </span>
                                    <input class="form-control" placeholder="Country" name="country" type="text" id="country">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="landmark">Landmark:</label>
                                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-map-marker"></i>
                </span>
                                    <input class="form-control" placeholder="Landmark" name="landmark" type="text" id="landmark">
                                </div>
                            </div>
                        </div>
                        <div class=" hide ">
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <hr/>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="custom_field1">Custom Field 1:</label>
                                    <input class="form-control" placeholder="Custom Field 1" name="custom_field1" type="text" id="custom_field1">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="custom_field2">Custom Field 2:</label>
                                    <input class="form-control" placeholder="Custom Field 2" name="custom_field2" type="text" id="custom_field2">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="custom_field3">Custom Field 3:</label>
                                    <input class="form-control" placeholder="Custom Field 3" name="custom_field3" type="text" id="custom_field3">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="custom_field4">Custom Field 4:</label>
                                    <input class="form-control" placeholder="Custom Field 4" name="custom_field4" type="text" id="custom_field4">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" id="posAddCustomer" onclick="$.ajax({method:'POST',data:$('form#quick_add_contact').serialize(),url:'{{url('contacts')}}',success:function() {
                            toastr.success('Successfully added', 'Success!');
                        location.reload(true);
                            }})" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </form>

        </div>
    </div>