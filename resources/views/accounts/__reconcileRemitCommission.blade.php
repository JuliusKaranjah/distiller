<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>Remitted Commission
    </h1>
</section>

<!-- Main content -->
<section class="content no-print">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Remitted Commission</h3>
            <div class="box-tools">
                <a class="btn btn-block btn-primary" id="remitCommission" href="{{action('AccountsController@remitCommission',['id' => $saleRep->id])}}">
                    <i class="fa fa-plus"></i> Remit Commission</a>
            </div>

        </div>
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-bordered table-striped ajax_view" id="sell_table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Sales Rep.</th>
                        <th>Remitted By</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($remittedCommissions as $remittedCommission)

                        <tr>
                            <td>{{'REM00'.$remittedCommission->id}}</td>
                            <td>{{$remittedCommission->created_at}}</td>
                            <td>KES {{number_format($remittedCommission->amount,2)}}</td>
                            <td>{{(@$remittedCommission->saleRep->fullName())}}</td>
                            <td>{{(@$remittedCommission->addedBy->fullName())}}</td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade payment_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade register_details_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<!-- This will be printed -->
<!-- <section class="invoice print_section" id="receipt_section">
</section> -->
<div class="modal remitCommission" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel"><div class="modal-dialog  no-print" role="document" id="myModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="modalTitle"> Remit Commission
                </h4>
                <span class="pull-right"><strong>{{$saleRep->fullName()}}</strong></span>
            </div>
            <div class="modal-body">

                <form action="{{action('AccountsController@remitCommission')}}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="sale_rep_id" value="{{$saleRep->id}}">

                    <div class="form-group">
                        <label for="pwd">Amount:</label>
                        <input type="number" step="any" class="form-control" id="remit_amount" name="remit_amount" max="{{$saleRep->totalCompanySaleRepInvoice()}}">
                        <span class="error remitAmount"></span>
                    </div>
                    <div class="form-group">
                        <label for="description">Note: (optional)</label>
                        <textarea name="description" id="description" cols="30" rows="3" class="form-control"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary postRemitCommission">Remit Commission</button>
                </form>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default no-print" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>