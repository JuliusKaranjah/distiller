<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>Bottle Invoice</h1>
</section>

<!-- Main content -->
<section class="content no-print">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Bottle Invoice</h3>

        </div>
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-bordered table-striped ajax_view" id="bottleInvoiceTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>sale</th>
                        <th>amount</th>
                        <th>Charge</th>
                        <th>addedBy</th>
                        <th>created_at</th>
                        <th>action</th>

                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</section>
