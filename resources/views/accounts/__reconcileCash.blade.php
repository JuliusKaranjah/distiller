<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>Cash Process
    </h1>
</section>

<!-- Main content -->
<section class="content no-print">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Cash Process</h3>

            <div class="box-tools">
                <a class="btn btn-block btn-primary" id="cashDeposit" href="{{action('AccountsController@receiveCashFromSales',['id' => $saleRep->id])}}">
                    <i class="fa fa-plus"></i> Cash Deposit</a>
            </div>

        </div>
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-bordered table-striped ajax_view" id="sell_table">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Customer</th>

                        <th>Status</th>
                        <th>Sale Ref.</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cash as $item)
                        <tr>
                            <td>{{$item->created_at}}</td>
                            <td>KES {{number_format($item->amount,2)}}</td>
                            <td>{{($item->transactionPayment->transaction->contact->name)}}</td>
                            <td><span class="badge badge-{{$item->status}}">{{$item->status}}</span></td>
                            <td>{{$item->transactionPayment->transaction->ref_no}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
    </div>
</div>



<div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade register_details_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<!-- This will be printed -->
<!-- <section class="invoice print_section" id="receipt_section">
</section> -->