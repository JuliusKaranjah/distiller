<div class="modal-dialog  no-print" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalTitle"> Bank Transfer
            </h4>
        </div>
        <div class="modal-body">


            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">

                    <p>Customer:
                        <b>{{($contact = ($transaction = ($payment = $cheque->transactionPayment)->transaction)->contact)->name}}</b>
                    </p>
                    <p>Mobile: <b>{{$contact->mobile}}</b></p>
                </div>


                <div class="col-sm-6 invoice-col">

                    <p>Trans. Date: <b>{{$cheque->created_at->toDayDateTimeString()}}</b> </p>
                    <p>Ref. No: <b>#{{$payment->payment_ref_no}}</b> </p>
                    <p>Date: <b>{{$payment->created_at->toDayDateTimeString()}}</b> </p>

                </div>
            </div>


            <br>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table bg-gray">
                            <tr class="bg-green">
                                <th>#</th>
                                <th class="text-center">Account Number</th>

                                <th class="text-center">Status</th>
                                <th class="text-center">Amount</th>

                            </tr>


                            <tr>
                                <td></td>
                                <td>
                                    {{$payment->bank_account_number}}
                                </td>
                                <td>
                                    <span class="label label-{{$cheque->status=='approved'?'primary':'info'}}">{{$cheque->status}}</span>
                                </td>

                                <td>
                                    <span class="display_currency"
                                          data-currency_symbol="true">{{number_format($cheque->amount,2)}}</span>
                                </td>

                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-xs-6 col-md-offset-6">
                    <div class="table-responsive">

                        <form action="/accounts/cheque-action" id="chequeFormAction" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="action" id="chequeAction">
                            <input type="hidden" name="chequeId" value="{{$cheque->id}}">
                        </form>

                        @if($cheque->status != 'approved')
                            <a href="#" class="btn btn-primary actionButton" data-action-type="accept"
                               data-value="{{$cheque->id}}" data-dismiss="modal">Approve Cheque</a>
                        @endif


                        <a href="#" class="btn btn-warning actionButton" data-action-type="decline"
                           data-value="{{$cheque->id}}" data-dismiss="modal">Decline Cheque</a>


                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default no-print" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var element = $('div.modal-xl');
        __currency_convert_recursively(element);

        $(".actionButton").on('click', function () {
            $("#chequeAction").val($(this).attr('data-action-type'));
            $("form#chequeFormAction").submit();
        })
    });
</script>