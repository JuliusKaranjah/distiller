@extends('layouts.app')
@section('title', 'Accounts Statement')

@section('content')

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {background-color: #f2f2f2;}
    </style>


    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-2 col-md-offset-1 col-xs-6 pull-right">
                <div class="form-group pull-right">
                    <div class="input-group">
                        <button type="button" class="btn btn-primary" id="purchase_sell_date_filter">
                    <span>
                      <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                    </span>
                            <i class="fa fa-caret-down"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br>


        <div class="row">
            <div class="col-sm-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Accounts Statement</h3>
                    </div>

                    <div class="box-body">

                        <h3 class="text-muted">
                            Sale rep.:

                            {{$sale->fullName()}}
                        </h3>

                        <h3 class="text-muted">
                            Total Invoice Amount:
                            KES {{number_format($sale->totalInvoiceAmount()-$sale->totalCompanySaleRepInvoice(),2)}}
                        </h3>

                        <p class="text-muted" style="color: mediumseagreen;">
                            Date Range:
                            {{\Carbon\Carbon::parse(isset(request()->start_date)?request()->start_date:\Carbon\Carbon::now()->startOfDay())->toDayDateTimeString()}} -
                            {{\Carbon\Carbon::parse(isset(request()->end_date)?request()->end_date:\Carbon\Carbon::now()->endOfDay())->toDayDateTimeString()}}
                        </p>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Total Sale: </h3>
                    </div>
                    <div class="box-body">
                        <a href="{{url('/reports/sales-representative-report/?')}}">
                            <strong>
                                KES {{number_format($totalSaleAmount,2)}}
                            </strong>
                        </a>
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Accounts Statement</h3>
                    </div>

                    <div class="box-body">

                        <table>
                            <thead>
                            <th>#</th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Credit</th>
                            <th>Debit</th>
                            </thead>
                           <tbody>
                           @foreach($saleStatements as $datum)
                               <tr>
                                   <td>{{$datum->id}}</td>
                                   <td>{{$datum->created_at}}</td>
                                   <td>{{$datum->description}}</td>
                                   <td>{{is_null($datum->credit)?'--':'KES '.number_format($datum->credit,2)}}</td>
                                   <td>{{is_null($datum->debit)?'--':'KES '.number_format($datum->debit,2)}}</td>
                               </tr>
                               @endforeach
                           </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>


    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script>
    $(function () {

        var url ="{{'/'.request()->segment(1).'/'.request()->segment(2).'/'.request()->segment(3).'/'}}";

        if($('#purchase_sell_date_filter').length == 1){
            $('#purchase_sell_date_filter').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#purchase_sell_date_filter span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    updatePurchaseSell();
                }
            );
            $('#purchase_sell_date_filter').on('cancel.daterangepicker', function(ev, picker) {
                $('#purchase_sell_date_filter').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
            });

            $('#purchase_sell_location_filter').change( function(){
                updatePurchaseSell();
            });

            updatePurchaseSell = function () {
                var start = $('#purchase_sell_date_filter').data('daterangepicker').startDate.format('YYYY-MM-DD');
                var end = $('#purchase_sell_date_filter').data('daterangepicker').endDate.format('YYYY-MM-DD');

                window.location = (url+"?start_date="+start+"&end_date="+end);
            }
        }

    })
    </script>
@endsection