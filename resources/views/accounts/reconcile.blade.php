@extends('layouts.app')
@section('title', 'Reconciliation')

@section('content')

    <style>
        .badge-completed{
            background-color: #4ca75b;
        }
        .badge-primary{
            background-color: #3b8dbc;
        }
        .badge-initiated{
            background-color: #01c0ef;
        }
        .badge-declined{
            background-color: #d63925;
        }
    </style>



    <!-- Content Header (Page header) -->
    {{--<section class="content-header">
        <h1>Reconciliation</h1>
    </section>--}}

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Cash</span>
                        <span class="info-box-number total_cash_at_hand">KES {{number_format($cash->sum('amount'),2)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Mpesa</span>
                        <span class="info-box-number total_cash_at_hand">KES {{number_format($mpesa->sum('amount'),2)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Cheque</span>
                        <span class="info-box-number total_cash_at_hand">KES {{number_format($cheque->sum('amount'),2)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            <div class="col-sm-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Bank Transfer</span>
                        <span class="info-box-number total_cash_at_hand">KES {{number_format($bankTransfer->sum('amount'),2)}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary" id="accordion">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
                                <i class="fa fa-filter" aria-hidden="true"></i> @lang('report.filters')
                            </a>
                        </h3>
                    </div>
                    <div id="collapseFilter" class="panel-collapse active collapse in" aria-expanded="true">
                        <div class="box-body">
                            {!! Form::open(['url' => action('AccountsController@reconciliation'), 'method' => 'get', 'id' => 'sales_representative_filter_form' ]) !!}
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('sr_id',  __('report.user') . ':') !!}
                                    <select class="form-control select2" id="userSelected" name="sr_id" style="width: 100%">
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->fullName()}}</option>
                                            @endforeach
                                    </select>

                                </div>
                            </div>


                            {{--<div class="col-md-4 hide">
                                <div class="form-group">

                                    {!! Form::label('sr_date_filter', __('report.date_range') . ':') !!}
                                    {!! Form::text('date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'id' => 'sr_date_filter', 'readonly']); !!}
                                </div>
                            </div>--}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <br>
                                    <button class="btn btn-primary" style="margin-top: 5px  "><i class="fa fa-filter"></i>Filter</button>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Summary -->
        <div class="row">
            <div class="col-sm-12">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">{{$saleRep?$saleRep->fullName():'Select Sale Rep.'}}</span>

                            <span class="info-box-number">
                            {{$saleRep?$saleRep->phone:'-'}}
                            </span>

                            <span class="info-box-number">
                            {{($saleRep?$saleRep->getFirstPermittedLocation():'')}}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>


                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total Uncleared Invoice</span>
                            <span class="info-box-number">
                               KES {{number_format(($saleRep)?$saleRep->totalInvoiceAmount():0,2)}}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total Commissions</span>
                            <span class="info-box-number">KES {{number_format($saleRep?$saleRep->totalCompanySaleRepInvoice():0,2)}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#sr_sales_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Cash</a>
                        </li>

                        <li>
                            <a href="#sr_mpesa_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Mpesa</a>
                        </li>

                        <li>
                            <a href="#sr_cheque_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Cheque</a>
                        </li>

                        <li>
                            <a href="#sr_bankTransfer_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Bank Transfer</a>
                        </li>

                        <li>
                            <a href="#sr_other_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Other</a>
                        </li>

                        <li>
                            <a href="#sr_commission_remit_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Remitted Commission</a>
                        </li>

                        <li>
                            <a href="#sr_bottleInvoice_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Bottle Invoice</a>
                        </li>

                        {{--<li>
                            <a href="#sr_bottlePaidCharges_tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i> Bottle Invoice Payment</a>
                        </li>--}}


                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="sr_sales_tab">
                            @include('accounts.__reconcileCash')
                        </div>
                        <div class="tab-pane" id="sr_mpesa_tab">
                            @include('accounts.__reconcileMpesa')
                        </div>

                        <div class="tab-pane" id="sr_cheque_tab">
                            @include('accounts.__reconcileCheque')
                        </div>


                        <div class="tab-pane" id="sr_bankTransfer_tab">
                            @include('accounts.__reconcileBankTransfer')
                        </div>


                        <div class="tab-pane" id="sr_other_tab">
                            @include('accounts.__reconcileOther')
                        </div>


                        <div class="tab-pane" id="sr_commission_remit_tab">
                            @include('accounts.__reconcileRemitCommission')
                        </div>


                        <div class="tab-pane" id="sr_bottleInvoice_tab">
                            @include('accounts.__bottleInvoice')
                        </div>


                        {{--<div class="tab-pane" id="sr_bottlePaidCharges_tab">
                            @include('accounts.__bottlePaidCharges')
                        </div>--}}


                    </div>

                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
    <div class="modal fade view_register" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade view_register" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

@endsection

@section('javascript')
    <script src="{{ asset('js/report.js?v=' . $asset_v) }}"></script>
    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            //load bottle invoice..
            var received_broken_bottle_table = $('#bottleInvoiceTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: '/bottle/process'+'?table=received-broken-bottle&sale_rep_id='+"{{request('sr_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'charge', name: 'charge'},
                    { data: 'addedBy', name: 'addedBy'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'action', name: 'action'},
                ]
            });

            var chargeBottleTable = $('#bottlePaidInvoiceTable').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[4, 'desc']],
                ajax: '/bottle/process'+'?table=charges-broken-bottle&sale_rep_id='+"{{request('sr_id')}}",
                columnDefs: [ {
                    // "targets": [1,2],
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'sale', name: 'sale'},
                    { data: 'amount', name: 'amount'},
                    { data: 'addedBy', name: 'addedBy'},
                    { data: 'created_at', name: 'created_at'},
                ]
            });


            $("#cashDeposit").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {
                        $("div.view_register").html(data).modal('show');
                    },
                    error:function (err) {
                        //
                    }
                })

            });



            $(".viewChequePayment").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {
                        $("div.view_register").html(data).modal('show');
                    },
                    error:function (err) {
                        //
                    }
                })

            });

            $("#remitCommission").on('click',function (e) {
                e.preventDefault();

                $("div.remitCommission").modal('show');
            });

            $("#remit_amount").on('input',function () {
                var amount = parseFloat($(this).val());
                var maximum = parseFloat($(this).attr('max'));

                if(amount > maximum)
                {
                    $("span.remitAmount").html('Amount input exceeds sale rep. commission. Commissions is '+maximum).show();
                    $("button.postRemitCommission").addClass('disabled');
                }else {
                    $("span.remitAmount").html('').hide();
                    $("button.postRemitCommission").removeClass('disabled');
                }

            });

            $("button.postRemitCommission").on('click',function (e) {
                if($(this).hasClass('disabled'))
                {
                    e.preventDefault();
                    return;
                }
            })
        })
    </script>

@endsection