<!-- Content Header (Page header) -->
<section class="content-header no-print">
    <h1>Cheque Process
    </h1>
</section>

<!-- Main content -->
<section class="content no-print">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Cheque Process</h3>


        </div>
        <div class="box-body">

            <div class="table-responsive">
                <table class="table table-bordered table-striped ajax_view" id="sell_table">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Cheque Number</th>
                        <th>Customer</th>
                        <th>Sale Ref.</th>

                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cheque as $item)
                        <tr href="{{action('AccountsController@chequeDetails', [$item->id])}}" class="viewChequePayment">
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->cheque_number}}</td>
                            <td>{{($item->transactionPayment->transaction->contact->name)}}</td>
                            <td>{{$item->transactionPayment->transaction->ref_no}}</td>
                            <td>KES {{number_format($item->amount,2)}}</td>
                            <td><span class="badge badge-{{$item->status}}">{{$item->status}}</span></td>

                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle btn-xs"
                                            data-toggle="dropdown" aria-expanded="false">
                                        view detail
                                    </button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade payment_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<div class="modal fade register_details_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>
<div class="modal fade close_register_modal" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
</div>

<!-- This will be printed -->
<!-- <section class="invoice print_section" id="receipt_section">
</section> -->