@extends('layouts.app')
@section('title', 'Cash Process')

@section('content')

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content no-print">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Sale Rep. Invoice</h3>


            </div>
            <div class="box-body">

                    <div class="row hide">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="button" class="btn btn-primary" id="sell_date_filter">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped ajax_view" id="sell_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sale Rep.</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                        </table>
                    </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="modal fade payment_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade register_details_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>
    <div class="modal fade close_register_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <!-- This will be printed -->
    <!-- <section class="invoice print_section" id="receipt_section">
    </section> -->


@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready( function(){
            //Date range as a button


            sell_table = $('#sell_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[0, 'desc']],
                "ajax": {
                    "url": "",
                    /*"data": function ( d ) {
                        var start = $('#sell_date_filter').data('daterangepicker').startDate.format('YYYY-MM-DD');
                        var end = $('#sell_date_filter').data('daterangepicker').endDate.format('YYYY-MM-DD');
                        d.start_date = start;
                        d.end_date = end;
                        d.is_direct_sale = 0;
                    }*/
                },
                columnDefs: [ {
                    "targets": 0,
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'name', name: 'name'  },
                    { data: 'action', name: 'action'  },
                    // { data: 'action', name: 'action'},
                ],
                columnDefs: [
                    {
                        'searchable'    : false,
                        'targets'       : [0]
                    },
                ],
            });
        });

    </script>

    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            $("#cashDeposit").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {

                        // console.info(data);

                        $("div.payment_modal").modal('show').html(data);

                    },
                    error:function (err) {
                        // console.error(err)
                    }
                })

            })
        })
    </script>
@endsection