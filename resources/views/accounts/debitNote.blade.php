@extends('layouts.app')
@section('title', 'Debit Note')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        @if(auth()->user()->can('invoice.view_all_invoice'))
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Sale Rep</span>
                            <span class="info-box-number total_cash_at_hand">{{$sale}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total Debit Note</span>
                            <span class="info-box-number total_cash_at_hand">KES {{number_format($totalDebitAmount,2)}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
            @endif
    </section>

    <!-- Main content -->
    <section class="content no-print">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Debit Note</h3>
                @can('accounts.debit.create')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" view-modal-popup href="{{action('AccountsController@addDebitNote')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endcan

            </div>
            <div class="box-body">

                    <div class="row hide">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <button type="button" class="btn btn-primary" id="sell_date_filter">
                                <span>
                                  <i class="fa fa-calendar"></i> {{ __('messages.filter_by_date') }}
                                </span>
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped ajax_view" id="sell_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Sale Rep.</th>
                                <th>Reason</th>
                                <th>Amount</th>

                            </tr>
                            </thead>
                        </table>
                    </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="modal fade payment_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade edit_payment_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <div class="modal fade register_details_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>
    <div class="modal fade close_register_modal" tabindex="-1" role="dialog"
         aria-labelledby="gridSystemModalLabel">
    </div>

    <!-- This will be printed -->
    <!-- <section class="invoice print_section" id="receipt_section">
    </section> -->


@stop

@section('javascript')
    <script type="text/javascript">
        $(document).ready( function(){
            //Date range as a button
            $('#sell_date_filter').daterangepicker(
                dateRangeSettings,
                function (start, end) {
                    $('#sell_date_filter span').html(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
                    sell_table.ajax.reload();
                }
            );
            $('#sell_date_filter').on('cancel.daterangepicker', function(ev, picker) {
                $('#sell_date_filter').html('<i class="fa fa-calendar"></i> {{ __("messages.filter_by_date") }}');
                sell_table.ajax.reload();
            });

            sell_table = $('#sell_table').DataTable({
                processing: true,
                serverSide: true,
                aaSorting: [[1, 'desc']],
                "ajax": {
                    "url": "",
                    "data": function ( d ) {
                        var start = $('#sell_date_filter').data('daterangepicker').startDate.format('YYYY-MM-DD');
                        var end = $('#sell_date_filter').data('daterangepicker').endDate.format('YYYY-MM-DD');
                        d.start_date = start;
                        d.end_date = end;
                        d.is_direct_sale = 0;
                    }
                },
                columnDefs: [ {
                    "targets": 0,
                    "orderable": false,
                    "searchable": false
                } ],
                columns: [
                    { data: 'id', name: 'id'  },
                    { data: 'created_at', name: 'created_at'  },
                    { data: 'sale', name: 'sale'  },
                    { data: 'description', name: 'description'},
                    { data: 'amount', name: 'amount'  },
                ],
                columnDefs: [
                    {
                        'searchable'    : false,
                        'targets'       : [0]
                    },
                ],
                // "fnDrawCallback": function (oSettings) {
                //     __currency_convert_recursively($('#sell_table'));
                // },
                // createdRow: function( row, data, dataIndex ) {
                //     $( row ).find('td:eq(4)').attr('class', 'clickable_td');
                // }
            });
        });

    </script>

    <script src="{{ asset('js/payment.js?v=' . $asset_v) }}"></script>
    <script>
        $(function () {

            $("#cashDeposit").on('click',function (e) {

                e.preventDefault();

                //do ajax load the modal and show it..
                $.ajax({
                    url:$(this).attr('href'),
                    method:"GET",
                    success:function (data) {

                        // console.info(data);

                        $("div.payment_modal").modal('show').html(data);

                    },
                    error:function (err) {
                        // console.error(err)
                    }
                })

            })
        })
    </script>
@endsection