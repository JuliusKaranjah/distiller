<div class="modal-dialog  no-print" role="document" id="myModal">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close no-print" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalTitle"> Receive Cash From Sales Rep.
            </h4>
        </div>
        <div class="modal-body">

            <form action="/accounts/post-receive-cash-from-sales" method="post">
                {!! csrf_field() !!}
                <input type="hidden" name="sale_rep_id" value="{{$saleRepId}}">
                {{--<div class="form-group">
                    <label for="email">Select Sales Rep.:</label>
                    <select name="sale_rep_id" id="sale_rep_id" class="select2" required>
                        @foreach($saleReps as $rep)
                            <option value="{{$rep->id}}">{{$rep->fullName()}}</option>
                            @endforeach
                    </select>
                </div>--}}
                <div class="form-group">
                    <label for="pwd">Cash Received:</label>
                    <input type="number" step="any" class="form-control" id="cash_amount" name="cash_amount">
                </div>
                <button type="submit" class="btn btn-primary">Accept Cash</button>
            </form>


        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default no-print" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var element = $('div.modal-xl');
        __currency_convert_recursively(element);

        $(".actionButton").on('click', function () {
            $("#chequeAction").val($(this).attr('data-action-type'));
            $("form#chequeFormAction").submit();
        })

        $(".select2").select2({
            dropdownParent: $("#myModal"),
            width: 'resolve'
        });
    });
</script>