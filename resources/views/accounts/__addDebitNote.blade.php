<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" id="modal-content">

<div class="box box-solid"><!--box start-->
    <form action="{{action('AccountsController@storeDebitNote')}}" method="post" class="box-body">
        {!! csrf_field() !!}
        <div class="col-md-12 col-lg-12" style="font-weight: bold">

            Add Debit Note
            <hr>
        </div>


        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Select Sales</label>

                        <select class="form-control select2" name="sale_rep_id">
                            @foreach($sales as $sale)
                                <option value="{{$sale->id}}">{{$sale->fullName()}}</option>
                                @endforeach
                        </select>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Amount</label>

                            <input type="number" step="0.01" name="amount" class="form-control" required>

                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12 col-lg-12">

            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="form-group">

                        <label>Reason</label>
                        <textarea name="description" id="reason" cols="30" rows="3" class="form-control">

                        </textarea>

                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-12 col-lg-12">
            <div class="row pull-left">
                <button type="submit" class="btn btn-primary" value="load">Add Debit Note</button>
            </div>
        </div>
    </form>
    </div><!--box end-->
    </div>
</div>