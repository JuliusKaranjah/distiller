@extends('layouts.app')
@section('title', 'Messages sent')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Messages sent</h1>
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Messages sent</h3>

            </div>
            <div class="box-body">
                @if(auth()->user()->can('message.view')||auth()->user()->can('message.view_all'))
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="messagesTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Recipient</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                @endcan
            </div>
        </div>

        <div class="modal fade customer_groups_modal" tabindex="-1" role="dialog"
             aria-labelledby="gridSystemModalLabel">
        </div>

    </section>
    <!-- /.content -->

@endsection
@section('script')
    <script>
        $("#messagesTable").DataTable({
            processing: true,
            serverSide: true,
            aaSorting: [[0, 'desc']],
            ajax: '',
            columnDefs: [{
                // "targets": [1,2],
                "orderable": false,
                "searchable": false
            }],
            columns: [
                {data: 'id', name: 'id'},
                {data: 'recipient', name: 'recipient'},
                {data: 'body', name: 'body'},

            ]
        });
    </script>
    @endsection