@extends('layouts.app')

@section('title','Send A Message')


@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Send A Message</h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => action('MessageController@store'), 'method' => 'post', 'id' => 'visit_add_form' ]) !!}

                <div class="row">


                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Select Recipient</label>

                                <select class="form-control select2" name="recipient" id="recipient">

                                    <option value="all_customers">All Customers</option>
                                    <option value="all_sale_rep">All Sales Representatives</option>
                                    <option value="other">Others</option>
                                </select>

                        </div>
                    </div>

                </div>
                <div class="row">


                    <div class="col-md-6" id="otherRecipient">
                        <div class="form-group">

                            <label>Others/external Recipient</label>

                                <select class="form-control select2-input" multiple name="recipientInput[]">


                                    @if($users->count())
                                        @foreach($users as $user)
                                            <option value="{{$mobile = $user->phone}}">{{$user->fullName().' - '.$mobile}}</option>
                                        @endforeach
                                    @endif

                                    @if($contacts->count())
                                        @foreach($contacts as $contact)
                                            <option value="{{$mobile = $contact->mobile}}">{{$contact->name.' - '.$mobile}}</option>
                                        @endforeach
                                    @endif

                                </select>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label>Message</label>
                            <textarea name="body" class="form-control" cols="30" rows="5" placeholder="Compose your message"></textarea>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-send"></i> Send</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

    <div class="modal fade contact_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    </div>

@endsection

@section('script')

    <script>
        $(function () {
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(showLocation);
            }else{
                $('#location').html('Geolocation is not supported by this browser.');
            }


            function showLocation(position) {

                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;

                $.ajax({
                    type: 'POST',
                    url: 'https://ridesafeweb.x20labs.com/admin/location_json/',
                    data: 'latitude=' + latitude + '&longitude=' + longitude,
                    success: function (msg) {
                        if (msg) {

                            if (msg.indexOf('Unnamed') != -1) {
                                $("#location").html('latitude=' + latitude + '&longitude=' + longitude);

                                document.cookie = "fbdata = " + 'lat=' + latitude + '&lon=' + longitude;
                            }
                            else {
                                $("#location").html(msg);
                                document.cookie = "fbdata = " + msg;
                            }


                        } else {
                            $("#location").html('Not Available');
                        }

                    },
                    error:function (err) {
                        console.error(err);
                    }

                });

            };

            var showOrHideOtherRecipients = function()
            {
                if($("select#recipient").val() == 'other'){

                    $("div#otherRecipient").slideDown('slow');
                }else {
                    $("div#otherRecipient").slideUp('slow');
                }
            };

            $("select#recipient").on('change',function () {
                showOrHideOtherRecipients();
            });

            showOrHideOtherRecipients();

            $("a#addCustomer").on('click',function (e) {

                e.preventDefault();

                $.ajax({
                    url:$(this).attr('href'),
                    method:'GET',
                    success:function (data) {
                        $("div.contact_modal").html(data).modal('show');
                    }
                })
            });

        })
    </script>

@endsection