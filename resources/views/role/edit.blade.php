{{--{{dd($role_permissions)}}--}}

@extends('layouts.app')
@section('title', __('role.edit_role'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@lang( 'role.edit_role' )</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                {!! Form::open(['url' => action('RoleController@update', [$role->id]), 'method' => 'PUT', 'id' => 'role_form' ]) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('name', __( 'user.role_name' ) . ':*') !!}
                            {!! Form::text('name', str_replace( '#' . auth()->user()->business_id, '', $role->name) , ['class' => 'form-control', 'required', 'placeholder' => __( 'user.role_name' ) ]); !!}
                        </div>
                    </div>
                </div>
                @if(in_array('service_staff', $enabled_modules))
                    <div class="row">
                        <div class="col-md-2">
                            <h4>@lang( 'lang_v1.user_type' )</h4>
                        </div>
                        <div class="col-md-9 col-md-offset-1">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('is_service_staff', 1, $role->is_service_staff,
                                        [ 'class' => 'input-icheck']); !!} {{ __( 'restaurant.service_staff' ) }}
                                    </label>
                                    @show_tooltip(__('restaurant.tooltip_service_staff'))
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-3">
                        <label>@lang( 'user.permissions' ):</label>
                    </div>
                </div>

                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.user' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'user.view', in_array('user.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.user.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'user.create', in_array('user.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.user.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'user.update', in_array('user.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.user.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'user.edit_credit_limit', in_array('user.edit_credit_limit', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Edit Credit Limit
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'user.delete', in_array('user.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.user.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>


                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang('role.accounts')</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.view', in_array('accounts.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.accounts.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.view_all', in_array('accounts.view_all', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View all sale rep. account's data
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.statement', in_array('accounts.statement', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Accounts statement
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.debit', in_array('accounts.debit', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Accounts debit
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.debit.create', in_array('accounts.debit.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Add debit note
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.credit', in_array('accounts.credit', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Accounts credit
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'accounts.credit.create', in_array('accounts.credit.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Add credit note
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <hr>


                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Bottle</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'bottle.list', in_array('bottle.list', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Bottle list
                                </label>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'bottle.limit', in_array('bottle.limit', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Bottle limit
                                </label>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'bottle.process', in_array('bottle.process', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Bottle process
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>


                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.supplier' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'supplier.view', in_array('supplier.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.supplier.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'supplier.create', in_array('supplier.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.supplier.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'supplier.update', in_array('supplier.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.supplier.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'supplier.delete', in_array('supplier.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.supplier.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>


                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.customer' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'customer.view', in_array('customer.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.customer.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'customer.create', in_array('customer.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.customer.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'customer.update', in_array('customer.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.customer.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'customer.delete', in_array('customer.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.customer.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'business.product' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'product.view', in_array('product.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.product.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'product.create', in_array('product.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.product.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'product.update', in_array('product.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.product.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'product.delete', in_array('product.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.product.delete' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'product.opening_stock', in_array('product.opening_stock', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'lang_v1.add_opening_stock' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.purchase' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'purchase.view', in_array('purchase.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.purchase.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'purchase.create', in_array('purchase.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.purchase.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'purchase.update', in_array('purchase.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.purchase.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'purchase.delete', in_array('purchase.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.purchase.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'sale.sale' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sell.view', in_array('sell.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sell.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sell.create', in_array('sell.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sell.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sell.update', in_array('sell.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sell.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sell.delete', in_array('sell.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sell.delete' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'direct_sell.access', in_array('direct_sell.access', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.direct_sell.access' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Order</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'order.view', in_array('order.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Order
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'order.create', in_array('order.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Create Order
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'order.view_all_orders', in_array('order.view_all_orders', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View All Orders
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'order.order_process', in_array('order.order_process', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Process Orders
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'order.order_accountant_process', in_array('order.order_accountant_process', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Accountant Orders Process
                                </label>
                            </div>
                        </div>
                    </div>
                </div><hr>


                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Logistics</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'logistics.view', in_array('logistics.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View logistics
                                </label>
                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'logistics.view_all_logistics', in_array('logistics.view_all_logistics', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View all logistics
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'logistics.warehouse.view', in_array('logistics.warehouse.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Add Warehouse
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'logistics.vehicle.view', in_array('logistics.vehicle.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Add Vehicle
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'logistics.driver.view', in_array('logistics.driver.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Add Driver
                                </label>
                            </div>
                        </div>
                    </div>
                </div><hr>



                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Visit</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'visit.view', in_array('visit.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Visit
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'visit.view_all', in_array('visit.view_all', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View all Visits
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'visit.create', in_array('visit.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Create Visit
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'visit.reason.view', in_array('visit.reason.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View visit reason
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'visit.reason.create', in_array('visit.reason.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Create visit reason
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>



                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Messages</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'message.view', in_array('message.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Messages
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'message.view_all', in_array('message.view_all', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View all Messages
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'message.compose', in_array('message.compose', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Create Message
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Promotion</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.order.list', in_array('sample.order.list', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Promotion order
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.order.create', in_array('sample.order.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Create Promotion Order
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.issue.sales', in_array('sample.issue.sales', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Issue Promo to sale rep
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.issue.customer', in_array('sample.issue.customer', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Issue Promo to customer
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.stock', in_array('sample.stock', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Promotion Stock
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.sale_rep_approval', in_array('sample.sale_rep_approval', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Promo. sale Rep. executive approval
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sample.accountant', in_array('sample.accountant', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Promo Accountant Approval
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>




                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>Invoice</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'invoice.view', in_array('invoice.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View Invoice
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'invoice.view_all_invoice', in_array('invoice.view_all_invoice', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} View All Invoice
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'invoice.process', in_array('invoice.process', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} Process Invoice
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.brand' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'brand.view', in_array('brand.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.brand.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'brand.create', in_array('brand.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.brand.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'brand.update', in_array('brand.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.brand.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'brand.delete', in_array('brand.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.brand.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.tax_rate' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'tax_rate.view', in_array('tax_rate.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.tax_rate.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'tax_rate.create', in_array('tax_rate.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.tax_rate.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'tax_rate.update', in_array('tax_rate.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.tax_rate.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'tax_rate.delete', in_array('tax_rate.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.tax_rate.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.unit' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'unit.view', in_array('unit.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.unit.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'unit.create', in_array('unit.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.unit.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'unit.update', in_array('unit.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.unit.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'unit.delete', in_array('unit.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.unit.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'category.category' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'category.view', in_array('category.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.category.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'category.create', in_array('category.create', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.category.create' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'category.update', in_array('category.update', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.category.update' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'category.delete', in_array('category.delete', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.category.delete' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.report' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'purchase_n_sell_report.view', in_array('purchase_n_sell_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.purchase_n_sell_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'tax_report.view', in_array('tax_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.tax_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'contacts_report.view', in_array('contacts_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.contacts_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'expense_report.view', in_array('expense_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.expense_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'profit_loss_report.view', in_array('profit_loss_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.profit_loss_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'stock_report.view', in_array('stock_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.stock_report.view' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'trending_product_report.view', in_array('trending_product_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.trending_product_report.view' ) }}
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'register_report.view', in_array('register_report.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.register_report.view' ) }}
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'sales_representative.view', in_array('sales_representative.view', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sales_representative.view' ) }}
                                </label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'report.view-all-users', in_array('sales_representative.view-all-report', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.sales_representative.view-all-report' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row check_group">
                    <div class="col-md-1">
                        <h4>@lang( 'role.settings' )</h4>
                    </div>
                    <div class="col-md-2">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'business_settings.access', in_array('business_settings.access', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.business_settings.access' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'barcode_settings.access', in_array('barcode_settings.access', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.barcode_settings.access' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'invoice_settings.access', in_array('invoice_settings.access', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.invoice_settings.access' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'expense.access', in_array('expense.access', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.expense.access' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <h4>@lang( 'role.dashboard' )</h4>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'dashboard.data', in_array('dashboard.data', $role_permissions),
                                    [ 'class' => 'input-icheck']); !!} {{ __( 'role.dashboard.data' ) }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                @if(in_array('tables', $enabled_modules) && in_array('service_staff', $enabled_modules) )
                    <div class="row check_group">
                        <div class="col-md-1">
                            <h4>@lang( 'restaurant.bookings' )</h4>
                        </div>
                        <div class="col-md-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="check_all input-icheck"> {{ __( 'role.select_all' ) }}
                                </label>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('permissions[]', 'crud_all_bookings', in_array('crud_all_bookings', $role_permissions),
                                        [ 'class' => 'input-icheck']); !!} {{ __( 'restaurant.add_edit_view_all_booking' ) }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('permissions[]', 'crud_own_bookings', in_array('crud_own_bookings', $role_permissions),
                                        [ 'class' => 'input-icheck']); !!} {{ __( 'restaurant.add_edit_view_own_booking' ) }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                @endif
                <div class="row">
                    <div class="col-md-3">
                        <h4>@lang( 'role.access_locations' )</h4>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('permissions[]', 'access_all_locations', in_array('access_all_locations', $role_permissions),
                                  [ 'class' => 'input-icheck']); !!} {{ __( 'role.all_locations' ) }}
                                </label>
                                @show_tooltip(__('tooltip.all_location_permission'))
                            </div>
                        </div>
                        @foreach($locations as $location)
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox('location_permissions[]', 'location.' . $location->id, in_array('location.' . $location->id, $role_permissions),
                                        [ 'class' => 'input-icheck']); !!} {{ $location->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">@lang( 'messages.update' )</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection