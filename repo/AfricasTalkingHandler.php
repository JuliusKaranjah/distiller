<?php

namespace Repo;

use AfricasTalking\SDK\AfricasTalking;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class AfricasTalkingHandler
{
    private $username;
    private $apiKey;
    private $at;
    private $sms;

    public function __construct()
    {


        $this->username = env('AFRICASTALKINGUSERNAME','crywandistillers');
        $this->apiKey = env('AFRICASTALKINGAPIKEY','738e8b56fb18766b4d66b36b0e000375ed23427f38429f8ac8fc97809a96079b');
//
//        $this->username = env('AFRICASTALKINGUSERNAME','sandbox');
//        $this->apiKey = env('AFRICASTALKINGAPIKEY','d3af9b69182dce77dec357d7a7572f8ed52c6631dafc064ac25e5d977a39ee19');

        $this->at = new AfricasTalking($this->username,$this->apiKey);

        $this->sms = $this->at->sms();
    }

    public function sendSms($recipients, $message)
    {

        try{

            $recipients = $this->prepareRecipient($recipients);

            $results = $this->sms->send([
                'to'      => $recipients,
                'message' => $message,
                'from'    => 'CRYWAN',
            ]);




            return ['status' => 'OK','message' => 'Message successfully sent'];

        }catch (\Exception $e)
        {

            return ['status' => 'ERROR','message' => $e->getMessage()];
        }
    }

    /**
     * @throws
    */

    private function prepareRecipient($recipient)
    {
        if(str_contains($recipient,'+'))
        {
            return $recipient;
        }

        if(strlen($recipient)>10)
        {
            return '+'.$recipient;
        }

        if(strlen($recipient)==9)
        {
            $recipient = '0'.$recipient;
        }


        if(strlen($recipient)==10)

        {
            return '+254'.substr($recipient,1);
        }

        throw (new \Exception('Invalid number'));

    }


}