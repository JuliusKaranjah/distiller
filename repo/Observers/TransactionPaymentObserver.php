<?php

namespace Repo\Observers;


use App\PaymentProcessing;
use App\TransactionPayment;
use Repo\Invoice;

class TransactionPaymentObserver
{

    public function created(TransactionPayment $transactionPayment)
    {

        if(! in_array($transactionPayment->method,['bank_transfer','cash','cheque']))
        {
            Invoice::adjustSaleRepInvoice(auth()->user(),$transactionPayment->amount);
        }



        $data = [
            'created_by' => $transactionPayment->created_by,
            'method' => ($paymentMode = $transactionPayment->method),
            'transaction_payment_id' => $transactionPayment->id,
            'amount' => $transactionPayment->amount,
            'cheque_number' => $transactionPayment->cheque_number,
            'drawer_name' => $transactionPayment->drawer_name,
            'bank_name' => $transactionPayment->bank_name,
        ];

        if( $transactionPayment->method == 'mpesa' || $transactionPayment->method == 'card' || $transactionPayment->method == 'other')
        {
            $data['remit_at'] = now();
            $data['status'] = 'completed';
        }



        PaymentProcessing::create($data);

    }

}