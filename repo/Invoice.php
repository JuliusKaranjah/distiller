<?php

namespace Repo;


use App\CompanySaleRepInvoice;
use App\Order;
use App\User;
use Illuminate\Validation\Rules\In;

class Invoice
{


    //after making a sale..this method should be called to
    //adjust invoice amount either for company or for sale rep.


    public static function adjustSaleRepInvoice(User $user,$amount)//reduce sale invoice amount.
    {
        if($amount<0)
        {

            $invoice = (new Invoice())->getSaleRepInvoice($user,true);

            if(is_null($invoice))
            {
                $companyInvoice = (new Invoice())->getSaleRepCompanyInvoice($user);

                $companyInvoice->increment('amount',$amount);

            }else
            {
                $invoice->decrement('amount',$amount);
            }

            return ['status' => 'success'];

        }

        $invoice = (new Invoice())->getSaleRepInvoice($user);

        if(!is_null($invoice))
        {
            $excessAmount = (new Invoice())->reduceSaleRepInvoice($invoice,$amount);

            if($excessAmount > 0)
            {
                (new Invoice())->adjustSaleRepInvoice($user,$excessAmount);
            }

        }else{
            //add to company sale invoice

            (new Invoice())->addToCompanySaleRepInvoice($user,$amount);
        }

        return ['status' => 'success'];
    }


    public static function addSaleRepInvoice(Order $order)
    {

        $saleRepInvoice = $order->saleRepInvoice()->create(
            [
                'user_id' => $order->added_by,
                'amount' => $order->amount?:0
            ]
        );

        return $saleRepInvoice;
    }















    private function getSaleRepInvoice(User $user,$withNegativeAmount = false)
    {
        $invoices = $user->saleRepInvoices;

        if(!$withNegativeAmount)
        {
            $invoices = $invoices->where('amount','>',0);
        }

        return $invoices->last();

    }
    private function getSaleRepCompanyInvoice(User $user)
    {
        $companySaleRepInvoice = $user->companySaleRepInvoice;


        if(is_null($companySaleRepInvoice))
        {
            return
                (new CompanySaleRepInvoice())->add($user);
        }

        return $companySaleRepInvoice;
    }

    private function reduceSaleRepInvoice($invoice,$amount)
    {


        $amountDue = $invoice->amount;


        if($amountDue > $amount)
        {
            $invoice->update(
                [
                    'amount' => $invoice->amount - $amount
                ]
            );

            return 0;
        }


        $invoice->update(
            [
                'amount' => 0
            ]
        );

        return $amount - $amountDue;

    }

    public function reduceCompanyCommission($user,$amount)
    {
        $companySaleInvoice = $this->getSaleRepCompanyInvoice($user);

        $companySaleInvoice->decrement('amount',$amount);
    }

    private function addToCompanySaleRepInvoice($user,$amount)
    {
        $companySaleInvoice = $this->getSaleRepCompanyInvoice($user);


        $companySaleInvoice->increment('amount',$amount);

    }


}