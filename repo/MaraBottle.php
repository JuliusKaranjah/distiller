<?php
/**
 * Created by PhpStorm.
 * User: julius
 * Date: 12/8/18
 * Time: 10:15 AM
 */

namespace Repo;


use App\SaleMaraBottle;
use App\User;

class MaraBottle
{

    public static function addMoreBottle(User $sale, $amount)
    {
        if($maraBottle = SaleMaraBottle::where(['sale_rep_id' => $sale->id])->first())
        {
            $maraBottle->increament('amount',$amount);
        }

        SaleMaraBottle::create(
            [
                'sale_rep_id' => $sale->id,
                'amount' => $amount
            ]
        );

        return true;
    }

}