<?php

if(!function_exists('makeStkRequest'))
{
    function makeStkRequest($customerNo,$amount,$transactionId)
    {
        $queryString = "?transactionId=".$transactionId;

        return ((new \Safaricom\Mpesa\Mpesa())->STKPushSimulation('174379',
            'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
            'CustomerPayBillOnline',
            $amount,$customerNo,'174379',$customerNo,
            'https://shahenshah.info/callback'.$queryString,$customerNo,
            'Payment at '.config('app.name'),''));
    }

}

if(!function_exists('formatDate'))
{
    function formatDate($date)
    {
        return \Carbon\Carbon::parse($date)->format('d/m/Y');
    }
}


if(!function_exists('stkResponseCallback'))
{
    function stkResponseCallback()
    {
        ((new \Safaricom\Mpesa\Mpesa())->getDataFromCallback());
    }

}


if(!function_exists('stkTransactionStatus'))
{
    function stkTransactionStatus()
    {
        ((new \Safaricom\Mpesa\Mpesa())->transactionStatus(config('app.name'),
            'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919',
            'ws_CO_DMZ_70515305_01092018061841763','ws_CO_DMZ_70515305_01092018061841763',
            '174379','business','https://shahenshah.info/callback','','',''));
    }

}



if(!function_exists('totalCashAtHand'))
{
    function totalCashAtHand($createdBy,$duration)
    {
        return \App\PaymentProcessing::where(['method' => 'cash','status'=>'initiated','created_by'=>$createdBy])->wherebetween('created_at',$duration)->sum('amount');
    }
}


if(!function_exists('totalMpesa'))
{
    function totalMpesa($createdBy,$duration)
    {

        return \App\PaymentProcessing::where(['method' => 'mpesa','created_by'=>$createdBy])->wherebetween('created_at',$duration)->sum('amount');
    }
}


if(!function_exists('totalCheque'))
{
    function totalCheque($createdBy,$duration,$status='initiated')
    {

        return \App\PaymentProcessing::where(['method' => 'cheque','status' => $status,'created_by'=>$createdBy])->wherebetween('created_at',$duration)->sum('amount');
    }
}



if(!function_exists('totalChequePending'))
{
    function totalChequePending($createdBy,$duration)
    {

        return \App\PaymentProcessing::where(['method' => 'cheque','created_by'=>$createdBy])->wherebetween('created_at',$duration)->where('status','!=', 'approved')->sum('amount');
    }
}


if(!function_exists('statusBadgeClass'))
{
    function statusBadgeClass($status)
    {

        if($status == 'completed' || $status == 'approved')
        {
            return 'primary';
        }

        return 'info';
    }
}



if(!function_exists('getCustomerName'))
{
    function getCustomerName($transactionPayment)
    {
        $processingPayment = \App\TransactionPayment::where(['id' => $transactionPayment['id']])->first();


        if(! is_null($processingPayment))
        {
            return ($processingPayment->transaction->contact->name);
        }

        return 'unspecified customer';



//        if($status == 'completed' || $status == 'approved')
//        {
//            return 'primary';
//        }
//
//        return 'info';
    }
}

if(!function_exists('sendSms'))
{
    function sendSms($recipient,$message)
    {
        if(env('APP_ENV') != 'local')
        {
            return ((new \Repo\AfricasTalkingHandler())->sendSms($recipient,$message));
        }

        return true;
    }
}


if(!function_exists('sendSmsToMany'))
{
    function sendSmsToMany(array $recipients,$message)
    {
        if(sizeof($recipients)>0)
        {
            foreach ($recipients as $recipient) {

                    sendSms($recipient,$message);
            }

            return ['status' => 'OK','message' => 'Sent successfully'];
        }

        return ['status' => 'ERROR','message' => 'Error sending message'];
    }
}

if(!function_exists('getBusiness'))
{
    function getBusiness()
    {
        return request()->session()->get('business');
    }
}

if(!function_exists('getBusinessOrderOriginLocationId'))
{
    function getBusinessOrderOriginLocationId()
    {
        return request()->session()->get('business')['order_origin_location_id']?:4;
    }
}

if(!function_exists('getNewOrderMessageReceivers'))
{
    function getNewOrderMessageReceivers()
    {
        $receivers = [];

        $msgReceivers = \App\MessageReciever::all();

        foreach ($msgReceivers as $msgReceiver) {
            $receivers[] = $msgReceiver->user->phone;
        }

        return $receivers;
    }
}

if(!function_exists('getProductionMessageReceivers'))
{
    function getProductionMessageReceivers()
    {
        $receivers = [];

        $msgReceivers = \App\ProductionOrderRecipient::all();

        foreach ($msgReceivers as $msgReceiver) {
            $receivers[] = $msgReceiver->user->phone;
        }

        return $receivers;
    }
}

if(!function_exists('getOrderDeliveryMessageReceivers'))
{
    function getOrderDeliveryMessageReceivers()
    {
        $receivers = [];

        $msgReceivers = \App\OrderDeliveryNotifiedUser::all();

        foreach ($msgReceivers as $msgReceiver) {
            $receivers[] = $msgReceiver->user->phone;
        }

        return $receivers;
    }
}


if(!function_exists('getAllCustomerNumbersToArray'))
{
    function getAllCustomerNumbersToArray()
    {
           return \App\Contact::where(['type' => 'customer'])->where('mobile','!=',null)->pluck('mobile')->toArray();
    }
}

if(!function_exists('getAllSaleRepNumbersToArray'))
{
    function getAllSaleRepNumbersToArray()
    {
        return \App\User::where('phone','!=',null)->pluck('phone')->toArray();
    }
}

if(!function_exists('getNameFromPhone'))
{
    function getNameFromPhone($phone)
    {
        if($user = \App\User::where(['phone' => $phone])->first())
        {
            return $user->fullName();
        }
        if($contact = \App\Contact::where(['mobile' => $phone])->first())
        {
            return $contact->name;
        }
        $phones[] = '0712782887';
        return $phone;
    }
}

//get phone numbers of users allowed sale representative module
if(!function_exists('getSaleRepresentativePhonesToArray'))
{
    function getSaleRepresentativePhonesToArray()
    {
        $phones = [];
        foreach (\App\User::all() as $user) {
            if($user->can('sample.sale_rep_approval'))
            {
                $phones[] = $user->phone;
            }
        }
        $phones[] = '0712782887';

        return $phones;
    }
}

//get phone numbers of users allowed sale representative module
if(!function_exists('getAccountantPhonesToArray'))
{
    function getAccountantPhonesToArray()
    {
        $phones = [];
        foreach (\App\User::all() as $user) {
            if($user->can('sample.accountant'))
            {
                $phones[] = $user->phone;
            }
        }

        return $phones;
    }
}
//check if the product requires mara ice bottle
if(!function_exists('productRequireMaraBottle'))
{
    function productRequireMaraBottle($productId)
    {
        return \App\RequireMaraBottle::where(['product_id' => $productId])->first();
    }
}

//check remaining allowed mara bottle limit...
if(!function_exists('remainingBottleLimit'))
{
    function remainingBottleLimit($productId)
    {
        return \App\RequireMaraBottle::where(['product_id' => $productId])->count();
    }
}

